const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

const rename = require('gulp-rename');
const version = require('./package.json').version;

function scss(cb) {
  gulp
    .src('./src/themes/*-master.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist'));

  gulp
    .src('./src/themes/*-master.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename(function(path){
      path.basename += `-${version}`
    }))
    .pipe(gulp.dest('./dist/min'));

  gulp
    .src('./src/icons/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./dist/min/icons'));

  gulp
    .src('./src/icons/**/font/*.*')
    .pipe(gulp.dest('./dist/min/icons'))

  cb();
}

function watch(cb) {
  gulp.watch('./src/**/*.scss', scss);
  cb();
}

exports.build = scss;
exports.watch = watch;
exports.default = scss;
