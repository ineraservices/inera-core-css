const cssList = {
  inera: {
    path: "https://www.inera.se/build/inera/static/stylesheets/app.css",
    preload: "https://www.inera.se/build/inera/static/stylesheets/preload.css",
    icons:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/dist/min/icons/inera/fontello/style.css",
    font: "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/fonts/poppins/poppins.css",
  },
  1177: {
    path: "https://www.1177.se/build/1177/static/stylesheets/app.css",
    preload: "https://www.1177.se/build/1177/static/stylesheets/preload.css",
    icons:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/dist/min/icons/1177/fontello/style.css",
    primaryFont:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/fonts/open-sans/open-sans.css",
    secondaryFont:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/fonts/inter/inter.css",
  },
  rgswebb: {
    path: "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/resources/rgswebb_copy.css",
    icons:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/dist/min/icons/1177/fontello/style.css",
    primaryFont:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/fonts/open-sans/open-sans.css",
    secondaryFont:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/static/fonts/inter/inter.css",
  },
  umo: {
    path: "https://www.umo.se/build/umo/static/stylesheets/app.css",
    preload: "https://www.umo.se/build/umo/static/stylesheets/preload.css",
    icons:
      "https://bb.githack.com/ineraservices/inera-core-css/raw/develop/dist/min/icons/1177/fontello/style.css",
  },
};

function loadCSS([id, path]) {
  if (!path) return;
  const style = document.createElement("link");
  style.rel = "stylesheet";
  style.type = "text/css";
  style.id = id;
  style.href = path;
  document.getElementsByTagName("head")[0].appendChild(style);
}

function loadSiteCSS() {
  const name = window.useStylesheet || "1177";
  Object.entries(cssList[name]).forEach(loadCSS);
}

window.addEventListener("load", loadSiteCSS);
