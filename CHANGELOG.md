### 0.16.0

- BREAKING: Expandables are now using the <details> element.
- Padding changed back to 20px for consistency with values in  (default.scss).
- Added csupport for logged in mobile menu.
- Layout fix for Inera checkboxes.
- Layout fixes for radio buttons (Inera theme). Changed labels for button examples to use Primary/Secondary wording.

### 0.15.0

- Removed use of multiple ULs for desktop navigation.
- Edited to default alignment of tooltip arrow.
- Code is now valid with validator.w3.org/nu.
- Added styles for expanded top level navigation link.

### 0.14.0

- Change name of $breakpoints to $core-breakpoints, avoiding foundation-sites collission. Add $base parameter to rem-calc(), maintaining use compatibility with foundation-sites.
- Changed breakpoint lg (large) 1024px (from 2025px).
- Removed the use of divs inside of spans on buttons.
- Updated aria attributes for modals.

### 0.13.0

- Breaking: Update name for breakpoint mixin. For compability reasons with foundation-css used on 1177.se we rename the breakpoint mixin and add "im" prefix for inera-mixin.
- layout and markup fixes for global navigation bar.

### 0.12.0

- add 100% max-width to iframe
- fix button styles for inera
- add margin to expandable icon with utility-classes (iu-ml-300)
- add position relative to clickable items inside linked card.
- removed deprecated word-break.
- layout and markup fixes for global navigation bar.

### 0.11.0

- content inside container is 1260px container width including gutters is 1280px

### 0.10.1

- update button group
- fix button font-weight
- typographic updates
- remove outline for elements with tabindex -1
- add cursor pointer for all button elements
- add headline component for linklist

### 0.9.0

- updates on agent banner.
- updates on notifications
- breaking: page footer markup
- update <dl> margins

### 0.8.1

- bugfixes, resolves issue: #7, #10, #11, #12, #13, #14
- new body/page wrapper class `ic-body` (issue #8)
- px based media querys (not em converted)

### 0.8.0

- add grid rows
- add fontello icons
- add svg icons
- correct border radius on badge component
- add agent notice
- add sticky component
- breaking: global message moved outside header element
- breaking: added !important to many utility classes
- add block list component
- change focus offset of ic-card
- add button that looks like a link
- add newslist
- add role=list to `ul` elements
- unify page footer demo for desktop and mobile content.
- add cookie information links to page footer

### 0.7.1

- change `.ic-modal__actions` to `.ic-button-group`
- add `.ic-forms__group`.
- improve error message
- more responsive helpers
- more styles for inera
- added more heading versions to demo

### 0.7.0

- add some minor fixes for internet explorer (11).
- new notification component
- new tooltip component
- new modal component
- change hamburger component
- styled scrollbar on textarea is optional (sass-variable)
- tabbed interface
- backdrop component

### 0.6.1

- add script to expandable content
- fix ic-button inside ic-text
- add loadstylesheet resources

### 0.5.0

- add footer logo
- textfield and error message
- fix responsive grid helpers
- add styled scrollbar to textfield
- dropdown component 1177
- fix checkbox and radiobuttons 1177
- add `a.ic-button` styles
- expandable components
- fix date label for small devices
- add accessability addon for storybook

### 0.4.0

- fix local message icon
- class for removing card styles for small devices
- update alert icons

### 0.3.0

- remove warning color and and observe color
- update alert components

### 0.2.1

- Add Date, Time and Range inputs
- Mobile navigation
- Improved page header and mobile menu

### 0.2.0

- Add badge component
- Fix secondary button
- Improve code display

### 0.1.0

- Upgrade all dependencies to latest
