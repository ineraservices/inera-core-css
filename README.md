# Gemensam CSS

Demo master inera: [css.inera.se](https://css.inera.se)

Demo develop inera: [här](https://develop--flamboyant-meninsky-54afd1.netlify.app/)

Demo master: [css.1177.se](https://css.1177.se)

Demo develop 1177: [här](https://develop--peaceful-noyce-77ac26.netlify.app/)

## Utveckling

För att köra utvecklingsmiljön behöver du node.js. Det kan du [ladda hem och installera här](http://nodejs.org).

Klona detta repository. Gå till katalogen och kör

```
$ npm install
```

För att starta utvecklingsmiljön och kör följande kommando,
öppna sedan http://localhost:6006/ webbläsaren.

```
$ npm start
```

För att skapa en färdig distributionsmapp kör.

```
$ npm run build
```

Detta kommer att rensa `/dist/` mappen och skapa nya CSS filer.
