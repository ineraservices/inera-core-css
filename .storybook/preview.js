import { addDecorator, addParameters } from "@storybook/html";

addParameters({
  statuses: {
    deprecated: "#c12143",
    wip: "#ffc100",
    beta: "#fa8100",
    done: "#419002",
  },
  options: {
    storySort: (a, b) =>
      a[1].kind === b[1].kind
        ? 0
        : a[1].id.localeCompare(b[1].id, undefined, { numeric: true }),
  },
});

const storyAsString = (story, context, theme) => {
  return `
    <link href="/${theme}-master.css" rel="stylesheet" />
    <link href="/min/icons/${theme}/fontello/style.css" rel="stylesheet" />
    ${story}
  `;
};

const storyAsNode = (story, context, theme) => {
  const wrapper = document.createElement("div");
  const style = document.createElement("link");
  style.href = `/${theme}-master.css`;
  style.rel = "stylesheet";
  wrapper.appendChild(style);
  wrapper.appendChild(story);
  return wrapper;
};

addDecorator((story, context) => {
  // Normally the story uses the theme selected in the toolbar
  let theme = context.globals.theme;

  // The story can override the theme in the parameters, then
  // it ignores the theme set in the toolbar
  if (context.parameters.theme) {
    theme = context.parameters.theme;
  }

  // Add style links to the story
  const tale = story();
  return typeof tale === "string"
    ? storyAsString(tale, context, theme)
    : storyAsNode(tale, context, theme);
});

// Use STORYBOOK_THEME from environment.
// ---
// Defined in ".env" file for development (rename .env.example)
// and in netlify configuration for hosted version
// --
// More on env variables in storybook:
// https://storybook.js.org/docs/react/configure/environment-variables
// --
// with fallback to "1177"
const defaultTheme = process.env.STORYBOOK_THEME || "1177";

export const globalTypes = {
  theme: {
    name: "Site/Theme",
    description: "Global theme for components",
    defaultValue: defaultTheme,
    toolbar: {
      items: [
        { value: "1177", title: "1177" },
        { value: "1177-profession", title: "1177-profession" },
        { value: "inera", title: "Inera" },
        { value: "inera-admin", title: "Inera-Admin" },
      ],
      showName: true,
    },
  },
};
