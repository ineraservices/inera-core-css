module.exports = {
  stories: ["../stories/**/*.stories.js"],
  addons: [
    {
      name: "@storybook/addon-essentials",
      options: {
        controls: false,
        actions: false,
      },
    },
    "@etchteam/storybook-addon-status/register",
    "@storybook/addon-a11y",
    "@storybook/addon-postcss",
  ],
  info: {
    minimized: true,
  },
};
