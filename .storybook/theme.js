import { create } from "@storybook/theming";
import logo from "../src/themes/inera_logo_1.0.svg";

export default create({
  base: "light",
  brandTitle: "Inera",
  brandUrl: "#",
  brandImage: logo,
});
