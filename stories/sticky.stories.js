import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Sticky",
  parameters: {
    status: "",
  },
};

export const Sticky = () =>
  markupWithCodeExample([
    `
<div style="background:#eee;height:100px;overflow:auto;position:relative;">
  <div style="height:1000px;">
    <p>This is a text</p>
    <div class="ic-sticky" style="width:50px;height:50px;background:red;"></div>
  </div>
</div>
`,
    `
<div style="background:#eee;height:100px;overflow:auto;position:relative;">
  <div style="height:1000px;">
    <p>This is a text</p>
    <div class="ic-sticky" style="top:10px;width:50px;height:50px;background:red;"></div>
  </div>
</div>
`,
  ]);
