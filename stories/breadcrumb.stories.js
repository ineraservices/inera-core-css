import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Breadcrumbs",
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* Breadcrums should not be used for small screens. Apply for example `.iu-hide-sm` on the `<nav>` element.",
          "* Remember to add a `aria-label` or `aria-describedby` attribute.",
          '* Remember to add `aria-current="location"` attribute.',
        ].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<nav aria-label="du är här" class="ic-breadcrumb">
  <ol>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">Start</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">En undersida</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a aria-current="location">Rubrik</a>
    </li>
  </ol>
</nav>
`,
    `
<nav dir="rtl" aria-label="du är här" class="ic-breadcrumb">
  <ol>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">Start</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">En undersida</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a aria-current="location">Rubrik</a>
    </li>
  </ol>
</nav>
`,
  ]);
export const WithPrefix = () =>
  markupWithCodeExample([
    `
<nav aria-label="du är här" class="ic-breadcrumb">
  <span aria-hidden="true" class="ic-breadcrumb__label">Du är här: </span>
  <ol>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">Start</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">En undersida</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a aria-current="location">Rubrik</a>
    </li>
  </ol>
</nav>
`,
    `
<nav dir="rtl" aria-label="du är här" class="ic-breadcrumb">
  <span aria-hidden="true" class="ic-breadcrumb__label">Du är här: </span>
  <ol>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">Start</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a href="javascript:void(0)">En undersida</a>
    </li>
    <li class="ic-breadcrumb__item">
      <a aria-current="location">Rubrik</a>
    </li>
  </ol>
</nav>
`,
  ]);
