import { storiesOf } from "@storybook/html";
import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

const ExpandableBlank = () =>
  markupWithCodeExample([
`<details class="ic-expandable">
  <summary class="ic-expandable-button iu-focus">Rubrik</summary>
  <p>Some hidden content</p>
  <p>This is a <a href="javascript:void(0)">a link</a></p>
  <p>Focus on the link should not be possible when closed.</p>
</details>`,

`<details class="ic-expandable" open>
  <summary class="ic-expandable-button iu-focus">Rubrik</summary>
  <p>Some hidden content</p>
  <p>This is a <a href="javascript:void(0)">a link</a></p>
  <p>Focus on the link should not be possible when closed.</p>
</details>`
  ]);

const ExpandableCard = () =>
  markupWithCodeExample([
`<details class="ic-card ic-expandable-card ic-card--expandable ic-card--sm-unset-style ic-expandable ic-card--inspiration-large">
  <summary class="ic-expandable-button iu-focuscard ic-expandable-button--chevron">Rubrik</summary>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
</details>`,   
    
`<details class="ic-card ic-expandable-card ic-card--expandable ic-card--sm-unset-style ic-expandable ic-card--inspiration-large" open>
  <summary class="ic-expandable-button iu-focuscard">Rubrik</summary>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
  <p>Some hidden content</p>
</details>`,
  ]);

  const ExpandableSection = () =>
  markupWithCodeExample([
    `<ul class="ic-expandables-group">
      <li class="">
        <details class="ic-expandable iu-color-cta">
          <summary class="ic-expandable-button iu-focus ic-expandable-button--plusminus-circle">Rubrik</summary>
          <p>Hello world</p>
        </details>
      </li>
      <li class="">
        <details class="ic-expandable iu-color-cta" open>
          <summary class="ic-expandable-button iu-focus ic-expandable-button--plusminus-circle">Rubrik</summary>
          <p>Hello world</p>
        </details>
      </li>
    </ul>`,
  ]);

const ExpandableAlternative = () =>
  markupWithCodeExample([
    `<details class="ic-expandable-alt">
    <summary class="ic-expandable-button iu-focus">Rubrik</summary>
    <p>Some hidden content</p>
    <p>This is a <a href="javascript:void(0)">a link</a></p>
    <p>Focus on the link should not be possible when closed.</p>
  </details>`,
  
  `<details class="ic-expandable-alt" open>
    <summary class="ic-expandable-button iu-focus">Rubrik</summary>
    <p>Some hidden content</p>
    <p>This is a <a href="javascript:void(0)">a link</a></p>
    <p>Focus on the link should not be possible when closed.</p>
  </details>`
]);

storiesOf("components/Expandables", module)
  .addDecorator(wrapStoryInContainer)
  .addParameters({
    docs: {
      description: {
        component: [''].join("\r\n"),
      },
    },
    status: "",
  })
  .add("Blank", () => {
    return ExpandableBlank();
  })
  .add("Card", () => {
    return ExpandableCard();
  })
  .add("Section", () => {
    return ExpandableSection();
  })
  .add("Alternative", () => {
    return ExpandableAlternative();
  });
