import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Notifications",
  parameters: {
    status: "",
    docs: {
      description: {
        component: [""].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    '<span class="ic-notification">0</span>',
    '<span class="ic-notification">2</span>',
    '<span class="ic-notification">7</span>',
    '<span class="ic-notification">99</span>',
    '<span class="ic-notification">9999</span>',
    '<span class="ic-notification"></span>',
    '<span class="ic-notification-outline">0</span>',
    '<span class="ic-notification-outline">2</span>',
    '<span class="ic-notification-outline">7</span>',
    '<span class="ic-notification-outline">99</span>',
    '<span class="ic-notification-outline">9999</span>',
    '<span class="ic-notification-outline"></span>',
  ]);

export const WithUtils = () =>
  markupWithCodeExample([
    '<span class="ic-notification iu-fs-sm">10</span>',
    '<span class="ic-notification iu-fs-md">10</span>',
    '<span class="ic-notification iu-fs-lg">10</span>',
  ]);
