import { markupWithCodeExample } from "./helpers";

export default {
  title: "utilities/helpers",
  parameters: {
    status: "",
  },
};

export const Layout = () =>
  markupWithCodeExample([
    `
<div class="iu-bg-secondary-light">
  <div class="iu-fullwidth">width: 100%</div>
</div>`,
    `
<div class="iu-flex iu-flex-column">
  <div class="iu-order-99">Last</div>
  <div class="iu-order-minus-1">First</div>
  <div class="iu-order-1">Order 1</div>
  <div class="iu-order-0">Order 0</div>
</div>`,
    `
<div><small>Large screen only</small></div>
<div class="iu-flex iu-flex-column">
  <div class="iu-order-99--lg">Last</div>
  <div class="iu-order-minus-1--lg">First</div>
  <div class="iu-order-1--lg">Order 1</div>
  <div class="iu-order-0--lg">Order 0</div>
</div>`,
    `
<div class="iu-flex">
  <div class="iu-grow iu-bg-muted iu-color-white">Wide</div>
  <div>Narrow</div>
</div>`,
    `
<div class="ic-card iu-flex-center" style="width: 200px; height: 200px">
  <p>centered</p>
</div>`,
    `
<table class="iu-fullwidth--sm iu-border-muted">
  <tr><td>Will have fullwidth on small device</td></tr>
</table>
`,
    `
<table class="iu-fullwidth--md iu-border-muted">
  <tr><td>Will have fullwidth from medium devices</td></tr>
</table>
`,
    `
<table class="iu-fullwidth--lg iu-border-muted">
  <tr><td>Will have fullwidth from large devices</td></tr>
</table>
`,
  ]);

export const Accessability = () =>
  markupWithCodeExample([
    `
<div class="ic-card">
  <div aria-hidden="true">not visible for screen readers.</div>
  <div class="iu-sr-only">only visible for screen readers.</div>
</div>`,
  ]);

export const Printing = () =>
  markupWithCodeExample([
    `
<ul class="iu-avoid-page-break-inside">
  <li>Printing</li>
  <li>Should</li>
  <li>Not</li>
  <li>Break</li>
  <li>This</li>
  <li>List</li>
</ul>`,
    `
<h2>This is a headline.</h2>
<div class="iu-always-page-break-before">
  <h2>This should be on a new page when printing.</h2>
</div>`,
    `<div class="iu-hide-for-print">
  This should not be visible when printing
</div>`,
  ]);

export const Opacity = () =>
  markupWithCodeExample([
    `
<div class="iu-bg-main iu-p-300">
    <div class="iu-opacity-100 iu-bg-white">opacity: 100</div>
    <div class="iu-opacity-50 iu-bg-white">opacity: 50</div>
    <div class="iu-opacity-0 iu-bg-white">opacity: 0</div>
</div>`,
  ]);

export const Float = () =>
  markupWithCodeExample([
    `
<div class="iu-text-justify">
  <div class="iu-fl iu-color-white iu-mr-500 iu-bg-muted">float-left</div>
  <div class="iu-fr iu-color-white iu-ml-500 iu-bg-muted">float-right</div>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et turpis vel justo dignissim viverra. Phasellus luctus, velit in lobortis sodales, massa sem fringilla felis, et ultricies elit mauris nec neque. Praesent venenatis ultricies turpis, interdum scelerisque odio egestas euismod.
</div>`,
  ]);

export const Text = () =>
  markupWithCodeExample([
    `<div class="iu-text-center">center</div>`,
    `<div class="iu-text-left">left</div>`,
    `<div class="iu-text-right">right</div>`,
  ]);

export const Resize = () =>
  markupWithCodeExample([
    `<div class="iu-bg-information-light iu-resize">Resize me</div>`,
    `<div class="iu-bg-information-light iu-resize-vertical">Vertical resize</div>`,
    `<div class="iu-bg-information-light iu-resize-horizontal">Horizontal resize</div>`,
    `<textarea class="iu-no-resize">Disable resize</textarea>`,
  ]);
