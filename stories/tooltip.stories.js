import { storiesOf } from "@storybook/html";
import { useEffect } from "@storybook/client-api";
import { markupWithCodeExample } from "./helpers";

const params = {
  status: "",
  docs: {
    description: {
      component: [
        "* Se till så att en öppen tooltip behåller fokus i elementet. Det ska inte gå att tabba sig utanför tooltip.",
        "* Tooltip ska gå att stänga med hjälp av ESC knappen.",
      ].join("\r\n"),
    },
  },
};

function tooltips() {
  let buttons = document.querySelectorAll("[data-tooltip]");
  let tooltips = [];
  let activeTooltip = null;

  function toggleTooltip(item, button) {
    let isOpen = item.classList.contains("ic-tooltip--open");
    button.setAttribute("aria-expanded", !isOpen);
    item.classList.toggle("ic-tooltip--open");

    if (isOpen) {
      if (typeof item.close === "function") {
        item.close();
      } else {
        item.removeAttribute("open");
        item.removeAttribute("tabindex");
      }
      if (activeTooltip === item) {
        activeTooltip = null;
      }
      button.focus();
    } else {
      if (typeof item.show === "function") {
        item.show();
      } else {
        let closeBtn = item.querySelector("[data-close=tooltip]");
        if (closeBtn) {
          closeBtn.focus();
        } else {
          item.setAttribute("tabindex", -1);
          item.focus();
        }
        item.setAttribute("open", true);
      }
      activeTooltip = item;
    }
  }

  function closeTooltips() {
    for (let tooltip of tooltips) {
      let button = document.querySelector(`[data-tooltip="${tooltip.id}"]`);
      if (tooltip.classList.contains("ic-tooltip--open")) {
        toggleTooltip(tooltip, button);
      }
    }
  }

  function closeTooltipOnEscape(event) {
    if (event.key === "Escape") {
      if (activeTooltip !== null) {
        toggleTooltip(
          activeTooltip,
          document.querySelector(`[data-tooltip="${activeTooltip.id}"]`)
        );
        activeTooltip = null;
      } else if (
        tooltips.some((tooltip) =>
          tooltip.classList.contains("ic-tooltip--open")
        )
      ) {
        closeTooltips();
      }
    }
  }

  function setTooltipActive(item) {
    activeTooltip = item;
  }

  for (let button of buttons) {
    let item = document.getElementById(button.dataset.tooltip);
    tooltips.push(item);
    let closeBtn = item.querySelector("[data-close=tooltip]");

    if (closeBtn) closeBtn.onclick = () => toggleTooltip(item, button);
    button.onclick = () => {
      setTooltipActive(item);
      toggleTooltip(item, button);
    };
  }

  document.addEventListener("keydown", closeTooltipOnEscape);
}

const Script = () =>
  markupWithCodeExample(`
<script>
function tooltips() {
  // Find all elements with data-tooltip attribute.
  let buttons = document.querySelectorAll("[data-tooltip]");

  // Array to store all tooltips
  let tooltips = [];
  // Track the active tooltip
  let activeTooltip = null;

  // Use this for toggle and close button.
  function toggleTooltip(item, button) {
    let isOpen = item.classList.contains("ic-tooltip--open");

    // Invert the aria-expanded value for the toggle button.
    button.setAttribute("aria-expanded", !isOpen);
    // Toggle the open class
    item.classList.toggle("ic-tooltip--open");

    if (isOpen) {
      // Check if dialog api is available
      if (typeof item.close === "function") {
        item.close();
      } else {
        item.removeAttribute("open");
        item.removeAttribute("tabindex");
      }
      if (activeTooltip === item) {
        // Clear the active tooltip
        activeTooltip = null;
      }
      button.focus();
    } else {
      // Check if dialog api is available
      if (typeof item.show === "function") {
        item.show();
      } else {
        let closeBtn = item.querySelector("[data-close=tooltip]");
        if (closeBtn) {
          closeBtn.focus();
        } else {
          item.setAttribute("tabindex", -1);
          item.focus();
        }
        item.setAttribute("open", true);
      }
      // Set the current tooltip as active
      activeTooltip = item;
    }
  }

  function closeTooltips() {
    for (let tooltip of tooltips) {
      let button = document.querySelector(\`[data-tooltip="\${tooltip.id}"]\`);
      if (tooltip.classList.contains("ic-tooltip--open")) {
        toggleTooltip(tooltip, button);
      }
    }
  }

  function closeTooltipOnEscape(event) {
    if (event.key === "Escape") {
      if (activeTooltip !== null) {
        toggleTooltip(
          activeTooltip,
          document.querySelector(\`[data-tooltip="\${activeTooltip.id}"]\`)
        );
        activeTooltip = null;
      } else if (
        tooltips.some((tooltip) =>
          tooltip.classList.contains("ic-tooltip--open")
        )
      ) {
        closeTooltips();
      }
    }
  }

  function setTooltipActive(item) {
    activeTooltip = item;
  }

  // Iterate over them.
  for (let button of buttons) {
    // Get the tooltip item
    let item = document.getElementById(button.dataset.tooltip);
    // Store each tooltip in the array
    tooltips.push(item);
    // Find the close button
    let closeBtn = item.querySelector("[data-close=tooltip]");

    // Bind the toggle function to the buttons.
    if (closeBtn) closeBtn.onclick = () => toggleTooltip(item, button);
    button.onclick = () => {
      // Set the clicked tooltip as active
      setTooltipActive(item);
      toggleTooltip(item, button);
    };
  }

  document.addEventListener("keydown", closeTooltipOnEscape);
}
</script>
`);

const PopOver = () =>
  markupWithCodeExample([
    `
<div role="dialog" class="ic-popover">
  <p>Common styling for modal and tooltip component.</p>
</div>
`,
  ]);

const Tooltip = () =>
  markupWithCodeExample([
    `
<div>

<button data-tooltip="tooltip-container-1" aria-expanded="true" type="button">Toggle tooltip</button>

<dialog open role="tooltip" id="tooltip-container-1" class="ic-tooltip ic-tooltip--open" aria-describedby="tooltip-1" style="width: 18.75rem;">
  <button data-close="tooltip" type="button" aria-label="Stäng" class="ic-tooltip__close iu-svg-icon">
    <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path fill="currentColor" d="M12 10.733l5.07-5.07c.35-.35.917-.35 1.267 0 .35.35.35.917 0 1.267L13.267 12l5.07 5.07c.35.35.35.917 0 1.267-.35.35-.917.35-1.267 0L12 13.267l-5.07 5.07c-.35.35-.917.35-1.267 0-.35-.35-.35-.917 0-1.267l5.07-5.07-5.07-5.07c-.35-.35-.35-.917 0-1.267.35-.35.917-.35 1.267 0l5.07 5.07z" transform="translate(-994 -650) translate(410 637) translate(584 13)"/>
    </svg>
  </button>
  <h2 id="tooltip-1" class="ic-tooltip__caption iu-fw-body ic-small-text ic-light-text" tabindex="0">
    Caption
  </h2>
  <div class="ic-text" tabindex="0">
    <h3 class="h4 ic-tooltip__head">
      Headline
    </h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac lacus nec odio iaculis consectetur. Curabitur id rutrum libero, non finibus ex. Duis sollicitudin elit eget lectus tempor mollis. Nulla facilisi. Aliquam felis diam, sodales et tempus ultricies, molestie et ex.</p>
  </div>
</dialog>

</div><!-- container -->`,
    `
<div>

<button data-tooltip="tooltip-container-2" aria-expanded="true" type="button">Toggle tooltip</button>

<dialog open role="tooltip" id="tooltip-container-2" class="ic-tooltip ic-tooltip--open ic-tooltip--has-scroll" aria-describedby="tooltip-2" style="width: 18.75rem;">
  <button data-close="tooltip" type="button" aria-label="Stäng" class="ic-tooltip__close iu-svg-icon">
    <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path fill="currentColor" d="M12 10.733l5.07-5.07c.35-.35.917-.35 1.267 0 .35.35.35.917 0 1.267L13.267 12l5.07 5.07c.35.35.35.917 0 1.267-.35.35-.917.35-1.267 0L12 13.267l-5.07 5.07c-.35.35-.917.35-1.267 0-.35-.35-.35-.917 0-1.267l5.07-5.07-5.07-5.07c-.35-.35-.35-.917 0-1.267.35-.35.917-.35 1.267 0l5.07 5.07z" transform="translate(-994 -650) translate(410 637) translate(584 13)"/>
    </svg>
  </button>
  <h2 id="tooltip-2" class="ic-tooltip__caption iu-fw-body ic-small-text ic-light-text" tabindex="0">
    Tooltip with scroll
  </h2>
  <div style="max-height: 200px;" class="ic-text ic-tooltip__scroll" tabindex="0">
    <h3 class="h4 ic-tooltip__head">
      Headline
    </h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac lacus nec odio iaculis consectetur. Curabitur id rutrum libero, non finibus ex. Duis sollicitudin elit eget lectus tempor mollis. Nulla facilisi. Aliquam felis diam, sodales et tempus ultricies, molestie et ex.</p>
  </div>
</dialog>

</div><!-- / container -->
`,
  ]);

const TooltipAlt = () =>
  markupWithCodeExample([
    `
<button data-tooltip="tooltip-container-3" aria-expanded="true" type="button">Toggle tooltip</button>

<dialog role="tooltip" id="tooltip-container-3" open class="ic-tooltip--open ic-tooltip-alt" aria-describedby="tooltip-3" style="width: 18.75rem;">
  <button data-close="tooltip" type="button" aria-label="Stäng" class="ic-tooltip__close iu-svg-icon">
    <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path fill="currentColor" d="M12 10.733l5.07-5.07c.35-.35.917-.35 1.267 0 .35.35.35.917 0 1.267L13.267 12l5.07 5.07c.35.35.35.917 0 1.267-.35.35-.917.35-1.267 0L12 13.267l-5.07 5.07c-.35.35-.917.35-1.267 0-.35-.35-.35-.917 0-1.267l5.07-5.07-5.07-5.07c-.35-.35-.35-.917 0-1.267.35-.35.917-.35 1.267 0l5.07 5.07z" transform="translate(-994 -650) translate(410 637) translate(584 13)"/>
    </svg>
  </button>
  <div class="ic-text" tabindex="0">
    <h3 id="tooltip-3" class="h4 ic-tooltip__head">
      Headline
    </h3>
    <p>Lorem ipsum dolor sit amet, consectetur.</p>
  </div>
  <p><a href="javascript:void(0)">And a link</a></p>
</dialog>`,
  ]);

storiesOf("components/Tooltip", module)
  .addParameters(params)
  .add("Popover", PopOver)
  .add("Tooltip", () => {
    useEffect(() => {
      tooltips();
    }, []);
    return Tooltip();
  })
  .add("Tooltip Alt", () => {
    useEffect(() => {
      tooltips();
    }, []);
    return TooltipAlt();
  })
  .add("Script", Script);
