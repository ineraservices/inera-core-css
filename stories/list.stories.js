import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Lists",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "Lists can be used for example for search hits and news items.",
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
    <ul class="ic-block-list">
    <li class="ic-block-list__item ic-text ic-card--link">
      <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
      </li>
`,
  ]);

export const Linked = () =>
  markupWithCodeExample([
    `
<ul class="ic-block-list">
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a href="#" class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
    <div> <em> Publicerat: 2020-05-17  &nbsp; &nbsp; Område: Lorem ipsum dolor </em> </div>
    </li>
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a href="#" class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
    <div> <em> Publicerat: 2020-05-17  &nbsp; &nbsp; Område: Lorem ipsum dolor </em> </div>
    </li>
  <li class="ic-block-list__item ic-text ic-card--link">
    <h3><a href="#" class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
    <div> <em> Publicerat: 2020-05-17  &nbsp; &nbsp; Område: Lorem ipsum dolor </em> </div>
    </li>
</ul>
`,
  ]);

export const NewsList = () =>
  markupWithCodeExample([
    `
<ul class="ic-block-list ic-block-list--striped">
  <li class="ic-block-list__item ic-card--link">

    <article class="ic-news-list-item">
      <time datetime="2024-12-24" class="ic-date-label ic-news-list-item__date">
      <span class="ic-date-label__day">24</span>
      <span class="ic-date-label__month">dec</span>
      <span class="ic-date-label__year">2024</span>
      </time>
      <div class="ic-news-list-item__body">
      <div class="ids-list-item__date">24 DEC 2024</div>
        <header>
          <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
        </header>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut...
        </p>
        <div class="ic-published"> <em> Publicerat: 2020-05-17  &nbsp; &nbsp; Område: Lorem ipsum dolor </em> </div>
      </div>
    </article>

  </li>
  <li class="ic-block-list__item ic-card--link">

    <article class="ic-news-list-item">
      <time datetime="2019-08-30" class="ic-date-label ic-news-list-item__date">
      <span class="ic-date-label__day">24</span>
      <span class="ic-date-label__month">dec</span>
      <span class="ic-date-label__year">2024</span>
      </time>
      <div class="ic-news-list-item__body">
      <div class="ids-list-item__date">24 DEC 2024</div>
        <header>
          <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
        </header>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
        <div class="ic-published"> <em> Publicerat: 2020-05-17  &nbsp; &nbsp; Område: Lorem ipsum dolor </em> </div>
      </div>
    </article>

  </li>
  <li class="ic-block-list__item ic-card--link">
    <article class="ic-news-list-item">
      <time datetime="2019-08-30" class="ic-date-label ic-news-list-item__date">
        <span class="ic-date-label__day">24</span>
        <span class="ic-date-label__month">dec</span>
        <span class="ic-date-label__year">2024</span>
      </time>
      <div class="ic-news-list-item__body">
      <div class="ids-list-item__date">24 DEC 2024</div>
        <header>
          <h3><a class="iu-no-underline ic-card-link" href="javascript:void(0)">Headline</a></h3>
        </header>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut…</p>
        <div class="ic-published"> <em> Publicerat: 2020-05-17  &nbsp; &nbsp; Område: Lorem ipsum dolor </em> </div>
        </div>
    </article>

  </li>
</ul>
`,
  ]);
