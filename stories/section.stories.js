import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Sections",
  parameters: {
    status: "",
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<section class="ic-section">
  <h1>I'm a section headline</h1>
</section>
`,
  ]);

export const WithDivider = () =>
  markupWithCodeExample([
    `
<section class="ic-section ic-section--divider ic-text">
  <p>I'm a section content</p>
</section>
`,
  ]);

export const Spacing = () =>
  markupWithCodeExample([
    `
<section class="ic-section ic-text">
  <p>I'm the first section</p>
</section>
<section class="ic-section ic-section--divider ic-section--no-padding ic-section--no-margin ic-text">
  <p>I'm a section without space</p>
</section>
<section class="ic-section ic-section--divider ic-section--no-padding ic-section--no-margin ic-text">
  <p>I'm a section without space</p>
</section>
<section class="ic-section ic-text ic-section--divider">
  <p>I'm a section with space</p>
</section>
<section class="ic-section ic-section--divider ic-section--extra-padding ic-text">
  <p>I'm a section with extra space</p>
</section>
`,
  ]);

export const WithHeader = () =>
  markupWithCodeExample([
    `
<section class="ic-section">

  <header class="ic-section-header">
    <h2 class="ic-section-headline">
      Default headline in section header
    </h2>
  </header>

</section>
`,
    `
<section class="ic-section ic-section--divider">

  <header class="ic-section-header">
    <h2 class="ic-section-headline ic-section-headline--noborder">
      Headline no border. Section has divider.
    </h2>
  </header>

</section>
`,
    `<section class="ic-section ic-section--divider">

  <header class="ic-section-header">
    <h2 class="ic-section-headline ic-section-headline--border">
      Headline with border. Section has divider.
    </h2>
  </header>

</section>
`,
  ]);

export const Intro = () =>
  markupWithCodeExample(`
<div class="ic-container">

  <section class="ic-section ic-section-intro">
    <div>
      <h1>This is a page</h1>
      <p class="ic-preamble">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus est nec enim facilisis faucibus. Sed aliquet ac ante et ornare. Nulla tempus bibendum lorem id congue. Proin nec ante dolor. Etiam convallis vulputate dolor nec finibus. </p>
    </div>
    <figure><img src="https://via.placeholder.com/400x300" alt="placeholder image" /></figure>
  </section>

</div>
`);
