import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Badge",
  parameters: {
    status: "",
  },
};

export const Default = () =>
  markupWithCodeExample([
    '<span class="ic-badge">This is a badge</span>',
    '<span class="ic-badge ic-badge--small">This is a badge</span>',
  ]);

export const Status = () =>
  markupWithCodeExample([
    '<span class="ic-badge-status ic-badge-color-main">Main</span>',
    '<span class="ic-badge-status ic-badge-color-main ic-badge--small">Main</span>',
  ]);

export const Variants = () =>
  markupWithCodeExample([
    `<div class="ic-badge-container">
        <span class="ic-badge-status ic-badge-color-main">Main</span>
        <span class="ic-badge-status ic-badge-color-neutral">Neutral</span>
        <span class="ic-badge-status ic-badge-color-info">Info</span>
        <span class="ic-badge-status ic-badge-color-attention">Attention</span>
        <span class="ic-badge-status ic-badge-color-success">Success</span>
        <span class="ic-badge-status ic-badge-color-error">Error</span>
        <span class="ic-badge-status ic-badge-color-secondary">Secondary</span>
    </div>
    `,
    ` <div class="ic-badge-container">
        <span class="ic-badge-status ic-badge-color-main ic-badge--small">Main</span>
        <span class="ic-badge-status ic-badge-color-neutral ic-badge--small">Neutral</span>
        <span class="ic-badge-status ic-badge-color-info ic-badge--small">Info</span>
        <span class="ic-badge-status ic-badge-color-attention ic-badge--small">Attention</span>
        <span class="ic-badge-status ic-badge-color-success ic-badge--small">Success</span>
        <span class="ic-badge-status ic-badge-color-error ic-badge--small">Error</span>
        <span class="ic-badge-status ic-badge-color-secondary ic-badge--small">Secondary</span>
      </div>
    `,
  ]);

export const Regional = () =>
  markupWithCodeExample([
    '<span class="ic-badge">Innehållet gäller Stockholms län</span>',
  ]);
