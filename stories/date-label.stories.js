import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Date Label",
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* use `ic-date-label--small` to force small device look for desktop size",
          "* remember to add `datetime` attribute in a [valid](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time#Valid_datetime_Values) format.",
        ].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<time datetime="2019-08-30" class="ic-date-label">
  <span class="ic-date-label__day">30</span>
  <span class="ic-date-label__month">aug</span>
  <span class="ic-date-label__year">2019</span>
</time>
`,
    `
<time datetime="2019-08-30" class="ic-date-label ic-date-label--small">
  <span class="ic-date-label__day">30</span>
  <span class="ic-date-label__month">aug</span>
  <span class="ic-date-label__year">2019</span>
</time>
`,
  ]);
