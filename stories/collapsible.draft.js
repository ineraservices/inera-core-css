/* THIS IS NOT READY AND NOT A PRIORITY */

import { markupWithCodeExample, bleedContainer } from "./helpers";
import mobileMenu from "./html/mobile-menu.html";

export default {
  title: "interactive/Collapsible Panels",
  parameters: {
    status: "",
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<div>
  <h2 data-collapsible>
    <button aria-expanded="false">This is text</button>
  </h2>
  <div hidden>
    Hidden content
  </div>
</div>`,
  ]);
