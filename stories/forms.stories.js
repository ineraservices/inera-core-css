import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Forms",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* Remember to always have a label with a for attribute to the correct form element.",
          "* Remember test your forms with keyboard navigation.",
          "* Make sure you connect an error message with the correct field, use `aria-describedby` for example",
          '* Consider if you should use for example aria-label="Loading..." when displaying the spinner.',
        ].join("\r\n"),
      },
    },
  },
};

export const Radio = () =>
  markupWithCodeExample([
    `
<div
  role="radiogroup"
  aria-label="Label for radio group"
  class="ic-radio-group-horizontal"
>
  <input
    type="radio"
    id="demo_radio_hor_1"
    name="demo_hor_radio"
    value="1"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_1">Horisontell grupp radioknapp 1</label>

  <input
    checked="checked"
    type="radio"
    id="demo_radio_hor_2"
    name="demo_hor_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_2">Horisontell grupp radioknapp 2 med en längre text beskrivning</label>
</div>
`,
    `
<div
  role="radiogroup"
  aria-label="Label for radio group"
  class="ic-radio-group-vertical"
>
  <input
    type="radio"
    id="demo_radio_ver_1"
    name="demo_vert_radio"
    value="1"
    class="ic-forms__radio"
  />
  <label for="demo_radio_ver_1">Vertikal grupp radioknapp 1</label>

  <input
    checked="checked"
    type="radio"
    id="demo_radio_ver_2"
    name="demo_vert_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_ver_2">Vertikal grupp radioknapp 2</label>
</div>
`,
    `
<div
  role="radiogroup"
  aria-label="Label for radio group"
  dir="rtl"
  class="ic-radio-group-horizontal"
>
  <input
    type="radio"
    id="demo_radio_rtl_1"
    name="demo_radio_rtl"
    value="1"
    class="ic-forms__radio"
  />
  <label for="demo_radio_rtl_1">Textriktning RTL 1</label>

  <input
    type="radio"
    id="demo_radio_rtl_2"
    name="demo_radio_rtl"
    value="1"
    class="ic-forms__radio"
  />
  <label for="demo_radio_rtl_2">Textriktning RTL 2</label>
</div>
`,
    `<input
  disabled
  type="radio"
  id="demo_radio_disabled"
  name="demo_radio_disabled"
  value="1"
  class="ic-forms__radio"
/>
<label for="demo_radio_disabled">Radioknapp 1</label>`,
    `
<div
  role="radiogroup"
  aria-label="Label for radio group"
  class="ic-radio-group-horizontal"
>
  <input
    type="radio"
    id="demo_radio_hor_1"
    name="demo_hor_radio"
    value="1"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_1">Make sure the items wrap</label>
  <input
    checked="checked"
    type="radio"
    id="demo_radio_hor_2"
    name="demo_hor_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_2">Make sure the items wrap</label>
  <input
    checked="checked"
    type="radio"
    id="demo_radio_hor_2"
    name="demo_hor_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_2">Make sure the items wrap</label>
  <input
    checked="checked"
    type="radio"
    id="demo_radio_hor_2"
    name="demo_hor_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_2">Make sure the items wrap</label>
  <input
    checked="checked"
    type="radio"
    id="demo_radio_hor_2"
    name="demo_hor_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_2">Make sure the items wrap</label>
  <input
    checked="checked"
    type="radio"
    id="demo_radio_hor_2"
    name="demo_hor_radio"
    value="2"
    class="ic-forms__radio"
  />
  <label for="demo_radio_hor_2">Make sure the items wrap</label>
</div>
`,
  ]);
export const Checkbox = () =>
  markupWithCodeExample([
    `
<div
  role="group"
  aria-label="Label for checkbox group"
  class="ic-checkbox-group-horizontal"
>
  <input
    class="ic-forms__checkbox"
    type="checkbox"
    id="demo_check_hor_1"
    name="demo_check_a"
    value="1"
  />
  <label for="demo_check_hor_1">Checkbox 1</label>

  <input
    class="ic-forms__checkbox"
    type="checkbox"
    checked
    id="demo_check_hor_2"
    name="demo_check_a"
    value="2"
  />
  <label for="demo_check_hor_2">Checkbox 2 med en mycket längre text beskrivning</label>
</div>
`,
    `
<div
  role="group"
  aria-label="Label for checkbox group"
  class="ic-checkbox-group-vertical"
>
  <input
    class="ic-forms__checkbox"
    type="checkbox"
    id="demo_check_vert_1"
    name="demo_check_b"
    value="1"
  />
  <label for="demo_check_vert_1"> Checkbox 1 </label>

  <input
    class="ic-forms__checkbox"
    type="checkbox"
    checked
    id="demo_check_vert_2"
    name="demo_check_b"
    value="2"
  />
  <label for="demo_check_vert_2"> Checkbox 2 </label>
</div>
`,
    `
<div
  dir="rtl"
  role="group"
  aria-label="Label for checkbox group"
  class="ic-checkbox-group-horizontal"
>
  <input
    class="ic-forms__checkbox"
    type="checkbox"
    id="demo_check_rtl_1"
    name="demo_check_c"
    value="1"
  />
  <label for="demo_check_rtl_1"> Checkbox RTL 1 </label>

  <input
    class="ic-forms__checkbox"
    type="checkbox"
    checked
    id="demo_check_rtl_2"
    name="demo_check_c"
    value="2"
  />
  <label for="demo_check_rtl_2"> Checkbox RTL 2 </label>
</div>
`,
    `<input
  disabled
  class="ic-forms__checkbox"
  type="checkbox"
  id="demo_check_disabled_1"
  name="demo_check_d"
  value="3"
/>
<label for="demo_check_disabled_1"> Disabled checkbox unchecked </label>`,
    `<input
  disabled
  checked
  class="ic-forms__checkbox"
  type="checkbox"
  id="demo_check_disabled_2"
  name="demo_check_d"
  value="4"
/>
<label for="demo_check_disabled_2"> Disabled checkbox checked </label>`,
  ]);
const textfields = [
  `<input class="ic-textfield" type="text" />`,
  `<input
  placeholder="With placeholder"
  class="ic-textfield"
  type="text"
/>`,
  `<input disabled class="ic-textfield" type="text" />`,
  `<input
  value="Ej skrivbart fält"
  readonly
  class="ic-textfield"
  type="text"
/>`,
  `<input class="ic-textfield ic-textfield--disabled" type="text" />`,
  `<input
  value="Något fel"
  class="ic-textfield ic-textfield--error"
  type="text"
/>

<p id="error-1" class="ic-forms__error-message">
  <span class="iu-svg-icon ic-textfield--icon">
    <svg
      width="40"
      height="40"
      viewBox="0 0 40 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      aria-hidden="true"
    >
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M.342 34.218 18.056 3.174c.674-1.183 2.13-1.54 3.212-.758.274.198.504.457.676.758l17.714 31.044c.645 1.13.355 2.623-.676 3.366-.374.27-.815.416-1.268.416H2.286C1.003 38 0 36.873 0 35.523c0-.46.118-.912.342-1.305Zm20.283-30.29a.875.875 0 0 0-.245-.278l-.085-.053c-.321-.17-.704-.048-.92.331L1.66 34.973a1.11 1.11 0 0 0-.142.55c0 .546.364.956.767.956h35.428a.648.648 0 0 0 .38-.129c.389-.28.511-.911.245-1.377L20.625 3.928Z"
        fill="var(--icon-alert2)"
      />
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M20.381 25.667a.384.384 0 0 1-.761 0L18.106 14.18c-.07-.516-.106-.9-.106-1.153 0-.613.198-1.104.593-1.473a2.01 2.01 0 0 1 1.425-.555 1.95 1.95 0 0 1 1.407.555c.383.37.575.919.575 1.648 0 .234-.024.56-.07.978l-1.55 11.487Zm1.033 6.745A1.92 1.92 0 0 1 20 33a1.923 1.923 0 0 1-1.414-.588A1.94 1.94 0 0 1 18 30.99c0-.543.195-1.01.586-1.403A1.92 1.92 0 0 1 20 28.999a1.92 1.92 0 0 1 1.414.588c.39.392.586.86.586 1.403a1.94 1.94 0 0 1-.586 1.42Z"
        fill="var(--icon-alert)"
      />
    </svg>
  </span>
  Här finns plats för text till felmeddelande.
</p>
`,
  `<label class="ic-forms__label" for="form_field_with_label"
  >With label</label
>
<input id="form_field_with_label" class="ic-textfield" type="text" />`,
];
export const Textfield = () => markupWithCodeExample(textfields);
const textareas = [
  `<textarea class="ic-textarea">Textfield</textarea>`,
  `<textarea
  class="ic-textarea iu-no-resize"
>
No resizing (iu-no-resize)</textarea
>`,
  `<textarea class="ic-textarea">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce facilisis, magna malesuada pellentesque molestie, sem nisi viverra ipsum, vel dictum purus eros ac nunc. Morbi sit amet elit maximus, scelerisque ante ullamcorper, venenatis erat. Sed et arcu eros. Nam eu est in tortor elementum rhoncus non eget metus. Mauris sem mauris, lobortis vel enim a, mattis sodales lacus. Suspendisse consequat odio sit amet dolor aliquam, sed facilisis risus facilisis. Aenean tempus, augue sed ultricies dapibus, ex est venenatis odio, sit amet placerat odio felis id lorem. Donec nec enim gravida, accumsan risus non, imperdiet elit. Curabitur non orci sodales, pretium tellus sit amet, volutpat velit. Vivamus et volutpat ante, id rhoncus nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque efficitur eros molestie rhoncus venenatis. Praesent vitae dolor sapien. Maecenas eu lacus dui.</textarea
>`,
  `<textarea placeholder="With placeholder" class="ic-textarea"></textarea>`,
  `<textarea disabled class="ic-textarea"></textarea>`,
  `<textarea
  readonly
  class="ic-textarea"
></textarea
>`,
  `<textarea class="ic-textarea ic-textarea--disabled"></textarea>`,
  `<textarea class="ic-textarea ic-textarea--error">Error</textarea>
<p id="error-1" class="ic-forms__error-message">
  <span class="iu-svg-icon ic-textfield--icon">
    <svg
      width="40"
      height="40"
      viewBox="0 0 40 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      aria-hidden="true"
    >
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M.342 34.218 18.056 3.174c.674-1.183 2.13-1.54 3.212-.758.274.198.504.457.676.758l17.714 31.044c.645 1.13.355 2.623-.676 3.366-.374.27-.815.416-1.268.416H2.286C1.003 38 0 36.873 0 35.523c0-.46.118-.912.342-1.305Zm20.283-30.29a.875.875 0 0 0-.245-.278l-.085-.053c-.321-.17-.704-.048-.92.331L1.66 34.973a1.11 1.11 0 0 0-.142.55c0 .546.364.956.767.956h35.428a.648.648 0 0 0 .38-.129c.389-.28.511-.911.245-1.377L20.625 3.928Z"
        fill="var(--icon-color2)"
      />
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M20.381 25.667a.384.384 0 0 1-.761 0L18.106 14.18c-.07-.516-.106-.9-.106-1.153 0-.613.198-1.104.593-1.473a2.01 2.01 0 0 1 1.425-.555 1.95 1.95 0 0 1 1.407.555c.383.37.575.919.575 1.648 0 .234-.024.56-.07.978l-1.55 11.487Zm1.033 6.745A1.92 1.92 0 0 1 20 33a1.923 1.923 0 0 1-1.414-.588A1.94 1.94 0 0 1 18 30.99c0-.543.195-1.01.586-1.403A1.92 1.92 0 0 1 20 28.999a1.92 1.92 0 0 1 1.414.588c.39.392.586.86.586 1.403a1.94 1.94 0 0 1-.586 1.42Z"
        fill="var(--icon-color)"
      />
    </svg>
  </span>
  Här finns plats för text till felmeddelande.
</p>
`,
  `
<label class="ic-textarea__label" for="form_field_with_label">With label</label>
<textarea id="form_field_with_label" class="ic-textarea"></textarea>`,
];
export const Textarea = () => markupWithCodeExample(textareas);
const buttons = [
  `<button class="ic-forms__button">Primary</button>`,
  `<button
  class="ic-forms__secondary-button"
>
  Secondary</button
>`,
  `<input type="submit" class="ic-forms__button" value="Primary" />`,
  `<input
  type="reset"
  class="ic-forms__secondary-button"
  value="Secondary"
/>`,
  `<input
  disabled
  type="submit"
  class="ic-forms__button"
  value="Primary"
/>`,
  `<input
  disabled
  type="reset"
  class="ic-forms__secondary-button"
  value="Secondary"
/>`,
];
export const Buttons = () => markupWithCodeExample(buttons);
const buttonGroups = [
  `
<div class="ic-button-group">
  <button class="ic-forms__button" type="submit">Primary</button>
  <button class="ic-forms__secondary-button" type="reset">Secondary</button>
</div>
`,
  `
<div class="ic-button-group ic-button-group--right">
  <button class="ic-forms__button" type="submit">Primary</button>
  <button class="ic-forms__secondary-button" type="reset">Secondary</button>
</div>
`,
  `
<div class="ic-button-group ic-button-group--center">
  <button class="ic-forms__button" type="submit">Primary</button>
</div>
`,
  `
<div class="ic-button-group iu-justify-space-between">
  <button class="ic-forms__secondary-button iu-mx-none" type="reset">
    Secondary
  </button>
  <p>Space between items</p>
  <button class="ic-forms__button iu-mx-none" type="submit">Primary</button>
</div>
`,
];
export const ButtonGroups = () => markupWithCodeExample(buttonGroups);
const dropdowns = [
  `
<div class="ic-forms__select">
  <select name="select_default">
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
    <option>Option 4</option>
  </select>
</div>
`,
  `
<div class="ic-forms__select ic-forms__select--focus">
  <select name="select_focus">
    <option disabled>Select something</option>
    <option disabled>---</option>
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
    <option>Option 4</option>
  </select>
</div>
`,
  `
<div class="ic-forms__select ic-forms__select--disabled">
  <select disabled name="select_disabled">
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
    <option>Option 4</option>
  </select>
</div>
`,
];
export const Dropdown = () => markupWithCodeExample(dropdowns);
const dropdownsMultiple = [
  `
<div class="ic-forms__select ic-forms__select--multiple">
  <select multiple name="select_default">
    <option disabled>Select 1 or more options</option>
    <option>Option 1</option>
    <option selected>Option 2</option>
    <option>Option 3</option>
    <option>Option 4</option>
    <option>Option 5</option>
    <option>Option 6</option>
  </select>
</div>
`,
  `
<div
  class="ic-forms__select ic-forms__select--multiple ic-forms__select--focus"
>
  <select multiple name="select_focus">
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
  </select>
</div>
`,
  `
<div
  class="ic-forms__select ic-forms__select--multiple ic-forms__select--disabled"
>
  <select multiple disabled name="select_disabled">
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
  </select>
</div>
`,
  `
<div
  class="ic-forms__select ic-forms__select--multiple ic-forms__select--error"
>
  <select multiple name="select_error">
    <option>Option 1</option>
    <option>Option 2</option>
    <option>Option 3</option>
  </select>
</div>
`,
];
export const DropdownMultiple = () => markupWithCodeExample(dropdownsMultiple);
export const Spinner = () =>
  markupWithCodeExample([
    `
<div class="ic-spinner">
  <div class="ic-spinner__bounce1"></div>
  <div class="ic-spinner__bounce2"></div>
  <div class="ic-spinner__bounce3"></div>
</div>
`,
    `
<div class="iu-bg-cta ic-spinner ic-spinner--cta-text">
  <div class="ic-spinner__bounce1"></div>
  <div class="ic-spinner__bounce2"></div>
  <div class="ic-spinner__bounce3"></div>
</div>
`,
    `
<div class="iu-bg-black ic-spinner ic-spinner--white">
  <div class="ic-spinner__bounce1"></div>
  <div class="ic-spinner__bounce2"></div>
  <div class="ic-spinner__bounce3"></div>
</div>
`,
  ]);
export const Date = () =>
  markupWithCodeExample([
    `<input
  class="ic-textfield"
  type="date"
  id="date-default"
  name="date-default"
/>`,
    `<input
  class="ic-textfield"
  type="date"
  id="date-min"
  name="date-min"
  value="2020-01-01"
  min="2020-01-01"
/>`,
    `<input
  class="ic-textfield"
  type="date"
  id="date-minmax"
  name="date-minmax"
  value="2018-07-22"
  min="2018-01-01"
  max="2018-12-31"
/>`,
  ]);
export const Time = () =>
  markupWithCodeExample([
    `<input
  class="ic-textfield"
  type="time"
  id="time-default"
  name="time-default"
/>`,
    `<input
  class="ic-textfield"
  type="time"
  id="time-minmax"
  name="time-minmax"
  min="07:00"
  max="18:00"
/>`,
    `<input
  class="ic-textfield"
  type="time"
  id="time-step-1h"
  name="time-step-1h"
  step="3600"
/>`,
    `<input
  class="ic-textfield"
  type="time"
  id="time-step-1s"
  name="time-step-1s"
  step="1"
/>`,
  ]);
export const Range = () =>
  markupWithCodeExample([
    `<input
  type="range"
  id="range-minmax"
  name="range-minmax"
  min="0"
  max="10"
/>`,
    `<input
  type="range"
  id="range-step"
  name="range-step"
  min="0"
  max="100"
  step="5"
/>`,
    `<input type="range" list="tickmarks" />
<datalist id="tickmarks">
  <option value="0" label="0%"></option>
  <option value="10"></option>
  <option value="20"></option>
  <option value="30"></option>
  <option value="40"></option>
  <option value="50" label="50%"></option>
  <option value="60"></option>
  <option value="70"></option>
  <option value="80"></option>
  <option value="90"></option>
  <option value="100" label="100%"></option></datalist
>`,
  ]);
export const FormExample = () =>
  markupWithCodeExample(`
<form method="GET" style="max-width: 30em" action="" class="ic-forms">
  <div class="ic-forms__group">
    <label class="ic-forms__label" for="text-field-1">Textfield</label>
    <input
      placeholder="Field 1"
      class="ic-textfield"
      type="text"
      name="text-field-1"
      id="text-field-1"
    />
  </div>

  <div class="ic-forms__group">
    <label class="ic-forms__label" for="text-field-2">Focus on me</label>
    <input
      aria-invalid="true"
      aria-describedby="error-1"
      onblur="this.classList.add('ic-textfield--error');document.getElementById('error-1').innerText='Ett felmeddelande';"
      onfocus="this.classList.remove('ic-textfield--error');document.getElementById('error-1').innerText='';"
      placeholder="Will have error on blur"
      class="ic-textfield ic-textfield--error"
      type="text"
      name="text-field-2"
      id="text-field-2"
    />

    <p id="error-1" class="ic-forms__error-message">
      <span class="iu-svg-icon ic-textfield--icon">
        <svg
          width="40"
          height="40"
          viewBox="0 0 40 40"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          aria-hidden="true"
        >
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M.342 34.218 18.056 3.174c.674-1.183 2.13-1.54 3.212-.758.274.198.504.457.676.758l17.714 31.044c.645 1.13.355 2.623-.676 3.366-.374.27-.815.416-1.268.416H2.286C1.003 38 0 36.873 0 35.523c0-.46.118-.912.342-1.305Zm20.283-30.29a.875.875 0 0 0-.245-.278l-.085-.053c-.321-.17-.704-.048-.92.331L1.66 34.973a1.11 1.11 0 0 0-.142.55c0 .546.364.956.767.956h35.428a.648.648 0 0 0 .38-.129c.389-.28.511-.911.245-1.377L20.625 3.928Z"
            fill="var(--icon-color2)"
          />
          <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M20.381 25.667a.384.384 0 0 1-.761 0L18.106 14.18c-.07-.516-.106-.9-.106-1.153 0-.613.198-1.104.593-1.473a2.01 2.01 0 0 1 1.425-.555 1.95 1.95 0 0 1 1.407.555c.383.37.575.919.575 1.648 0 .234-.024.56-.07.978l-1.55 11.487Zm1.033 6.745A1.92 1.92 0 0 1 20 33a1.923 1.923 0 0 1-1.414-.588A1.94 1.94 0 0 1 18 30.99c0-.543.195-1.01.586-1.403A1.92 1.92 0 0 1 20 28.999a1.92 1.92 0 0 1 1.414.588c.39.392.586.86.586 1.403a1.94 1.94 0 0 1-.586 1.42Z"
            fill="var(--icon-color)"
          />
        </svg>
      </span>
      Här finns plats för text till felmeddelande.
    </p>
  </div>

  <div class="ic-forms__group iu-grid-cols">
    <div class="">
      <label class="ic-forms__label" for="text-field-a">Type A</label>
      <input
        placeholder="Insert number"
        class="ic-textfield"
        type="number"
        name="text-field-a"
        id="text-field-a"
      />
    </div>
    <div class="">
      <label class="ic-forms__label" for="text-field-b">Type B</label>
      <input
        placeholder="Insert text"
        class="ic-textfield"
        type="text"
        name="text-field-b"
        id="text-field-b"
      />
    </div>
  </div>

  <fieldset class="ic-forms__group ic-radio-group-horizontal">
    <legend class="ic-forms__label">Select one</legend>

    <input
      type="radio"
      id="demo_radio_hor_1"
      name="demo_hor_radio"
      value="1"
      class="ic-forms__radio"
    />
    <label for="demo_radio_hor_1">Radio 1</label>

    <input
      checked="checked"
      type="radio"
      id="demo_radio_hor_2"
      name="demo_hor_radio"
      value="2"
      class="ic-forms__radio"
    />
    <label for="demo_radio_hor_2">Radio 2</label>
  </fieldset>

  <div class="ic-forms__group">
    <label class="ic-forms__label" for="select-1">Select something</label>
    <div class="ic-forms__select">
      <select name="select-1" id="select-1">
        <option>Option 1</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
      </select>
    </div>
  </div>

  <div class="ic-forms__group">
    <label class="ic-forms__label" for="textarea-1">Textarea</label>
    <textarea
      placeholder="Textarea"
      class="ic-textarea"
      name="textarea-1"
      id="textarea-1"
    ></textarea>
  </div>

  <div
    role="group"
    aria-label="Do you accept the terms?"
    class="ic-forms__group ic-checkbox-group-horizontal"
  >
    <input
      class="ic-forms__checkbox"
      type="checkbox"
      id="checkbox-1"
      name="checkbox-1"
      value="1"
    />
    <label id="checkbox-1-label" for="checkbox-1">
      <span>I agree! <a href="javascript:void(0)">Terms</a></span>
    </label>
  </div>

  <div class="ic-forms__group ic-button-group">
    <button class="ic-forms__secondary-button" type="reset">Cancel</button>
    <button class="ic-forms__button" type="submit">Submit</button>
  </div>
</form>
`);
