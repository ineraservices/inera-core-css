import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Table",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* definition list: responsive helpers (--md, --lg) extends `.ic-definition-list`",
        ].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<table class="ic-table">
  <thead>
    <tr>
      <th scope="col">Vaccin</th>
      <th scope="col">Lägsta ålder</th>
      <th scope="col">1 och 2</th>
      <th scope="col">2 och 3</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="3">* a table footer</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <td>Polio</td>
      <td>2,5 mån</td>
      <td>6 v*</td>
      <td>6 mån</td>
    </tr>
    <tr>
      <td>Haemophilus infl. typ B</td>
      <td>2,5 mån</td>
      <td>6 v*</td>
      <td>6 mån</td>
    </tr>
    <tr>
    <td>Hepatit B*</td>
    <td>2,5 mån</td>
    <td>6 v*</td>
    <td>6 mån</td>
  </tr>
  <tr>
  <td>Pneumokocker</td>
  <td>12 mån</td>
  <td>6 v*</td>
  <td>6 mån</td>
</tr>
  </tbody>
</table>
`,
    `
<table class="ic-table ic-table--full">
  <tr><td>Full width</td></tr>
</table>
`,
    `
<table class="ic-table">
  <caption>Table with caption element</caption>
  <thead>
    <tr>
      <th scope="col">Table heading</th>
      <th scope="col">Table heading</th>
      <th scope="col">Table heading</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="3">* a table footer</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
    <tr>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
  </tbody>
</table>
`,
    `
<table class="ic-table">
  <caption>Table with headings in col and row scope</caption>
  <thead>
    <tr>
      <th scope="col">Headings</th>
      <th scope="col">Heading col 2</th>
      <th scope="col">Heading col 3</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="3">* a table footer</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <th scope="row">Table heading in row scope</td>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
    <tr>
      <th scope="row">Table heading in second row</th>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
  </tbody>
</table>
`,
    `
<div dir="rtl">
  <table class="ic-table">
    <caption>Table with headings in col and row scope in RTL</caption>
    <thead>
      <tr>
        <th scope="col">Headings</th>
        <th scope="col">Heading col 2</th>
        <th scope="col">Heading col 3</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="3">* a table footer</td>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <th scope="row">Table heading in row scope</td>
        <td>Table cell</td>
        <td>Table cell</td>
      </tr>
      <tr>
        <th scope="row">Table heading in second row</th>
        <td>Table cell</td>
        <td>Table cell</td>
      </tr>
    </tbody>
  </table>
</div>
`,
    `
<table class="ic-table">
  <caption>Table with caption element and no table headings</caption>
  <tr>
    <td>Table cell</td>
    <td>Table cell</td>
    <td>Table cell</td>
  </tr>
  <tr>
    <td>Table cell</td>
    <td>Table cell</td>
    <td>Table cell</td>
  </tr>
</table>
`,
    `
<div style="max-width: 300px" class="ic-table-wrapper">

  <table class="ic-table iu-text-nowrap">
    <tr>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
    <tr>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
  </table>

</div>
`,
  ]);