import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Info Box",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
  },
};

export const Information = () =>
  markupWithCodeExample([
    `
<div class="ic-alert ic-alert--info">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-info-icon"></i>
    Information
  </h3>
  <div class="ic-alert__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
    <ul role="list">
      <li>This is information</li>
      <li>This is information</li>
      <li>This is information</li>
    </ul>
    <p><a href="javascript:void(0)">And a link</a></p>
  </div>
</div>
`,
  ]);

export const Observe = () =>
  markupWithCodeExample([
    `
<div class="ic-alert ic-alert--observe">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-observe-icon"></i>
    Observe
  </h3>
  <div class="ic-alert__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
    <ul role="list">
      <li>This is information</li>
      <li>This is information</li>
      <li>This is information</li>
    </ul>
    <p><a href="javascript:void(0)">And a link</a></p>
  </div>
</div>
`,
  ]);
