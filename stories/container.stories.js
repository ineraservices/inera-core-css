import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Container",
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* Do not nest container elements.",
          "* The medium narrow container is used for example in the footer.",
        ].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<div class="ic-container demo-box">
    I am a container. I have a max-width and gutters.
</div>
`,
    `
<div class="ic-container iu-grid-rows">
  <div class="demo-box">
    I am the content inside a container.
  </div>
  <div class="iu-grid-cols">
    <div class="demo-box">1</div>
    <div class="demo-box">2</div>
    <div class="demo-box">3</div>
    <div class="demo-box">4</div>
  </div>
</div>
`,
    `
<div class="ic-container ic-container--full demo-box">
    I am a full width container. I have a no max-width but gutters.
</div>
`,
    `
<div class="ic-container--narrow-md demo-box">
    I inherit from default container<br />
    but I am narrower on medium screens.
</div>
`,
  ]);
