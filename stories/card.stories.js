import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Cards",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component: "Cards can be used for many types of component and content.",
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<div class="ic-card">
  <p>Card with content</p>
</div>
`,
    `
<div class="ic-card ic-card--shadow ic-card--noborder">
  <p>No border. Box shadow.</p>
</div>`,
    `
<div class="ic-card shadow noborder">
  <p>This works as well.</p>
</div>`,
    `
<div class="ic-card">
  <h2 class="ic-card__title">Rubrik</h2>
  <div class="ic-card__body">
    <p>
      Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      Vestibulum id ligula porta felis euismod semper. Nulla vitae elit
      libero, a pharetra augue. Cras mattis consectetur purus sit amet
      fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
    </p>
  </div>
</div>
  `,
    `
<div class="ic-card">
  <h2 class="ic-card__title h2 ic-card__title--chevron ic-card__title--border">
    Headline with h2 properties
  </h2>
  <div class="ic-card__body">
    <p>
      Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      Vestibulum id ligula porta felis euismod semper. Nulla vitae elit
      libero, a pharetra augue. Cras mattis consectetur purus sit amet
      fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
    </p>
  </div>
</div>
`,
  ]);

export const Linked = () =>
  markupWithCodeExample([
    `
<div class="ic-card ic-text ic-card--link">
  <a class="ic-card-link" href="javascript:void(0)">I am the link</a>
  <p>
    This whole card can be clicked. The card should have a focus
    style when the link has focus.
  </p>
  <button
    class="iu-fs-sm iu-bg-grey-300 iu-px-200"
    type="button"
    onclick="document.querySelector('.ic-card-link').focus(); return false;">
    Set focus
  </button>
</div>
`,
  ]);

export const WithUtils = () =>
  markupWithCodeExample([
    `
<div class="ic-card iu-shadow-none iu-border-muted">
  <p>No box shadow. Muted border</p>
</div>`,
    `
<div class="ic-card iu-bg-secondary-light iu-shadow-none iu-border-main">
  <p>No box shadow. Secondary light background and main border.</p>
</div>
`,
    `
<div class="ic-card iu-shadow-none iu-bg-main iu-color-white iu-radius-none">
  <p>No box shadow. Secondary light background and main border.</p>
</div>
`,
    `
<div class="ic-card iu-mb-xxl"  >
  <p>Big margin under.</p>
</div>
<p>Text under</p>
`,
  ]);

export const WithGrid = () =>
  markupWithCodeExample([
    `
<div class="ic-card">
  <div class="iu-grid-cols-lg-12 iu-align-center">
    <div class="iu-grid-span-lg-4">
      <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
    </div>
    <div class="iu-grid-span-lg-8">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus a pretium tellus, ac vehicula lectus. Aenean varius nisi nec cursus lacinia. Nam laoreet turpis a convallis lacinia. Pellentesque accumsan erat eros, ut blandit mauris facilisis vitae.</p>
    </div>
  </div>
</div>`,
  ]);

export const WithText = () =>
  markupWithCodeExample([
    `
<div class="ic-card ic-text">
  <h2>This is a card with styled text content.</h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus a pretium tellus, ac vehicula lectus. Aenean varius nisi nec cursus lacinia. Nam laoreet turpis a convallis lacinia. Pellentesque accumsan erat eros, ut blandit mauris facilisis vitae.</p>
  <h3>Heading</h3>
  <ul role="list">
    <li>List</li>
    <li>List</li>
  </ul>
  <p>Ut ut augue a ipsum hendrerit laoreet. Proin porttitor mollis enim, non accumsan est sodales vel. Nunc et mauris in massa ullamcorper pellentesque. </p>
  <h3>Heading</h3>
  <table>
    <thead>
      <tr>
        <th>Table heading</th>
        <th>Table heading</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Table cell</td>
        <td>Table cell</td>
      </tr>
      <tr>
        <td>Table cell</td>
        <td>Table cell</td>
      </tr>
    </tbody>
  </table>
  <p>Ut ut augue a ipsum hendrerit laoreet. Proin porttitor mollis enim, non accumsan est sodales vel. Nunc et mauris in massa ullamcorper pellentesque.</p>
</div>`,
  ]);

export const InsideGrid = () =>
  markupWithCodeExample([
    `
<ul class="iu-grid-cols iu-grid-cols-md-2 iu-grid-cols-lg-4">
  <li class="ic-card ic-card--inspiration">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar sagittis elit ut faucibus.</p>
  </li>
  <li class="ic-card ic-card--inspiration">
    <p>Cras faucibus arcu ut nisi ullamcorper venenatis. In odio leo, congue eget aliquet ac, imperdiet ut turpis. Etiam fringilla, nulla sollicitudin posuere commodo, elit massa tristique nisi, at vehicula ante lorem sed leo.</p>
  </li>
  <li class="ic-card ic-card--inspiration">
    <p>Vivamus malesuada congue nisi, sed porttitor orci vestibulum efficitur. Nunc lobortis velit at vehicula sollicitudin. </p>
  </li>
  <li class="ic-card ic-card--inspiration">
    <p> Maecenas vitae ultricies risus. Quisque euismod metus mauris. Suspendisse finibus risus sit amet dolor lacinia efficitur.</p>
  </li>
</ul>
`,
    `
<ul class="iu-grid-cols">
  <li class="ic-card ic-card--inspiration">
    <p>Navigationskomponent inspiration</p>
  </li>
  <li class="ic-card ic-card--inspiration">
    <p>Navigationskomponent inspiration</p>
  </li>
  <li class="ic-card ic-card--inspiration">
    <p>Navigationskomponent inspiration</p>
  </li>
</ul>
`,
    `

<ul class="iu-grid-cols">
  <li class="ic-card ic-card--inspiration">
    <p>Navigationskomponent inspiration</p>
  </li>
  <li class="ic-card ic-card--inspiration">
    <p>Navigationskomponent inspiration</p>
  </li>
</ul>
`,
  ]);

export const InfoBox = () =>
  markupWithCodeExample(`
<div class="ic-card ic-card--sm-unset-style shadow noborder iu-p-900 ic-text">
  <h3 class="ic-subtitle-small ic-light-text ic-small-divider">I'm a subtitle and I have a divider.</h3>
  <p>This is some text content.</p>
  <p>I will remove my padding, border, border-radius and box-shadow for small devices.</p>
</div>
`);

export const Inspiration = () =>
  markupWithCodeExample([
    `
<div class="ic-card ic-card--inspiration">
  <p>Navigationskomponent inspiration</p>
</div>
`,
    `<div class="ic-card ic-card--inspiration">
<div class="ic-card__body">
    <p>
      Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      Vestibulum id ligula porta felis euismod semper. Nulla vitae elit
      libero, a pharetra augue. Cras mattis consectetur purus sit amet
      fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
    </p>
  </div>
</div>`,
    `
<div class="ic-card ic-card--inspiration">
  <h2 class="ic-card__title"><a href="javascript:void(0)">Rubrik</a></h2>
  <div class="ic-card__body">
    <p>
      Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      Vestibulum id ligula porta felis euismod semper. Nulla vitae elit
      libero, a pharetra augue. Cras mattis consectetur purus sit amet
      fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
    </p>
  </div>
</div>
`,
    `
<ul class="iu-grid-cols iu-grid-cols-lg-4 iu-grid-cols-md-2">
  <li class="ic-card ic-card--inspiration">
    <h2 class="ic-card__title"><a href="javascript:void(0)">Rubrik</a></h2>
    <div class="ic-card__body">
      <p>
        Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      </p>
    </div>
    <figure class="ic-card__image iu-order-minus-1">
      <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
    </figure>
  </li>
  <li class="ic-card ic-card--inspiration">
    <h2 class="ic-card__title"><a href="javascript:void(0)">Rubrik</a></h2>
    <div class="ic-card__body">
      <p>
        Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      </p>
    </div>
    <figure class="ic-card__image iu-order-minus-1">
      <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
    </figure>
  </li>
  <li class="ic-card ic-card--inspiration">
    <h2 class="ic-card__title"><a href="javascript:void(0)">Rubrik</a></h2>
    <div class="ic-card__body">
      <p>
        Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      </p>
    </div>
    <figure class="ic-card__image iu-order-minus-1">
      <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
    </figure>
  </li>
  <li class="ic-card ic-card--inspiration">
    <h2 class="ic-card__title"><a href="javascript:void(0)">Rubrik</a></h2>
    <div class="ic-card__body">
      <p>
        Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      </p>
    </div>
    <figure class="ic-card__image iu-order-minus-1">
      <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
    </figure>
  </li>
</ul>
  `,
    `
<div class="ic-card ic-card--inspiration">
  <h2 class="ic-card__title"><a href="javascript:void(0)">Rubrik</a></h2>
  <div class="ic-card__body">
    <p>
      Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
      Vestibulum id ligula porta felis euismod semper. Nulla vitae elit
      libero, a pharetra augue. Cras mattis consectetur purus sit amet
      fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
    </p>
  </div>
  <!--
  we use utility for: order: -1 (iu-order-minus-1) to pull the image to the top,
  but keep the text first in source order.
  -->
  <figure class="ic-card__image iu-order-minus-1">
    <img src="https://via.placeholder.com/1200x300" alt="placeholder image" />
  </figure>
</div>

`,
  ]);

export const ArticleList = () =>
  markupWithCodeExample(`
<section class="ic-section">

  <header class="ic-section-header">
    <h2 class="ic-section-headline">Article list</h2>
  </header>

  <ul class="iu-grid-cols iu-grid-cols-lg-3 iu-grid-cols-md-2">

    <li class="ic-card ic-card--link ic-card--article">
      <div class="ic-card__text">
        <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
        <p>Text content goes here</p>
      </div>
      <!--
      we use utility for: order: -1 (iu-order-minus-1) to pull the image to the top,
      but keep the text first in source order.
      -->
      <figure class="ic-card__image iu-order-minus-1">
        <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
      </figure>
    </li>

    <li class="ic-card ic-card--link ic-card--article">
      <div class="ic-card__text">
        <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
        <p>Text content goes here</p>
      </div>
      <figure class="ic-card__image iu-order-minus-1">
        <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
      </figure>
    </li>

    <li class="ic-card ic-card--link ic-card--article">
      <div class="ic-card__text">
        <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
        <p>Text content goes here</p>
      </div>
      <figure class="ic-card__image iu-order-minus-1">
        <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
      </figure>
    </li>

    <li class="ic-card ic-card--link ic-card--article">
      <div class="ic-card__text">
        <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
        <p>Text content goes here</p>
      </div>
      <figure class="ic-card__image iu-order-minus-1">
        <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
      </figure>
    </li>

    <li class="ic-card ic-card--link ic-card--article">
      <div class="ic-card__text">
        <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
        <p>Text content goes here</p>
      </div>
      <figure class="ic-card__image iu-order-minus-1">
        <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
      </figure>
    </li>

    <li class="ic-card ic-card--link ic-card--article">
      <div class="ic-card__text">
        <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
        <p>Text content goes here</p>
      </div>
      <figure class="ic-card__image iu-order-minus-1">
        <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
      </figure>
    </li>

  </ul>

</section>
`);

export const TextFadeout = () =>
  markupWithCodeExample([
    `
<div class="ic-card ic-text-fadeout" style="max-height: 120px; max-width: 300px;">
  <p class="ic-text-fadeout-inner">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    Pellentesque lobortis sem eu felis pharetra, sit amet dapibus nisl laoreet.
    Phasellus quis ultricies nunc, a eleifend sem.
    Curabitur pellentesque justo vel sapien rutrum, ut volutpat leo sodales.
    Vivamus accumsan tellus a ligula porta, quis ornare augue aliquet
  </p>
</div>
`,
    `
<div class="ic-card ic-text-fadeout" style="max-height: 120px; max-width: 300px;">
  <p class="ic-text-fadeout__inner">
    A short text. This will not be hidden.
  </p>
</div>
`,
  ]);
