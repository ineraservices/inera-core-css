import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Buttons",
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* Do not forget the type attribute. Should be one of `button`, `submit` or `reset`.",
        ].join("\r\n"),
      },
    },
  },
};

export const Default = () =>
  markupWithCodeExample([
    '<button class="ic-button" type="button">Default</button>',
    '<button class="ic-button ic-button--hover" type="button">Hover</button>',
    '<button class="ic-button ic-button--secondary" type="button">Secondary</button>',
    '<button class="ic-button" disabled type="button" aria-disabled="true">Disabled</button>',
    '<button class="ic-button ic-button--disabled" disabled type="button" aria-disabled="true">Disabled</button>',
    '<button class="ic-button ic-button--rounded" type="button">Rounded</button>',
    '<div class="iu-p-md iu-bg-nav"><button class="ic-button ic-button--ghost" type="button">Ghost</button></div>',
  ]);

export const States = () =>
  markupWithCodeExample([
    `
<button aria-label="Form is loading" class="ic-button ic-button--loading">

  <!-- initial state -->
  <span class="ic-button-loading-hide ic-button-success-hide">Submit form</span>

  <!-- loading state -->
  <span class="ic-button-loading-show ic-button-success-hide">
    <span class="ic-spinner">
      <span class="ic-spinner__bounce1"></span>
      <span class="ic-spinner__bounce2"></span>
      <span class="ic-spinner__bounce3"></span>
    </span>
  </span>

  <!-- success state -->
  <span class="ic-button-success-show ic-button-loading-hide">
    <span class="iu-svg-icon">
      <svg aria-hidden="true" focusable="false" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
        <path fill="#ffffff" d="M26.046 14.925l3.485-3.486c.586-.585 1.536-.585 2.122 0 .585.586.585 1.536 0 2.122l-3.486 3.485-2.121 2.121-8.485 8.486c-.586.585-1.536.585-2.122 0-.585-.586-.585-1.536 0-2.122l8.486-8.485 2.12-2.121z" transform="translate(-663 -475) translate(90 386) translate(531 84) translate(42 5)"/>
        <path fill="#ffffff" d="M14.096 23.096h3c.829 0 1.5.672 1.5 1.5 0 .829-.671 1.5-1.5 1.5h-7c-.828 0-1.5-.671-1.5-1.5 0-.828.672-1.5 1.5-1.5h4z" transform="translate(-663 -475) translate(90 386) translate(531 84) translate(42 5) rotate(45 13.596 24.596)"/>
      </svg>
    </span>
  </span>

</button>`,
    `
<button aria-label="Form sent" class="ic-button ic-button--success">

  <!-- initial state -->
  <span class="ic-button-loading-hide ic-button-success-hide">Submit form</span>

  <!-- loading state -->
  <span class="ic-button-loading-show ic-button-success-hide">
    <span class="ic-spinner">
      <span class="ic-spinner__bounce1"></span>
      <span class="ic-spinner__bounce2"></span>
      <span class="ic-spinner__bounce3"></span>
    </span>
  </span>

  <!-- success state -->
  <span class="ic-button-success-show ic-button-loading-hide">
    <span class="iu-svg-icon">
      <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
        <path fill="#ffffff" d="M26.046 14.925l3.485-3.486c.586-.585 1.536-.585 2.122 0 .585.586.585 1.536 0 2.122l-3.486 3.485-2.121 2.121-8.485 8.486c-.586.585-1.536.585-2.122 0-.585-.586-.585-1.536 0-2.122l8.486-8.485 2.12-2.121z" transform="translate(-663 -475) translate(90 386) translate(531 84) translate(42 5)"/>
        <path fill="#ffffff" d="M14.096 23.096h3c.829 0 1.5.672 1.5 1.5 0 .829-.671 1.5-1.5 1.5h-7c-.828 0-1.5-.671-1.5-1.5 0-.828.672-1.5 1.5-1.5h4z" transform="translate(-663 -475) translate(90 386) translate(531 84) translate(42 5) rotate(45 13.596 24.596)"/>
      </svg>
    </span>
  </span>

</button>`,
    `
<button class="ic-button">

  <!-- initial state -->
  <span class="ic-button-loading-hide ic-button-success-hide">Submit form</span>

  <!-- loading state -->
  <span class="ic-button-loading-show ic-button-success-hide">
    <span class="ic-spinner">
      <span class="ic-spinner__bounce1"></span>
      <span class="ic-spinner__bounce2"></span>
      <span class="ic-spinner__bounce3"></span>
    </span>
  </span>

  <!-- success state -->
  <span class="ic-button-success-show ic-button-loading-hide">
    <span class="iu-svg-icon">
      <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
        <path fill="#ffffff" d="M26.046 14.925l3.485-3.486c.586-.585 1.536-.585 2.122 0 .585.586.585 1.536 0 2.122l-3.486 3.485-2.121 2.121-8.485 8.486c-.586.585-1.536.585-2.122 0-.585-.586-.585-1.536 0-2.122l8.486-8.485 2.12-2.121z" transform="translate(-663 -475) translate(90 386) translate(531 84) translate(42 5)"/>
        <path fill="#ffffff" d="M14.096 23.096h3c.829 0 1.5.672 1.5 1.5 0 .829-.671 1.5-1.5 1.5h-7c-.828 0-1.5-.671-1.5-1.5 0-.828.672-1.5 1.5-1.5h4z" transform="translate(-663 -475) translate(90 386) translate(531 84) translate(42 5) rotate(45 13.596 24.596)"/>
      </svg>
    </span>
  </span>

</button>`,
  ]);

export const ButtonLink = () =>
  markupWithCodeExample([
    `<a class="ic-button" href="https://www.example.com/">Button link</a>`,
    `
<div class="ic-text">
  <a class="ic-button" href="https://www.example.com/">Button link</a>
</div>
`,
  ]);

export const Responsive = () =>
  markupWithCodeExample([
    `
<button class="ic-button ic-button--sm-full">
  Click me!
</button>`,
    `
<a href="javascript:void(0);" class="ic-button ic-button--sm-full">
  Click me!
</a>`,
  ]);

export const Circle = () =>
  markupWithCodeExample([
    `<a class="ic-button--circle">A</a>`,
    `<button class="ic-button--circle">1</button>`,
  ]);

export const Hamburger = () =>
  markupWithCodeExample([
    `
<!-- onclick function only for demo -->

  <button
    onclick="this.querySelectorAll('.lines-button')[0].classList.toggle('close')"
    type="button"
    class="ic-burger iu-bg-cta iu-p-300"
    aria-label="Meny"
    aria-expanded="false"
  >
    <span class="lines-button x">
      <span class="lines"></span>
    </span>
  </button>`,
    `
<!-- onclick function only for demo -->
  <button
    onclick="this.querySelectorAll('.lines-button')[0].classList.toggle('close')"
    type="button"
    class="ic-burger iu-bg-cta iu-p-300"
    aria-label="Meny"
    aria-expanded="true"
  >
    <span class="lines-button x close">
      <span class="lines"></span>
    </span>
  </button>`,
  ]);

export const Sizes = () =>
  markupWithCodeExample([
    `
<div class="ic-button--group">
  <button class="ic-button ic-button--large" type="button">Large</button>
  <button class="ic-button ic-button--medium" type="button">Medium</button>
  <button class="ic-button ic-button--small" type="button">Small</button>
</div>    
`,
  ]);
