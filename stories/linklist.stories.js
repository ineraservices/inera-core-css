import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Link List",
  parameters: {
    status: "",
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<ul role="list" class="ic-link-list">
  <li><a href="javascript:void(0)">Lätt svenska</a></li>
  <li><a href="javascript:void(0)">Other languages</a></li>
  <li><a href="javascript:void(0)">Lyssna</a></li>
</ul>
`,
  ]);

export const WithHeadline = () =>
  markupWithCodeExample([
    `
<h3 class="ic-link-list__headline">This is a list</h3>
<ul role="list" class="ic-link-list">
  <li><a href="javascript:void(0)">Links</a></li>
  <li><a href="javascript:void(0)">Some other link</a></li>
  <li><a href="javascript:void(0)">A third link</a></li>
</ul>
`,
  ]);

export const Icons = () =>
  markupWithCodeExample([
    `
<h3 class="ic-link-list__headline">This is a list with links</h3>
<ul role="list" class="ic-link-list">
  <li><a class="icon-arrow" href="javascript:void(0)">Link somewhere</a></li>
  <li><a class="icon-arrow" href="javascript:void(0)">Link somewhere</a></li>
  <li><a class="icon-arrow" href="javascript:void(0)">Link somewhere</a></li>
  <li><a class="icon-arrow" href="javascript:void(0)">Link somewhere</a></li>
  <li><a class="icon-arrow" href="javascript:void(0)">Link somewhere</a></li>
</ul>`,
    `
<h3 class="ic-link-list__headline">This is a list of documents</h3>
<ul role="list" class="ic-link-list">
  <li><a class="icon-pdf" href="javascript:void(0)">Link to PDF document</a></li>
  <li><a class="icon-xls" href="javascript:void(0)">Link to XLS document</a></li>
  <li><a class="icon-doc" href="javascript:void(0)">Link to Word document</a></li>
  <li><a class="icon-ppt" href="javascript:void(0)">Link to PPT document</a></li>
</ul>`,
  ]);

export const NavAndChevron = () =>
  markupWithCodeExample([
    `
<ul role="list" class="ic-link-list ic-link-list--nav">
  <li>
    <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
  </li>
  <li>
    <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
  </li>
  <li>
    <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
  </li>
</ul>
`,
  ]);
