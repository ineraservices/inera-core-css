import { markupWithCodeExample, bleedContainer } from "./helpers";

export default {
  title: "templates/Pages",
  decorators: [bleedContainer],
  parameters: {
    status: "",
  },
};

export const Samlingssida = () =>
  markupWithCodeExample(`
<!-- <div class="ic-body"> could also be <body class="ic-body"> -->
<div class="ic-body">

  <header class="ic-page-header">
  <div class="ic-page-header__container">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
  </div>
  
  <nav class="ic-topnav iu-hide-md iu-hide-sm">
    <ul class="ic-container">
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-0">
          <span>Menu item 1</span>
        </a>
        <div class="ic-meganav" id="subnav-0" aria-hidden="true"></div>
      </li>
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-1">
          <span>Menu item 2</span>
        </a>
        <div class="ic-meganav" id="subnav-1" aria-hidden="true"></div>
      </li>
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-2">
          <span>Menu item 3</span>
        </a>
        <div class="ic-meganav" id="subnav-2" aria-hidden="true"></div>
      </li>
    </ul>
  </nav>
  
</header>

<main class="ic-page">

  <div class="ic-container">

    <nav aria-label="Brödsmulor" class="ic-breadcrumb iu-my-500 iu-hide-sm">
    <span aria-hidden="true" class="ic-breadcrumb__label">Du är här: </span>
      <ol>
        <li class="ic-breadcrumb__item">
          <a class="ic-breadcrumb__link" href="javascript:void(0)">Start</a>
        </li>
        <li class="ic-breadcrumb__item">
          <a class="ic-breadcrumb__link" href="javascript:void(0)">En undersida</a>
        </li>
        <li class="ic-breadcrumb__item">
          <a class="ic-breadcrumb__link" href="javascript:void(0)" aria-current="location">Rubrik</a>
        </li>
      </ol>
    </nav>

    <div id="content">

      <section class="ic-section ic-section-intro">
        <div>
          <h1>This is a page</h1>
          <p class="ic-preamble">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus est nec enim facilisis faucibus. Sed aliquet ac ante et ornare. Nulla tempus bibendum lorem id congue. Proin nec ante dolor. Etiam convallis vulputate dolor nec finibus. </p>
        </div>
        <figure><img src="https://via.placeholder.com/400x300" alt="placeholder image" /></figure>
      </section>

      <section class="ic-section ic-section--divider">
        <header class="ic-section-header iu-justify-start">
          <h2 class="ic-section-headline ic-section-headline--noborder">
            Headline no border. Section has divider.
          </h2>
        </header>

        <ul class="iu-grid-cols iu-grid-cols-md-2 iu-grid-cols-lg-4">

          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
              </p>
            </div>
          </li>
          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
              </p>
            </div>
          </li>
          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum
              </p>
            </div>
          </li>
          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
              </p>
            </div>
          </li>
          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
              </p>
            </div>
          </li>
          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
              </p>
            </div>
          </li>
          <li class="ic-card ic-card--link ic-card--inspiration ic-text-fadeout">
            <h2 class="ic-card__title"><a class="ic-card-link" href="javascript:void(0)">Rubrik</a></h2>
            <div class="ic-card__body" style="max-height: 100px">
              <p>
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
              </p>
            </div>
          </li>
        </ul>
      </section>

      <section class="ic-section ic-section--divider">
        <header class="ic-section-header">
          <h2 class="ic-section-headline">Article list</h2>
        </header>

        <ul class="iu-grid-cols iu-grid-cols-lg-3 iu-grid-cols-md-2">

          <li class="ic-card ic-card--link ic-text-fadeout ic-card--article">
            <div class="ic-card__text ic-text-fadeout__inner" style="max-height: 140px;">
              <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
              <p>Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
            <figure class="ic-card__image iu-order-minus-1">
              <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
            </figure>
          </li>

          <li class="ic-card ic-card--link ic-text-fadeout ic-card--article">
            <div class="ic-card__text ic-text-fadeout__inner" style="max-height: 140px;">
              <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
              <p>Sed posuere consectetur est at lobortis. Aenean eu leo quam. </p>
            </div>
            <figure class="ic-card__image iu-order-minus-1">
              <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
            </figure>
          </li>

          <li class="ic-card ic-card--link ic-text-fadeout ic-card--article">
            <div class="ic-card__text ic-text-fadeout__inner" style="max-height: 140px;">
              <h3 class="iu-fs-600"><a class="ic-card-link" href="javascript:void(0)">Headline for the article</a></h3>
              <p>Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui</p>
            </div>
            <figure class="ic-card__image iu-order-minus-1">
              <img src="https://via.placeholder.com/400x300" alt="placeholder image" />
            </figure>
          </li>

        </ul>

      </section>

    </div>

  </div>

</main>

<footer class="ic-page-footer">
  <div class="ic-page-footer__inner">
    <div class="iu-grid-cols-lg-5 ic-container iu-mb-300">
      <h2 class="ic-page-footer__heading iu-grid-span-lg-2">
        <span class="iu-color-main">1177</span>
        <span class="iu-color-secondary-dark">- tryggt om din hälsa och vård</span>
      </h2>
    </div>
    <div class="iu-grid-cols-lg-5 ic-container">
      <div class="iu-grid-span-lg-2 ic-text">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Praesent nisl lacus, vulputate commodo sagittis sit amet,
          scelerisque vel tortor.
        </p>
        <p>
          Donec ultrices lobortis nulla, sit amet suscipit tellus
          accumsan non. Quisque rutrum aliquet elit id dignissim.
          Nulla accumsan orci enim, ut feugiat justo luctus non.
        </p>
      </div>
      <div class="iu-grid-span-lg-3 iu-grid iu-grid-cols-3 iu-pl-xl iu-hide-sm iu-hide-md">
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
          </li>
        </ul>
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
          </li>
        </ul>
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

</div><!-- / ic-body -->
`);

export const EmptyPage = () =>
  markupWithCodeExample(`
<!-- <div class="ic-body"> could also be <body class="ic-body"> -->
<div class="ic-body">

  <header class="ic-page-header">
  <div class="ic-page-header__container">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
  </div>
  <nav class="ic-topnav iu-hide-md iu-hide-sm">
    <ul class="ic-container">
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-0">
          <span>Menu item 1</span>
        </a>
        <div class="ic-meganav" id="subnav-0" aria-hidden="true"></div>
      </li>
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-1">
          <span>Menu item 2</span>
        </a>
        <div class="ic-meganav" id="subnav-1" aria-hidden="true"></div>
      </li>
      <li class="ic-topnav__item">
        <a class="ic-topnav__link" href="javascript:void(0)" aria-expanded="false" aria-controls="subnav-2">
          <span>Menu item 3</span>
        </a>
        <div class="ic-meganav" id="subnav-2" aria-hidden="true"></div>
      </li>
    </ul>
  </nav>
</header>

<main class="ic-page">

  <div class="ic-container">

    <nav aria-label="Brödsmulor" class="ic-breadcrumb iu-my-500 iu-hide-sm">
    <span aria-hidden="true" class="ic-breadcrumb__label">Du är här: </span>
      <ol>
        <li class="ic-breadcrumb__item">
          <a class="ic-breadcrumb__link" href="javascript:void(0)">Start</a>
        </li>
        <li class="ic-breadcrumb__item">
          <a class="ic-breadcrumb__link" href="javascript:void(0)">En undersida</a>
        </li>
        <li class="ic-breadcrumb__item">
          <a class="ic-breadcrumb__link" href="javascript:void(0)" aria-current="location">Rubrik</a>
        </li>
      </ol>
    </nav>

    <div id="content">



    </div>

  </div>
  

</main>

<footer class="ic-page-footer">
  <div class="ic-page-footer__inner">
    <div class="iu-grid-cols-lg-5 ic-container iu-mb-300">
      <h2 class="ic-page-footer__heading iu-grid-span-lg-2">
        <span class="iu-color-main">1177</span>
        <span class=" iu-color-secondary-dark">- tryggt om din hälsa och vård</span>
      </h2>
    </div>
    <div class="iu-grid-cols-lg-5 ic-container">
      <div class="iu-grid-span-lg-2 ic-text">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Praesent nisl lacus, vulputate commodo sagittis sit amet,
          scelerisque vel tortor.
        </p>
        <p>
          Donec ultrices lobortis nulla, sit amet suscipit tellus
          accumsan non. Quisque rutrum aliquet elit id dignissim.
          Nulla accumsan orci enim, ut feugiat justo luctus non.
        </p>
      </div>
      <div class="iu-grid-span-lg-3 iu-grid iu-grid-cols-3 iu-pl-xl iu-hide-sm iu-hide-md">
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
          </li>
        </ul>
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
          </li>
        </ul>
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Lätt svenska</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Other languages</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Lyssna</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

</div><!-- / ic-body -->
`);
