import { markupWithCodeExample, bleedContainer } from "../../helpers";

export default {
  title: "components/Page Footer/1177-Profession",
  decorators: [bleedContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: "1177-profession",
  },
};

const footerOpenMobile = `
  <div class="ic-page-footer__mobile iu-hide-from-lg ic-container">
    <h1 class="ic-page-footer__heading iu-color-secondary-dark iu-mb-500">
      Om <span>[Tjänstens namn]</span>
    </h1>

    <div class="ic-text iu-mb-1000">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Praesent nisl lacus, vulputate commodo sagittis sit amet,
        scelerisque vel tortor.
      </p>
      <p>
        Donec ultrices lobortis nulla, sit amet suscipit tellus
        accumsan non. Quisque rutrum aliquet elit id dignissim.
        Nulla accumsan orci enim, ut feugiat justo luctus non.
      </p>
    </div>

    <div class="ic-page-footer__menu iu-bg-main">
    <nav class="ic-nav-list" id="mobile-nav" aria-label="sidfot meny">
      <ul class="ic-nav-list__list">
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 1</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-0"
              class="ic-nav-list__expandpro"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
        </li>
        <li class="ic-nav-list__item__no__style" aria-expanded="true">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 2</a>
            <button
              aria-expanded="true"
              aria-controls="submenu-1"
              class="ic-nav-list__expandpro"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 2</span>
            </button>
          </div>
        </li>
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 3</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-2"
              class="ic-nav-list__expandpro"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 3</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-2"></ul>
        </li>
        <li aria-hidden="true"><!-- add last shadow element --></li>
      </ul>
    </nav>
  </div>

    <div class="ic-page-footer__about">
      <div class="ic-page-footer__about__links">
        <ul class="ic-cookie-list ic-link-list iu-fs-200">
          <li><a class="ic-cookie-list--link" href="javascript:void(0)">Behandling av personuppgifter</a></li>
          <li><a class="ic-cookie-list--link" href="javascript:void(0)">Hantering av kakor</a></li>
          <li><a class="ic-cookie-list--link" href="javascript:void(0)">Inställningar för kakor</a></li>
        </ul>
      </div>

      <div class="ic-page-footer__about__who">
        <a class="ic-page-footer__about__who__service" href="/" aria-label="[Tjänstens namn], startsida" tabindex="-1">
          <span class="ic-page-footer__logo-profession"></span>
          <span class="ic-page-header__line-vertical"></span>
          <span class="ic-page-footer__about__who__service--name">[Tjänstens namn]</span>
        </a>

        <div class="ic-page-footer__about__who--responsible">
          <span>[Tjänstens namn]</span> 
          drivs av <a href="https://www.inera.se/" style = "text-decoration: none;">
          <span style = "color: #636466; !important;
          border-bottom: 2px solid currentColor;
          display: inline-block;
          line-height: 0.85;"> Inera AB </span> </a>
          <a style="color: #636466; !important; 
          text-decoration: none;
          class="ic-link-footer"
          href="https://www.inera.se/"
        >
        <svg width="14" height="14" viewBox="0 -7 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.3871 1.87695C9.23218 1.87695 1 10.1091 1 20.264C1 30.419 9.23218 38.6511 19.3871 38.6511C29.542 38.6511 37.7742 30.419 37.7742 20.264" stroke="#636466" stroke-width="4" stroke-linecap="round"/>
                <path d="M36.3616 3.4654L37.3587 3.3887L37.2929 2.53408L36.4383 2.46834L36.3616 3.4654ZM36.4237 4.27275L37.4208 4.19605L37.4208 4.19605L36.4237 4.27275ZM35.5543 3.40329L35.4776 4.40035L35.4776 4.40035L35.5543 3.40329ZM29.9202 2.96991L29.8435 3.96696L29.8435 3.96696L29.9202 2.96991ZM28.9059 3.83936L27.9088 3.76266L27.9088 3.76266L28.9059 3.83936ZM29.7753 4.85372L29.6986 5.85077L29.6986 5.85077L29.7753 4.85372ZM33.5623 5.14503L34.2694 5.85213L35.8068 4.31473L33.639 4.14797L33.5623 5.14503ZM18.7786 19.9288L18.0715 19.2217L18.7786 19.9288ZM18.7786 21.2648L19.4857 20.5577L19.4857 20.5577L18.7786 21.2648ZM34.7129 6.66644L35.7099 6.58974L35.5432 4.42192L34.0058 5.95933L34.7129 6.66644ZM34.9733 10.0517L35.9703 9.975L35.9703 9.975L34.9733 10.0517ZM35.9876 10.9212L36.0643 11.9182L36.0643 11.9182L35.9876 10.9212ZM36.8571 9.90679L35.86 9.98349L35.86 9.98349L36.8571 9.90679ZM35.3646 3.54209L35.4267 4.34944L37.4208 4.19605L37.3587 3.3887L35.3646 3.54209ZM35.4776 4.40035L36.2849 4.46245L36.4383 2.46834L35.631 2.40624L35.4776 4.40035ZM29.8435 3.96696L35.4776 4.40035L35.631 2.40624L29.9969 1.97285L29.8435 3.96696ZM29.9029 3.91605C29.9006 3.94651 29.874 3.9693 29.8435 3.96696L29.9969 1.97285C28.9261 1.89048 27.9912 2.6918 27.9088 3.76266L29.9029 3.91605ZM29.852 3.85666C29.8825 3.85901 29.9052 3.8856 29.9029 3.91605L27.9088 3.76266C27.8264 4.83352 28.6277 5.7684 29.6986 5.85077L29.852 3.85666ZM33.639 4.14797L29.852 3.85666L29.6986 5.85077L33.4856 6.14208L33.639 4.14797ZM19.4857 20.6359L34.2694 5.85213L32.8552 4.43792L18.0715 19.2217L19.4857 20.6359ZM19.4857 20.5577C19.5073 20.5793 19.5073 20.6143 19.4857 20.6359L18.0715 19.2217C17.312 19.9811 17.312 21.2124 18.0715 21.9719L19.4857 20.5577ZM19.4074 20.5577C19.429 20.5361 19.4641 20.5361 19.4857 20.5577L18.0714 21.9719C18.8309 22.7313 20.0622 22.7313 20.8217 21.9719L19.4074 20.5577ZM34.0058 5.95933L19.4074 20.5577L20.8217 21.9719L35.42 7.37354L34.0058 5.95933ZM35.9703 9.975L35.7099 6.58974L33.7158 6.74313L33.9762 10.1284L35.9703 9.975ZM35.9109 9.9241C35.9414 9.92175 35.968 9.94455 35.9703 9.975L33.9762 10.1284C34.0586 11.1993 34.9935 12.0006 36.0643 11.9182L35.9109 9.9241ZM35.86 9.98349C35.8577 9.95303 35.8805 9.92644 35.9109 9.9241L36.0643 11.9182C37.1352 11.8358 37.9365 10.901 37.8542 9.83009L35.86 9.98349ZM35.4267 4.34944L35.86 9.98349L37.8542 9.83009L37.4208 4.19605L35.4267 4.34944Z" fill="#636466"/>
              </svg>
              </span>
              </a> på uppdrag av Sveriges regioner.
        </div>
      </div>
    </div>
  </div>
`;

const footerOpenDesktop = `
  <div class="ic-page-footer__desktop iu-hide-sm iu-hide-md">

    <div class="iu-grid-cols-lg-5 ic-container iu-mb-500">
      <h1 class="ic-page-footer__heading iu-grid-span-lg-2 iu-color-secondary-dark">
        Om <span>[Tjänstens namn]</span>
      </h1>
    </div>

    <div class="iu-grid-cols-lg-5 ic-container">
      <div class="iu-grid-span-lg-2 ic-text">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Praesent nisl lacus, vulputate commodo sagittis sit amet,
          scelerisque vel tortor.
        </p>
        <p>
          Donec ultrices lobortis nulla, sit amet suscipit tellus
          accumsan non. Quisque rutrum aliquet elit id dignissim.
          Nulla accumsan orci enim, ut feugiat justo luctus non.
        </p>
      </div>

      <nav class="iu-grid iu-grid-span-lg-3 iu-pl-xl" aria-label="sidfot meny">
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 1</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 2</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 3</a>
          </li>
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 4</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 5</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 6</a>
          </li>
          <li>
            <a class="ic-link-chevron ic-link ic-link--external" href="javascript:void(0)">External link 1
              <svg width="18" height="16" viewBox="0 0 40 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.3871 1.87695C9.23218 1.87695 1 10.1091 1 20.264C1 30.419 9.23218 38.6511 19.3871 38.6511C29.542 38.6511 37.7742 30.419 37.7742 20.264" stroke="#a00b36" stroke-width="4" stroke-linecap="round"/>
                <path d="M36.3616 3.4654L37.3587 3.3887L37.2929 2.53408L36.4383 2.46834L36.3616 3.4654ZM36.4237 4.27275L37.4208 4.19605L37.4208 4.19605L36.4237 4.27275ZM35.5543 3.40329L35.4776 4.40035L35.4776 4.40035L35.5543 3.40329ZM29.9202 2.96991L29.8435 3.96696L29.8435 3.96696L29.9202 2.96991ZM28.9059 3.83936L27.9088 3.76266L27.9088 3.76266L28.9059 3.83936ZM29.7753 4.85372L29.6986 5.85077L29.6986 5.85077L29.7753 4.85372ZM33.5623 5.14503L34.2694 5.85213L35.8068 4.31473L33.639 4.14797L33.5623 5.14503ZM18.7786 19.9288L18.0715 19.2217L18.7786 19.9288ZM18.7786 21.2648L19.4857 20.5577L19.4857 20.5577L18.7786 21.2648ZM34.7129 6.66644L35.7099 6.58974L35.5432 4.42192L34.0058 5.95933L34.7129 6.66644ZM34.9733 10.0517L35.9703 9.975L35.9703 9.975L34.9733 10.0517ZM35.9876 10.9212L36.0643 11.9182L36.0643 11.9182L35.9876 10.9212ZM36.8571 9.90679L35.86 9.98349L35.86 9.98349L36.8571 9.90679ZM35.3646 3.54209L35.4267 4.34944L37.4208 4.19605L37.3587 3.3887L35.3646 3.54209ZM35.4776 4.40035L36.2849 4.46245L36.4383 2.46834L35.631 2.40624L35.4776 4.40035ZM29.8435 3.96696L35.4776 4.40035L35.631 2.40624L29.9969 1.97285L29.8435 3.96696ZM29.9029 3.91605C29.9006 3.94651 29.874 3.9693 29.8435 3.96696L29.9969 1.97285C28.9261 1.89048 27.9912 2.6918 27.9088 3.76266L29.9029 3.91605ZM29.852 3.85666C29.8825 3.85901 29.9052 3.8856 29.9029 3.91605L27.9088 3.76266C27.8264 4.83352 28.6277 5.7684 29.6986 5.85077L29.852 3.85666ZM33.639 4.14797L29.852 3.85666L29.6986 5.85077L33.4856 6.14208L33.639 4.14797ZM19.4857 20.6359L34.2694 5.85213L32.8552 4.43792L18.0715 19.2217L19.4857 20.6359ZM19.4857 20.5577C19.5073 20.5793 19.5073 20.6143 19.4857 20.6359L18.0715 19.2217C17.312 19.9811 17.312 21.2124 18.0715 21.9719L19.4857 20.5577ZM19.4074 20.5577C19.429 20.5361 19.4641 20.5361 19.4857 20.5577L18.0714 21.9719C18.8309 22.7313 20.0622 22.7313 20.8217 21.9719L19.4074 20.5577ZM34.0058 5.95933L19.4074 20.5577L20.8217 21.9719L35.42 7.37354L34.0058 5.95933ZM35.9703 9.975L35.7099 6.58974L33.7158 6.74313L33.9762 10.1284L35.9703 9.975ZM35.9109 9.9241C35.9414 9.92175 35.968 9.94455 35.9703 9.975L33.9762 10.1284C34.0586 11.1993 34.9935 12.0006 36.0643 11.9182L35.9109 9.9241ZM35.86 9.98349C35.8577 9.95303 35.8805 9.92644 35.9109 9.9241L36.0643 11.9182C37.1352 11.8358 37.9365 10.901 37.8542 9.83009L35.86 9.98349ZM35.4267 4.34944L35.86 9.98349L37.8542 9.83009L37.4208 4.19605L35.4267 4.34944Z" fill="#a00b36"/>
              </svg>
            </a>
          </li>
        </ul>
      </nav>
    </div>

    <div class="ic-page-footer__about">
      <div class="ic-page-footer__about__content ic-container--narrow-md">
      
      <ul class="ic-page-footer__about__content__information">
      <li><span>[Tjänstens namn]</span> drivs av <a
      class="ic-link-footer"
      href="https://www.inera.se/"
    >
      Inera AB <span>
    <svg width="14" height="14" viewBox="0 -7 40 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M19.3871 1.87695C9.23218 1.87695 1 10.1091 1 20.264C1 30.419 9.23218 38.6511 19.3871 38.6511C29.542 38.6511 37.7742 30.419 37.7742 20.264" stroke="#fff" stroke-width="4" stroke-linecap="round"/>
            <path d="M36.3616 3.4654L37.3587 3.3887L37.2929 2.53408L36.4383 2.46834L36.3616 3.4654ZM36.4237 4.27275L37.4208 4.19605L37.4208 4.19605L36.4237 4.27275ZM35.5543 3.40329L35.4776 4.40035L35.4776 4.40035L35.5543 3.40329ZM29.9202 2.96991L29.8435 3.96696L29.8435 3.96696L29.9202 2.96991ZM28.9059 3.83936L27.9088 3.76266L27.9088 3.76266L28.9059 3.83936ZM29.7753 4.85372L29.6986 5.85077L29.6986 5.85077L29.7753 4.85372ZM33.5623 5.14503L34.2694 5.85213L35.8068 4.31473L33.639 4.14797L33.5623 5.14503ZM18.7786 19.9288L18.0715 19.2217L18.7786 19.9288ZM18.7786 21.2648L19.4857 20.5577L19.4857 20.5577L18.7786 21.2648ZM34.7129 6.66644L35.7099 6.58974L35.5432 4.42192L34.0058 5.95933L34.7129 6.66644ZM34.9733 10.0517L35.9703 9.975L35.9703 9.975L34.9733 10.0517ZM35.9876 10.9212L36.0643 11.9182L36.0643 11.9182L35.9876 10.9212ZM36.8571 9.90679L35.86 9.98349L35.86 9.98349L36.8571 9.90679ZM35.3646 3.54209L35.4267 4.34944L37.4208 4.19605L37.3587 3.3887L35.3646 3.54209ZM35.4776 4.40035L36.2849 4.46245L36.4383 2.46834L35.631 2.40624L35.4776 4.40035ZM29.8435 3.96696L35.4776 4.40035L35.631 2.40624L29.9969 1.97285L29.8435 3.96696ZM29.9029 3.91605C29.9006 3.94651 29.874 3.9693 29.8435 3.96696L29.9969 1.97285C28.9261 1.89048 27.9912 2.6918 27.9088 3.76266L29.9029 3.91605ZM29.852 3.85666C29.8825 3.85901 29.9052 3.8856 29.9029 3.91605L27.9088 3.76266C27.8264 4.83352 28.6277 5.7684 29.6986 5.85077L29.852 3.85666ZM33.639 4.14797L29.852 3.85666L29.6986 5.85077L33.4856 6.14208L33.639 4.14797ZM19.4857 20.6359L34.2694 5.85213L32.8552 4.43792L18.0715 19.2217L19.4857 20.6359ZM19.4857 20.5577C19.5073 20.5793 19.5073 20.6143 19.4857 20.6359L18.0715 19.2217C17.312 19.9811 17.312 21.2124 18.0715 21.9719L19.4857 20.5577ZM19.4074 20.5577C19.429 20.5361 19.4641 20.5361 19.4857 20.5577L18.0714 21.9719C18.8309 22.7313 20.0622 22.7313 20.8217 21.9719L19.4074 20.5577ZM34.0058 5.95933L19.4074 20.5577L20.8217 21.9719L35.42 7.37354L34.0058 5.95933ZM35.9703 9.975L35.7099 6.58974L33.7158 6.74313L33.9762 10.1284L35.9703 9.975ZM35.9109 9.9241C35.9414 9.92175 35.968 9.94455 35.9703 9.975L33.9762 10.1284C34.0586 11.1993 34.9935 12.0006 36.0643 11.9182L35.9109 9.9241ZM35.86 9.98349C35.8577 9.95303 35.8805 9.92644 35.9109 9.9241L36.0643 11.9182C37.1352 11.8358 37.9365 10.901 37.8542 9.83009L35.86 9.98349ZM35.4267 4.34944L35.86 9.98349L37.8542 9.83009L37.4208 4.19605L35.4267 4.34944Z" fill="#fff"/>
          </svg>
          </span>
          </a> 
          på uppdrag av Sveriges regioner.
          </li>
          <li><a class="ic-page-footer__about__content__information__link" href="javascript:void(0)">Behandling av personuppgifter</a></li>
          <li><a class="ic-page-footer__about__content__information__link" href="javascript:void(0)">Hantering av kakor</a></li>
          <li><a class="ic-page-footer__about__content__information__link" href="javascript:void(0)">Inställningar för kakor</a></li>
        </ul>
      </div>
    </div>
  </div>
`;

const footerMobile = `
  <div class="ic-page-footer__mobile iu-hide-from-lg ic-container">
    <h1 class="ic-page-footer__heading iu-color-secondary-dark iu-mb-500">
      Om <span>[Tjänstens namn]</span>
    </h1>

    <div class="ic-text iu-mb-1000">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Praesent nisl lacus, vulputate commodo sagittis sit amet,
        scelerisque vel tortor.
      </p>
      <p>
        Donec ultrices lobortis nulla, sit amet suscipit tellus
        accumsan non. Quisque rutrum aliquet elit id dignissim.
        Nulla accumsan orci enim, ut feugiat justo luctus non.
      </p>
    </div>

    <div class="ic-page-footer__menu iu-bg-main">
    <nav class="ic-nav-list" id="mobile-nav" aria-label="sidfot meny">
      <ul class="ic-nav-list__list">
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 1</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-0"
              class="ic-nav-list__expandpro"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
        </li>
        <li class="ic-nav-list__item__no__style" aria-expanded="true">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 2</a>
            <button
              aria-expanded="true"
              aria-controls="submenu-1"
              class="ic-nav-list__expandpro"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 2</span>
            </button>
          </div>
          <ul class="ic-nav-list__listpro" id="submenu-1">
            <li class="ic-nav-list__item__sub"><div class="iu-px-none ic-container--narrow-md"><a href="javascript:void(0)">Submenu item 1</a></div></li>
            <li class="ic-nav-list__item__sub"><div class="iu-px-none ic-container--narrow-md"><a href="javascript:void(0)">Submenu item 2</a></div></li>
            <li class="ic-nav-list__item__sub"><div class="iu-px-none ic-container--narrow-md"><a href="javascript:void(0)">Submenu item 3</a></div></li>
          </ul>
        </li>
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 3</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-2"
              class="ic-nav-list__expandpro"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 3</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-2"></ul>
        </li>
        <li aria-hidden="true"><!-- add last shadow element --></li>
      </ul>
    </nav>
  </div>

    <div class="ic-page-footer__about">
      <div class="ic-page-footer__about__links">
        <ul class="ic-cookie-list ic-link-list iu-fs-200">
          <li><a class="ic-cookie-list--link" href="javascript:void(0)">Behandling av personuppgifter</a></li>
          <li><a class="ic-cookie-list--link" href="javascript:void(0)">Hantering av kakor</a></li>
          <li><a class="ic-cookie-list--link" href="javascript:void(0)">Inställningar för kakor</a></li>
        </ul>
      </div>

      <div class="ic-page-footer__about__who">
        <a class="ic-page-footer__about__who__service" href="/" aria-label="[Tjänstens namn], startsida" tabindex="-1">
          <span class="ic-page-footer__logo-profession"></span>
          <span class="ic-page-header__line-vertical"></span>
          <span class="ic-page-footer__about__who__service--name">[Tjänstens namn]</span>
        </a>

        <div class="ic-page-footer__about__who--responsible">
          <span>[Tjänstens namn]</span> 
          drivs av <a href="https://www.inera.se/" style = "text-decoration: none;">
          <span style = "color: #636466; !important;
          border-bottom: 2px solid currentColor;
          display: inline-block;
          line-height: 0.85;"> Inera AB </span> </a>
          <a style="color: #636466; !important; 
          text-decoration: none;
          class="ic-link-footer"
          href="https://www.inera.se/"
        >
        <svg width="14" height="14" viewBox="0 -7 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.3871 1.87695C9.23218 1.87695 1 10.1091 1 20.264C1 30.419 9.23218 38.6511 19.3871 38.6511C29.542 38.6511 37.7742 30.419 37.7742 20.264" stroke="#636466" stroke-width="4" stroke-linecap="round"/>
                <path d="M36.3616 3.4654L37.3587 3.3887L37.2929 2.53408L36.4383 2.46834L36.3616 3.4654ZM36.4237 4.27275L37.4208 4.19605L37.4208 4.19605L36.4237 4.27275ZM35.5543 3.40329L35.4776 4.40035L35.4776 4.40035L35.5543 3.40329ZM29.9202 2.96991L29.8435 3.96696L29.8435 3.96696L29.9202 2.96991ZM28.9059 3.83936L27.9088 3.76266L27.9088 3.76266L28.9059 3.83936ZM29.7753 4.85372L29.6986 5.85077L29.6986 5.85077L29.7753 4.85372ZM33.5623 5.14503L34.2694 5.85213L35.8068 4.31473L33.639 4.14797L33.5623 5.14503ZM18.7786 19.9288L18.0715 19.2217L18.7786 19.9288ZM18.7786 21.2648L19.4857 20.5577L19.4857 20.5577L18.7786 21.2648ZM34.7129 6.66644L35.7099 6.58974L35.5432 4.42192L34.0058 5.95933L34.7129 6.66644ZM34.9733 10.0517L35.9703 9.975L35.9703 9.975L34.9733 10.0517ZM35.9876 10.9212L36.0643 11.9182L36.0643 11.9182L35.9876 10.9212ZM36.8571 9.90679L35.86 9.98349L35.86 9.98349L36.8571 9.90679ZM35.3646 3.54209L35.4267 4.34944L37.4208 4.19605L37.3587 3.3887L35.3646 3.54209ZM35.4776 4.40035L36.2849 4.46245L36.4383 2.46834L35.631 2.40624L35.4776 4.40035ZM29.8435 3.96696L35.4776 4.40035L35.631 2.40624L29.9969 1.97285L29.8435 3.96696ZM29.9029 3.91605C29.9006 3.94651 29.874 3.9693 29.8435 3.96696L29.9969 1.97285C28.9261 1.89048 27.9912 2.6918 27.9088 3.76266L29.9029 3.91605ZM29.852 3.85666C29.8825 3.85901 29.9052 3.8856 29.9029 3.91605L27.9088 3.76266C27.8264 4.83352 28.6277 5.7684 29.6986 5.85077L29.852 3.85666ZM33.639 4.14797L29.852 3.85666L29.6986 5.85077L33.4856 6.14208L33.639 4.14797ZM19.4857 20.6359L34.2694 5.85213L32.8552 4.43792L18.0715 19.2217L19.4857 20.6359ZM19.4857 20.5577C19.5073 20.5793 19.5073 20.6143 19.4857 20.6359L18.0715 19.2217C17.312 19.9811 17.312 21.2124 18.0715 21.9719L19.4857 20.5577ZM19.4074 20.5577C19.429 20.5361 19.4641 20.5361 19.4857 20.5577L18.0714 21.9719C18.8309 22.7313 20.0622 22.7313 20.8217 21.9719L19.4074 20.5577ZM34.0058 5.95933L19.4074 20.5577L20.8217 21.9719L35.42 7.37354L34.0058 5.95933ZM35.9703 9.975L35.7099 6.58974L33.7158 6.74313L33.9762 10.1284L35.9703 9.975ZM35.9109 9.9241C35.9414 9.92175 35.968 9.94455 35.9703 9.975L33.9762 10.1284C34.0586 11.1993 34.9935 12.0006 36.0643 11.9182L35.9109 9.9241ZM35.86 9.98349C35.8577 9.95303 35.8805 9.92644 35.9109 9.9241L36.0643 11.9182C37.1352 11.8358 37.9365 10.901 37.8542 9.83009L35.86 9.98349ZM35.4267 4.34944L35.86 9.98349L37.8542 9.83009L37.4208 4.19605L35.4267 4.34944Z" fill="#636466"/>
              </svg>
              </span>
              </a> på uppdrag av Sveriges regioner.
        </div>
      </div>
    </div>
  </div>
`;

const footerDesktop = `
  <div class="ic-page-footer__desktop iu-hide-sm iu-hide-md">

    <div class="iu-grid-cols-lg-5 ic-container iu-mb-500">
      <h1 class="ic-page-footer__heading iu-grid-span-lg-2 iu-color-secondary-dark">
        Om <span>[Tjänstens namn]</span>
      </h1>
    </div>

    <div class="iu-grid-cols-lg-5 ic-container">
      <div class="iu-grid-span-lg-2 ic-text">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Praesent nisl lacus, vulputate commodo sagittis sit amet,
          scelerisque vel tortor.
        </p>
        <p>
          Donec ultrices lobortis nulla, sit amet suscipit tellus
          accumsan non. Quisque rutrum aliquet elit id dignissim.
          Nulla accumsan orci enim, ut feugiat justo luctus non.
        </p>
      </div>

      <nav class="iu-grid iu-grid-span-lg-3 iu-pl-xl" aria-label="sidfot meny">
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 1</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 2</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 3</a>
          </li>
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 4</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 5</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 6</a>
          </li>
          <li>
            <a class="ic-link-chevron ic-link ic-link--external" href="javascript:void(0)">External link 1
              <svg width="18" height="16" viewBox="0 0 40 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.3871 1.87695C9.23218 1.87695 1 10.1091 1 20.264C1 30.419 9.23218 38.6511 19.3871 38.6511C29.542 38.6511 37.7742 30.419 37.7742 20.264" stroke="#a00b36" stroke-width="4" stroke-linecap="round"/>
                <path d="M36.3616 3.4654L37.3587 3.3887L37.2929 2.53408L36.4383 2.46834L36.3616 3.4654ZM36.4237 4.27275L37.4208 4.19605L37.4208 4.19605L36.4237 4.27275ZM35.5543 3.40329L35.4776 4.40035L35.4776 4.40035L35.5543 3.40329ZM29.9202 2.96991L29.8435 3.96696L29.8435 3.96696L29.9202 2.96991ZM28.9059 3.83936L27.9088 3.76266L27.9088 3.76266L28.9059 3.83936ZM29.7753 4.85372L29.6986 5.85077L29.6986 5.85077L29.7753 4.85372ZM33.5623 5.14503L34.2694 5.85213L35.8068 4.31473L33.639 4.14797L33.5623 5.14503ZM18.7786 19.9288L18.0715 19.2217L18.7786 19.9288ZM18.7786 21.2648L19.4857 20.5577L19.4857 20.5577L18.7786 21.2648ZM34.7129 6.66644L35.7099 6.58974L35.5432 4.42192L34.0058 5.95933L34.7129 6.66644ZM34.9733 10.0517L35.9703 9.975L35.9703 9.975L34.9733 10.0517ZM35.9876 10.9212L36.0643 11.9182L36.0643 11.9182L35.9876 10.9212ZM36.8571 9.90679L35.86 9.98349L35.86 9.98349L36.8571 9.90679ZM35.3646 3.54209L35.4267 4.34944L37.4208 4.19605L37.3587 3.3887L35.3646 3.54209ZM35.4776 4.40035L36.2849 4.46245L36.4383 2.46834L35.631 2.40624L35.4776 4.40035ZM29.8435 3.96696L35.4776 4.40035L35.631 2.40624L29.9969 1.97285L29.8435 3.96696ZM29.9029 3.91605C29.9006 3.94651 29.874 3.9693 29.8435 3.96696L29.9969 1.97285C28.9261 1.89048 27.9912 2.6918 27.9088 3.76266L29.9029 3.91605ZM29.852 3.85666C29.8825 3.85901 29.9052 3.8856 29.9029 3.91605L27.9088 3.76266C27.8264 4.83352 28.6277 5.7684 29.6986 5.85077L29.852 3.85666ZM33.639 4.14797L29.852 3.85666L29.6986 5.85077L33.4856 6.14208L33.639 4.14797ZM19.4857 20.6359L34.2694 5.85213L32.8552 4.43792L18.0715 19.2217L19.4857 20.6359ZM19.4857 20.5577C19.5073 20.5793 19.5073 20.6143 19.4857 20.6359L18.0715 19.2217C17.312 19.9811 17.312 21.2124 18.0715 21.9719L19.4857 20.5577ZM19.4074 20.5577C19.429 20.5361 19.4641 20.5361 19.4857 20.5577L18.0714 21.9719C18.8309 22.7313 20.0622 22.7313 20.8217 21.9719L19.4074 20.5577ZM34.0058 5.95933L19.4074 20.5577L20.8217 21.9719L35.42 7.37354L34.0058 5.95933ZM35.9703 9.975L35.7099 6.58974L33.7158 6.74313L33.9762 10.1284L35.9703 9.975ZM35.9109 9.9241C35.9414 9.92175 35.968 9.94455 35.9703 9.975L33.9762 10.1284C34.0586 11.1993 34.9935 12.0006 36.0643 11.9182L35.9109 9.9241ZM35.86 9.98349C35.8577 9.95303 35.8805 9.92644 35.9109 9.9241L36.0643 11.9182C37.1352 11.8358 37.9365 10.901 37.8542 9.83009L35.86 9.98349ZM35.4267 4.34944L35.86 9.98349L37.8542 9.83009L37.4208 4.19605L35.4267 4.34944Z" fill="#a00b36"/>
              </svg>
            </a>
          </li>
        </ul>
      </nav>
    </div>

    <div class="ic-page-footer__about">
      <div class="ic-page-footer__about__content ic-container--narrow-md">
      
      <ul class="ic-page-footer__about__content__information">
      <li><span>[Tjänstens namn]</span> drivs av <a
      class="ic-link-footer"
      href="https://www.inera.se/"
    >
      Inera AB <span>
    <svg width="14" height="14" viewBox="0 -7 40 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M19.3871 1.87695C9.23218 1.87695 1 10.1091 1 20.264C1 30.419 9.23218 38.6511 19.3871 38.6511C29.542 38.6511 37.7742 30.419 37.7742 20.264" stroke="#fff" stroke-width="4" stroke-linecap="round"/>
            <path d="M36.3616 3.4654L37.3587 3.3887L37.2929 2.53408L36.4383 2.46834L36.3616 3.4654ZM36.4237 4.27275L37.4208 4.19605L37.4208 4.19605L36.4237 4.27275ZM35.5543 3.40329L35.4776 4.40035L35.4776 4.40035L35.5543 3.40329ZM29.9202 2.96991L29.8435 3.96696L29.8435 3.96696L29.9202 2.96991ZM28.9059 3.83936L27.9088 3.76266L27.9088 3.76266L28.9059 3.83936ZM29.7753 4.85372L29.6986 5.85077L29.6986 5.85077L29.7753 4.85372ZM33.5623 5.14503L34.2694 5.85213L35.8068 4.31473L33.639 4.14797L33.5623 5.14503ZM18.7786 19.9288L18.0715 19.2217L18.7786 19.9288ZM18.7786 21.2648L19.4857 20.5577L19.4857 20.5577L18.7786 21.2648ZM34.7129 6.66644L35.7099 6.58974L35.5432 4.42192L34.0058 5.95933L34.7129 6.66644ZM34.9733 10.0517L35.9703 9.975L35.9703 9.975L34.9733 10.0517ZM35.9876 10.9212L36.0643 11.9182L36.0643 11.9182L35.9876 10.9212ZM36.8571 9.90679L35.86 9.98349L35.86 9.98349L36.8571 9.90679ZM35.3646 3.54209L35.4267 4.34944L37.4208 4.19605L37.3587 3.3887L35.3646 3.54209ZM35.4776 4.40035L36.2849 4.46245L36.4383 2.46834L35.631 2.40624L35.4776 4.40035ZM29.8435 3.96696L35.4776 4.40035L35.631 2.40624L29.9969 1.97285L29.8435 3.96696ZM29.9029 3.91605C29.9006 3.94651 29.874 3.9693 29.8435 3.96696L29.9969 1.97285C28.9261 1.89048 27.9912 2.6918 27.9088 3.76266L29.9029 3.91605ZM29.852 3.85666C29.8825 3.85901 29.9052 3.8856 29.9029 3.91605L27.9088 3.76266C27.8264 4.83352 28.6277 5.7684 29.6986 5.85077L29.852 3.85666ZM33.639 4.14797L29.852 3.85666L29.6986 5.85077L33.4856 6.14208L33.639 4.14797ZM19.4857 20.6359L34.2694 5.85213L32.8552 4.43792L18.0715 19.2217L19.4857 20.6359ZM19.4857 20.5577C19.5073 20.5793 19.5073 20.6143 19.4857 20.6359L18.0715 19.2217C17.312 19.9811 17.312 21.2124 18.0715 21.9719L19.4857 20.5577ZM19.4074 20.5577C19.429 20.5361 19.4641 20.5361 19.4857 20.5577L18.0714 21.9719C18.8309 22.7313 20.0622 22.7313 20.8217 21.9719L19.4074 20.5577ZM34.0058 5.95933L19.4074 20.5577L20.8217 21.9719L35.42 7.37354L34.0058 5.95933ZM35.9703 9.975L35.7099 6.58974L33.7158 6.74313L33.9762 10.1284L35.9703 9.975ZM35.9109 9.9241C35.9414 9.92175 35.968 9.94455 35.9703 9.975L33.9762 10.1284C34.0586 11.1993 34.9935 12.0006 36.0643 11.9182L35.9109 9.9241ZM35.86 9.98349C35.8577 9.95303 35.8805 9.92644 35.9109 9.9241L36.0643 11.9182C37.1352 11.8358 37.9365 10.901 37.8542 9.83009L35.86 9.98349ZM35.4267 4.34944L35.86 9.98349L37.8542 9.83009L37.4208 4.19605L35.4267 4.34944Z" fill="#fff"/>
          </svg>
          </span>
          </a> 
          på uppdrag av Sveriges regioner.
          </li>
          <li><a class="ic-page-footer__about__content__information__link" href="javascript:void(0)">Behandling av personuppgifter</a></li>
          <li><a class="ic-page-footer__about__content__information__link" href="javascript:void(0)">Hantering av kakor</a></li>
          <li><a class="ic-page-footer__about__content__information__link" href="javascript:void(0)">Inställningar för kakor</a></li>
        </ul>
      </div>
    </div>
  </div>
`;

export const Footer = () =>
  markupWithCodeExample([
    `
<footer class="ic-page-footer">
  ${footerOpenDesktop}
  ${footerOpenMobile}
</footer>
`,
  ]);

  export const OpenFooter = () =>
  markupWithCodeExample([
    `
<footer class="ic-page-footer">
  ${footerDesktop}
  ${footerMobile}
</footer>
`,
  ]);
