import { markupWithCodeExample, bleedContainer } from "../../helpers";

export default {
  title: "components/Page Footer/Inera",
  decorators: [bleedContainer],
  parameters: {
    status: "",
    theme: "inera",
  },
};

export const Footer = () =>
  markupWithCodeExample([
    `
<footer class="ic-page-footer">
  <div class="ic-page-footer__inner">

    <div class="iu-grid-cols-lg-5 ic-container--narrow-md ic-container iu-mb-500">
      <h2 class="ic-page-footer__heading iu-grid-span-lg-2">
        <span class="iu-color-white">Inera</span>
      </h2>
    </div>
    <div class="iu-grid-cols-lg-12 ic-container--narrow-md ic-container">
      <div class="iu-grid-span-lg-5 iu-color-white ic-text">
        <p>
          Inera skapar förutsättningar för att digitalisera välfärden genom att tillhandahålla en gemensam digital infrastruktur och arkitektur.
        </p>
      </div>
      <nav class="iu-grid iu-grid-span-lg-7 iu-grid-cols-3 iu-pl-xxl iu-hide-sm iu-hide-md" aria-label="sidfot meny">
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 1</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 2</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 3</a>
          </li>
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 4</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 5</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 6</a>
          </li>
          <li>
            <a class="ic-link-chevron ic-link ic-link--external" href="javascript:void(0)">
              External link 1
              <span aria-hidden="true" class="icon-arrow-rigt-ext-circle"></span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="ic-page-footer__menu iu-hide-from-lg iu-bg-main">
    <nav class="ic-nav-list" id="mobile-nav" aria-label="sidfot meny">
      <ul class="ic-nav-list__list">
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 1</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-0"
              class="ic-nav-list__expandinera"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
        </li>
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 2</a>
            <button
              aria-expanded="true"
              aria-controls="submenu-1"
              class="ic-nav-list__expandinera"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 2</span>
            </button>
          </div>
        </li>
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 3</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-2"
              class="ic-nav-list__expandinera"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 3</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-2"></ul>
        </li>
        <li aria-hidden="true"><!-- add last shadow element --></li>
      </ul>
    </nav>
  </div>
  <hr class="line" />
  <div class="">
    <div class="ic-container--narrow-md">
      <ul class="ic-cookie-list ic-link-list ic-link-list--nav iu-fs-200">
        <li><a class="" href="javascript:void(0)">Cookiepolicy</a></li>
        <li><button type="button">Cookie inställningar</button></li>
      </ul>
    </div>
  </div>
</footer>
`,
  ]);

  export const OpenFooter = () =>
  markupWithCodeExample([
    `
<footer class="ic-page-footer">
  <div class="ic-page-footer__inner">

    <div class="iu-grid-cols-lg-5 ic-container--narrow-md ic-container iu-mb-500">
      <h2 class="ic-page-footer__heading iu-grid-span-lg-2">
        <span class="iu-color-white">Inera</span>
      </h2>
    </div>
    <div class="iu-grid-cols-lg-12 ic-container--narrow-md ic-container">
      <div class="iu-grid-span-lg-5 iu-color-white ic-text">
        <p>
          Inera skapar förutsättningar för att digitalisera välfärden genom att tillhandahålla en gemensam digital infrastruktur och arkitektur.
        </p>
      </div>
      <nav class="iu-grid iu-grid-span-lg-7 iu-grid-cols-3 iu-pl-xxl iu-hide-sm iu-hide-md" aria-label="sidfot meny">
        <ul class="ic-link-list ic-link-list--nav">
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 1</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 2</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 3</a>
          </li>
          <li>
            <a class="ic-link-chevron" href="javascript:void(0)">Internal link 4</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 5</a>
          </li>
          <li>
            <a class="ic-link-chevron"  href="javascript:void(0)">Internal link 6</a>
          </li>
          <li>
            <a class="ic-link-chevron ic-link ic-link--external" href="javascript:void(0)">
              External link 1
              <span aria-hidden="true" class="icon-arrow-rigt-ext-circle"></span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="ic-page-footer__menu iu-hide-from-lg iu-bg-main">
    <nav class="ic-nav-list" id="mobile-nav" aria-label="sidfot meny">
      <ul class="ic-nav-list__list">
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 1</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-0"
              class="ic-nav-list__expandinera"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
        </li>
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 2</a>
            <button
              aria-expanded="true"
              aria-controls="submenu-1"
              class="ic-nav-list__expandinera"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 2</span>
            </button>
          </div>
          <ul class="ic-nav-list__list__inera" id="submenu-1">
            <li class="ic-nav-list__item"><div class="iu-px-none ic-container--narrow-md"><a href="javascript:void(0)">Submenu item 1</a></div></li>
            <li class="ic-nav-list__item"><div class="iu-px-none ic-container--narrow-md"><a href="javascript:void(0)">Submenu item 2</a></div></li>
            <li class="ic-nav-list__item"><div class="iu-px-none ic-container--narrow-md"><a href="javascript:void(0)">Submenu item 3</a></div></li>
          </ul>
        </li>
        <li class="ic-nav-list__item">
          <div class="ic-container--narrow-md iu-px-none">
            <a href="javascript:void(0)">Menu item 3</a>
            <button
              aria-expanded="false"
              aria-controls="submenu-2"
              class="ic-nav-list__expandinera"
            >
              <span class="iu-sr-only">Visa innehåll för Menu item 3</span>
            </button>
          </div>
          <ul hidden class="ic-nav-list__list" id="submenu-2"></ul>
        </li>
        <li aria-hidden="true"><!-- add last shadow element --></li>
      </ul>
    </nav>
  </div>
  <hr class="line" />
  <div class="">
    <div class="ic-container--narrow-md">
      <ul class="ic-cookie-list ic-link-list ic-link-list--nav iu-fs-200">
        <li><a class="" href="javascript:void(0)">Cookiepolicy</a></li>
        <li><button type="button">Cookie inställningar</button></li>
      </ul>
    </div>
  </div>
</footer>
`,
  ]);
