import { storiesOf } from "@storybook/html";
import { useEffect } from "@storybook/client-api";
import { markupWithCodeExample } from "./helpers";

function cssOutput(value) {
  return value;
}

function showCSS() {
  // Naive way to make sure the css is loaded
  setTimeout(() => {
    let showCSSelements = document.querySelectorAll("[data-showcss]");
    Array.from(showCSSelements).forEach(function displayCSS(element) {
      let styles = window.getComputedStyle(element);
      element.innerText = element.dataset.showcss
        .split(" ")
        .reduce(
          function combineText(a, c) {
            return [...a, `${c}: ${cssOutput(styles[c])}`];
          },
          [element.innerText]
        )
        .join(" ");
    });
  }, 500);
}

const mixin = (m, v) => `sass mixin: @include ${m}('${v}')`;

const lh = ["narrow", "body", "heading"];
const LineHeight = () =>
  markupWithCodeExample(
    lh.map(
      (v) => `
<p data-showcss="line-height" class="iu-lh-${v}">
  lh-${v}: <!-- computed value -->
</p>
`
    )
  );

const fw = ["body", "heading", "bold"];
const FontWeight = () =>
  markupWithCodeExample(
    fw.map(
      (v) => `
<p data-showcss="font-weight" class="iu-fw-${v}">
  fw-${v}: <!-- computed value -->
</p>`
    )
  );

const fontSizes = [
  "100",
  "200",
  "300",
  "400",
  "500",
  "600",
  "700",
  "800",
  "900",
  "h1",
  "h2",
  "h3",
  "h4",
  "body",
  "xs",
  "sm",
  "md",
  "lg",
  "xl",
  "xxl",
];
const FontSize = () =>
  markupWithCodeExample(
    fontSizes.map(
      (v) => `
<p data-showcss="font-size" class="iu-fs-${v}">
  fs-${v}: <!-- computed value -->
</p>`
    )
  );

const textTransform = ["button", "uc", "lc"];
const TextTransform = () =>
  markupWithCodeExample(
    textTransform.map(
      (v) => `
<p data-showcss="text-transform" class="iu-tt-${v}">
  tt-${v}: <!-- computed value -->
</p>`
    )
  );

const shadow = ["none", "card", "xs", "sm", "md", "lg", "xl"];
const BoxShadow = () =>
  markupWithCodeExample(
    shadow.map(
      (v) => `
<p data-showcss="box-shadow" class="iu-shadow-${v}">
  shadow-${v}: <!-- computed value -->
</p>`
    )
  );

const radius = ["none", "round", "card", "xs", "sm", "md", "lg", "xl"];
const BorderRadius = () =>
  markupWithCodeExample(
    radius.map(
      (v) => `
<p data-showcss="border-radius"
  style="width: 150px; height: 150px;"
  class="iu-bg-muted iu-color-white iu-fs-100 iu-flex-center iu-text-center iu-radius-${v}">
  radius-${v}: <!-- computed value -->
</p>`
    )
  );

const fonts = ["body", "secondary", "heading", "monospace"];
const FontFamily = () =>
  markupWithCodeExample(
    fonts.map(
      (v) => `
<p data-showcss="font-family" class="iu-ff-${v}">
  ff-${v}: <!-- computed value -->
</p>`
    )
  );

const colors = [
  "main",
  "secondary-dark",
  "secondary-light",
  "black",
  "white",
  "text",
  "background",
  "cta",
  "cta-dark",
  "cta-light",
  "cta-text:",
  "nav",
  "nav-dark",
  "nav-light",
  "nav-text",
  "muted",
  "highlight",
  "warning",
  "error",
  "success",
  "information",
];

const TextColor = () =>
  markupWithCodeExample(
    colors.map(
      (color) => `
<p data-showcss="color" class="iu-color-${color}">
  ${color}: <!-- computed value -->
</p>`
    )
  );

const BackgroundColor = () =>
  markupWithCodeExample(
    colors.map(
      (color) => `
<p data-showcss="background-color" class="iu-bg-${color} iu-color-white" style="text-shadow: 1px 1px 0 black">
  bg-${color}: <!-- computed value -->
</p>`
    )
  );

const Border = () =>
  markupWithCodeExample(
    colors.map(
      (color) => `
<p data-showcss="border" class="iu-border-${color}">
  border: ${color}
</p>`
    )
  );

const BorderWidth = () =>
  markupWithCodeExample(
    [1, 2, 3].map(
      (s) => `
<p data-showcss="border" class="iu-border-main iu-rem-border-${s}">
  border: ${s}px
</p>`
    )
  );

const spacingValues = [
  "none",
  "100",
  "200",
  "300",
  "400",
  "500",
  "600",
  "700",
  "800",
  "900",
  "xs",
  "sm",
  "md",
  "lg",
  "xl",
  "xxl",
];

const spacingPrefixes = ["", "t", "b", "l", "r", "y", "x"];

const paddingMarkup = spacingPrefixes
  .map((prefix) =>
    markupWithCodeExample(
      spacingValues.map(
        (value) => `
<p data-showcss="padding" class="iu-bg-muted iu-color-white iu-p${prefix}-${value}">
  p${prefix}-${value}: <!-- computed value -->
</p>`
      )
    )
  )
  .join("");

const Padding = () => paddingMarkup;

const marginMarkup = spacingPrefixes
  .map((prefix) =>
    markupWithCodeExample(
      spacingValues.map(
        (value) => `
<p data-showcss="margin" style="width: 200px;" class="iu-bg-muted iu-color-white iu-m${prefix}-${value}">
  m${prefix}-${value}: <!-- computed value -->
</p>`
      )
    )
  )
  .join("");

const Margin = () => marginMarkup;

const heightMarkup = markupWithCodeExample(
  spacingValues.map(
    (value) => `
iu-height-${value}:
<div data-showcss="height" class="iu-height-${value} iu-bg-muted">
   <!-- computed value -->
</div>
`
  )
);

const Height = () => heightMarkup;

const widthMarkup = markupWithCodeExample(
  spacingValues.map(
    (value) => `
iu-width-${value}:
<div data-showcss="width" class="iu-width-${value} iu-bg-muted">
    :<!-- computed value -->
</div>
`
  )
);

const Width = () => widthMarkup;

storiesOf("utilities/classes", module)
  .addParameters({
    status: "",
  })
  .add("LineHeight", () => {
    useEffect(() => showCSS(), []);
    return LineHeight();
  })
  .add("FontWeight", () => {
    useEffect(() => showCSS(), []);
    return FontWeight();
  })
  .add("FontSize", () => {
    useEffect(() => showCSS(), []);
    return FontSize();
  })
  .add("TextTransform", () => {
    useEffect(() => showCSS(), []);
    return TextTransform();
  })
  .add("BoxShadow", () => {
    useEffect(() => showCSS(), []);
    return BoxShadow();
  })
  .add("BorderRadius", () => {
    useEffect(() => showCSS(), []);
    return BorderRadius();
  })
  .add("FontFamily", () => {
    useEffect(() => showCSS(), []);
    return FontFamily();
  })
  .add("TextColor", () => {
    useEffect(() => showCSS(), []);
    return TextColor();
  })
  .add("BackgroundColor", () => {
    useEffect(() => showCSS(), []);
    return BackgroundColor();
  })
  .add("Border", () => {
    useEffect(() => showCSS(), []);
    return Border();
  })
  .add("BorderWidth", () => {
    useEffect(() => showCSS(), []);
    return BorderWidth();
  })
  .add("Padding", () => {
    useEffect(() => showCSS(), []);
    return Padding();
  })
  .add("Margin", () => {
    useEffect(() => showCSS(), []);
    return Margin();
  })
  .add("Height", () => {
    useEffect(() => showCSS(), []);
    return Height();
  })
  .add("Width", () => {
    useEffect(() => showCSS(), []);
    return Width();
  });
