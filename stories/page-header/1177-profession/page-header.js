export const logo = `
<li class="ic-page-header__item ic-page-header__logo-container-prof">
  <a
    class="ic-page-header__logo-link"
    href="/"
    aria-label="1177 profession, startsida"
  >
    <span class="ic-page-header__logo-link-text"
      >1177 profession, startsida</span
    >
  </a>
</li>
`;
export const namedService = `
<li
  class="ic-page-header__item ic-page-header__service-container"
  role="presentation"
>
  <h1 class="ic-page-header__service-name">Tjänstens namn</h1>
</li>
`;
export const unnamedService = `
<li
  class="ic-page-header__item ic-page-header__noname_service-container"
  role="presentation"
>
  <span class="ic-page-header__noname_service-title"
    >Rubrik för tjänst utan egennamn</span
  >
  <span class="ic-page-header__noname_service-explanation"
    >Förklaring till rubriken</span
  >
</li>
`;
export const loggedOut = `
<li
  class="ic-page-header__item iu-pl-0 iu-pr-0 iu-hide-md iu-hide-sm"
  role="presentation"
>
  <a
    href="javascript:void(0)"
    class="iu-flex iu-align-center iu-flex iu-align-center iu-pr-700"
    aria-label="Logga in"
  >
    <span role="presentation" class="ic-user-image"></span>
    <span class="iu-lh-narrow iu-pl-400 iu-hide-md iu-hide-sm">Logga in</span>
  </a>
</li>
`;
export const loggedIn = `
<li
  class="ic-page-header__item iu-pl-0 iu-pr-0 iu-hide-md iu-hide-sm iu-mr-700"
  role="presentation"
>
  <a class="iu-flex iu-align-center iu-flex iu-align-center">
    <span role="presentation" class="ic-user-image iu-mr-400"></span>
    <div
      class="iu-lh-narrow iu-hide-md iu-hide-sm iu-display-flex iu-flex-column"
      role="presentation"
    >
      <span class="ic-loggedin__text">Du är inloggad</span>
      <a href="javascript:void(0)"
        ><span class="iu-flex iu-align-center">Logga ut</span></a
      >
    </div>
  </a>
</li>
`;
export const searchButton = `
<li role="presentation" class="ic-page-header__item">
  <button
    onClick="void(0)"
    aria-label="Sök efter information"
    class="ic-page-header__button iu-flex iu-align-center"
  >
    <span role="presentation" class="ic-search-image"></span>
    <span class="iu-lh-narrow iu-ml-400 iu-hide-md iu-hide-sm">Sök</span>
  </button>
</li>
`;
export const mobileMenuButton = `
<li class="ic-page-header__item iu-hide-lg iu-hide-xl iu-hide-xxl">
  <button
    onClick="void(0)"
    aria-label="Mobilmeny"
    class="ic-page-header__button iu-flex iu-align-center"
  >
    <span class="ic-mobile-menu-image"></span>
  </button>
</li>
`;
export const buttonNavBasic = `
<nav aria-label="Huvud" class="ic-buttonnav iu-hide-md iu-hide-sm iu-mt-300">
  <ul class="ic-buttonnav__container" role="presentation">
    <li class="ic-buttonnav__item" role="presentation">
      <a
        class="ic-buttonnav__link"
        href="javascript:void(0)"
        aria-expanded="false"
        aria-controls="subnav-0"
      >
        Rubrik
      </a>
      <div class="ic-meganav" id="subnav-0" aria-hidden="true"></div>
    </li>
    <li class="ic-buttonnav__item" role="presentation">
      <a
        class="ic-buttonnav__link"
        href="javascript:void(0)"
        aria-expanded="false"
        aria-controls="subnav-1"
      >
        Rubrik
      </a>
      <div class="ic-meganav" id="subnav-1" aria-hidden="true"></div>
    </li>
    <li class="ic-buttonnav__item" role="presentation">
      <a
        class="ic-buttonnav__link"
        href="javascript:void(0)"
        aria-expanded="false"
        aria-controls="subnav-2"
      >
        Rubrik
      </a>
      <div class="ic-meganav" id="subnav-2" aria-hidden="true"></div>
    </li>
  </ul>
</nav>
`;
