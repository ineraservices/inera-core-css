import { markupWithCodeExample, bleedContainer } from "../../helpers";
import {
  logo,
  namedService,
  unnamedService,
  loggedOut,
  loggedIn,
  searchButton,
  mobileMenuButton,
  buttonNavBasic,
} from "./page-header";

export default {
  title: "components/Page Header/1177-Profession/Desktop",
  decorators: [bleedContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: "1177-profession",
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <nav aria-label="Topp" class="iu-fullwidth iu-height-full">
      <ul class="iu-flex iu-fullwidth iu-height-full">
        ${logo}

        <li role="presentation" class="ic-page-header__line-vertical"></li>

        ${namedService} ${loggedOut} ${searchButton} ${mobileMenuButton}
      </ul>
    </nav>
  </div>

  <div
    role="presentation"
    class="ic-page-header__line-horizontal iu-hide-md iu-hide-sm"
  ></div>

  ${buttonNavBasic}
</header>
`,
  ]);
export const LoggedIn = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <nav aria-label="Topp" class="iu-fullwidth iu-height-full">
      <ul class="iu-flex iu-fullwidth iu-height-full">
        ${logo}

        <li role="presentation" class="ic-page-header__line-vertical"></li>

        ${namedService} ${loggedIn} ${searchButton} ${mobileMenuButton}
      </ul>
    </nav>
  </div>
  <div class="ic-page-header__line-horizontal iu-hide-md iu-hide-sm"></div>

  ${buttonNavBasic}
</header>
`,
  ]);
export const Noname = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <nav aria-label="Topp" class="iu-fullwidth iu-height-full">
      <ul class="iu-flex iu-fullwidth iu-height-full">
        ${logo}

        <li role="presentation" class="ic-page-header__line-vertical"></li>

        ${unnamedService} ${loggedIn} ${searchButton} ${mobileMenuButton}
      </ul>
    </nav>
  </div>

  <div class="ic-page-header__line-horizontal iu-hide-md iu-hide-sm"></div>

  ${buttonNavBasic}
</header>
`,
  ]);
export const Buttons = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <nav aria-label="Topp" class="iu-fullwidth iu-height-full">
      <ul class="iu-flex iu-fullwidth iu-height-full">
        ${logo}

        <li role="presentation" class="ic-page-header__line-vertical"></li>

        ${namedService} ${loggedIn} ${searchButton} ${mobileMenuButton}
      </ul>
    </nav>
  </div>

  <div class="ic-page-header__line-horizontal iu-hide-md iu-hide-sm"></div>

  <nav
    aria-label="Huvudmeny"
    class="ic-buttonnav iu-hide-md iu-hide-sm iu-mt-300"
  >
    <ul class="ic-buttonnav__container">
      <li class="ic-buttonnav__item">
        <a
          class="ic-buttonnav__link ic-buttonnav__link--selected"
          href="javascript:void(0)"
          aria-expanded="false"
          aria-controls="subnav-0"
        >
          Vald sida
        </a>
        <div class="ic-meganav" id="subnav-0" aria-hidden="true"></div>
      </li>
      <li class="ic-buttonnav__item">
        <a
          class="ic-buttonnav__link"
          href="javascript:void(0)"
          aria-expanded="false"
          aria-controls="subnav-1"
        >
          Rubrik
        </a>
        <div class="ic-meganav" id="subnav-1" aria-hidden="true"></div>
      </li>
      <li class="ic-buttonnav__item">
        <a
          class="ic-buttonnav__link ic-buttonnav__link--expanded"
          href="javascript:void(0)"
          aria-expanded="true"
          aria-controls="subnav-2"
        >
          Utfälld meny
        </a>
      </li>
      <li class="ic-buttonnav__item">
        <a
          class="ic-buttonnav__link hover"
          href="javascript:void(0)"
          aria-expanded="false"
          aria-controls="subnav-3"
        >
          Hover
        </a>
        <div class="ic-meganav" id="subnav-3" aria-hidden="true"></div>
      </li>
    </ul>
    <div class="ic-meganav" id="subnav-2" aria-hidden="false">
          <div class="ic-meganav__container">
            <nav class="ic-meganav__menu" aria-label="Mega">
              <ul class="ic-meganav__list">
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)" aria-current="page">
                    Nuvarande sida
                  </a>
                </li>
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 1 </a>
                </li>
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 2 </a>
                </li>
              </ul>
              <ul class="ic-meganav__list">
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 3 </a>
                </li>
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 4 </a>
                </li>
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 5 </a>
                </li>
              </ul>
              <ul class="ic-meganav__list">
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 6 </a>
                </li>
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 7 </a>
                </li>
                <li class="ic-meganav__list__item">
                  <a href="javascript:void(0)"> Vanlig sida 8 </a>
                </li>
              </ul>
            </nav>

            <div class="ic-meganav__information iu-hide-lg iu-hide-xl">
              <h2 class="ic-meganav__information__title">
                En kort text om vad sektionen innehåller
              </h2>
              <p class="ic-meganav__information__text">
                Valfri text om sektionen kan skrivas här. Klicka på länken för
                att se allt innehåll inom området.
              </p>
              <div class="ic-meganav__information__link">
                <a href="javascript: void()">Mer om årets influensa</a>
              </div>
            </div>
          </div>
        </div>
  </nav>
</header>
`,
  ]);
