import { markupWithCodeExample, bleedContainer } from "../../helpers";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import {
  logo,
  namedService,
  unnamedService,
  loggedOut,
  loggedIn,
  searchButton,
  mobileMenuButton,
  buttonNavBasic,
} from "./page-header";

export default {
  title: "components/Page Header/1177-Profession/Mobile",
  decorators: [bleedContainer],
  parameters: {
    status: "",
    viewport: {
      viewports: INITIAL_VIEWPORTS,
      defaultViewport: "iphone12",
    },
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: "1177-profession",
  },
};

const basicHeaderTop = `<header class="ic-page-header iu-desktop-header-padding">
  <div class="ic-page-header__inner iu-mobile-header">
    <nav aria-label="Topp" class="iu-fullwidth iu-height-full">
      <ul class="iu-flex iu-fullwidth iu-height-full">
        ${logo}

        <li role="presentation" class="ic-page-header__line-vertical"></li>

        ${namedService}

        ${loggedIn}

        ${searchButton}

        ${mobileMenuButton}
      </ul>
    </nav>
  </div>

  <div class="ic-page-header__line-horizontal iu-hide-md iu-hide-sm"></div>

  ${buttonNavBasic}
`;

const basicHeaderBottom = `</header>`;

const userLogin = `<nav aria-label="användare inloggning" class="ic-user iu-hide-lg iu-hide-xl iu-hide-xxl">
    <a href="javascript:void(0)" aria-labelledby="ic-user-link-label" class="iu-flex iu-align-center iu-flex iu-align-center">
      <span role="presentation" class="ic-user-image"></span>
      <span id="ic-user-link-label">Logga in</span>
    </a>
  </nav>`;

const userChangePassword = `  <nav aria-label="användare lösenord" class="ic-user iu-hide-lg iu-hide-xl iu-hide-xxl">
    <div role="presentation" class="ic-user-image">
      <span class="ic-user-image-text">Användare</span>
    </div>
    <span class="ic-user-name">Namn-Namet Namnsson</span>
    <a class="ic-user-link" href="javascript:void(0)" aria-labelledby="ic-user-link-label">
      <span id="ic-user-link-label">Logga ut</span>
    </a>
  </nav>`;

const mobileMenuTopsExtended = `  <nav aria-label="Mobil" class="ic-nav-list iu-hide-lg iu-hide-xl iu-hide-xxl" id="mobile-nav-main">
    <ul class="ic-nav-list__list">
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu 1</a>
        <button aria-expanded="false" aria-controls="submenu-20" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
        </button>
        <ul hidden class="ic-nav-list__list" id="submenu-20"></ul>
      </li>
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu 2</a>
        <button aria-expanded="false" aria-controls="submenu-21" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 2</span>
        </button>
        <ul hidden class="ic-nav-list__list" id="submenu-21"></ul>
      </li>
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu 3</a>
        <button aria-expanded="false" aria-controls="submenu-22" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 3</span>
        </button>
        <ul hidden class="ic-nav-list__list" id="submenu-22"></ul>
      </li>
    </ul>
  </nav>`;

export const LoggedOut = () =>
  markupWithCodeExample([
    `
  ${basicHeaderTop}
  ${basicHeaderBottom}
`,
  ]);

export const LoggedOutOpenMenu = () =>
  markupWithCodeExample([
    `
${basicHeaderTop}
${mobileMenuTopsExtended}
${userLogin}
${basicHeaderBottom}
`,
  ]);

export const LoggedInOpenMenu = () =>
  markupWithCodeExample([
    `
${basicHeaderTop}
${mobileMenuTopsExtended}
${userChangePassword}
${basicHeaderBottom}
`,
  ]);

export const LoggedInDeepMenu = () =>
  markupWithCodeExample([
    `
${basicHeaderTop}
  <nav aria-label="Mobil" class="ic-nav-list iu-hide-lg iu-hide-xl iu-hide-xxl" id="mobile-nav">
    <ul class="ic-nav-list__list__pro">
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu item 1</a>
        <button aria-expanded="false" aria-controls="submenu-0" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
        </button>
        <ul hidden class="ic-nav-list__list__pro" id="submenu-0"></ul>
      </li>
      <li class="ic-nav-list__item">
        <a href="javascript:void(0)">Menu item 2</a>
      </li>
      <li class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded">
        <a href="javascript:void(0)">Menu item 3</a>
        <button aria-expanded="true" aria-controls="submenu-1" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
        </button>
        <ul class="ic-nav-list__list__pro" id="submenu-1">
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Submenu item 1</a></li>
          <li class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded">
            <a href="javascript:void(0)">Submenu item 2</a>
            <button aria-expanded="true" aria-controls="submenu-2" class="ic-nav-list__expand__pro__menu">
              <span class="iu-sr-only">Visa innehåll för Submenu item 2</span>
            </button>
            <ul class="ic-nav-list__list__pro" id="submenu-2">
              <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub Submenu item 1</a></li>
              <li class="ic-nav-list__item ic-nav-list__item--selected"><a href="javascript:void(0)" aria-current="page">Sub Submenu item 2</a></li>
              <li class="ic-nav-list__item"><a href="javascript:void(0)" class="next-to-last-navbar">Sub Submenu item 3</a></li>
            </ul>
          </li>
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Submenu item 3</a></li>
        </ul>
      </li>
      <li class="ic-nav-list__item ic-nav-list__item--has-children">
        <a href="javascript:void(0)">Menu item 4</a>
        <button aria-expanded="false" aria-controls="submenu-4" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 4</span>
        </button>
        <ul hidden class="ic-nav-list__list__pro" id="submenu-4">
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub Submenu item 1</a></li>
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub Submenu item 2</a></li>
        </ul>
      </li>
      <li class="ic-nav-list__item ic-nav-list__item--has-children">
        <a href="javascript:void(0)">Menu item 5</a>
        <button aria-expanded="false" aria-controls="submenu-5" class="ic-nav-list__expand__pro__menu">
          <span class="iu-sr-only">Visa innehåll för Menu item 5</span>
        </button>
        <ul hidden class="ic-nav-list__list__pro" id="submenu-5">
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub Submenu item 1</a></li>
          <li class="ic-nav-list__item"><a href="javascript:void(0)">Sub Submenu item 2</a></li>
        </ul>
      </li>
    </ul>
  </nav>
${userChangePassword}
${basicHeaderBottom}
`,
  ]);
