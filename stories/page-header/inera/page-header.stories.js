import { bleedContainer } from "../../helpers";
export {
  DefaultInera,
  // LoggedIn,
  // LoggedInWithMobileMenuInera,
  // WithMenuSide,
  IneraExamples,
  WithMobileMenu,
  // WithMobileTools,
} from "../page-header";

export default {
  title: "components/Page Header/Inera",
  decorators: [bleedContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: "inera",
  },
};
