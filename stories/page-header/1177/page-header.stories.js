import { bleedContainer } from "../../helpers";
export {
  Default,
  LoggedIn,
  LoggedInWithMobileMenu1177,
  WithMenuUnder,
  WithMobileMenu,
  WithMobileTools,
} from '../page-header';

export default {
  title: "components/Page Header/1177",
  decorators: [bleedContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: '1177',
  },
};
