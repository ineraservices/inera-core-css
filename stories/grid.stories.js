import { markupWithCodeExample } from "./helpers";

export default {
  title: "components/Grid",
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "We use [css-grid](https://developer.mozilla.org/en-US/docs/Web/CSS/grid) for the grid container. All child elements will become grid items.",
      },
    },
  },
};

export const Columns = () =>
  markupWithCodeExample([
    `
<div class="iu-grid-cols">
  <div class="demo-box">Automatic</div>
  <div class="demo-box">Grid</div>
  <div class="demo-box">Items</div>
</div>`,
    `
<div class="iu-grid-cols iu-grid-cols-12">
  <div class="demo-box">12 columns</div>
  <div class="iu-grid-span-2 demo-box">2 cols wide</div>
  <div class="iu-grid-span-9 demo-box">9 cols wide</div>
  <div class="iu-grid-span-12 demo-box">12</div>
  <div class="iu-grid-span-6 demo-box">6</div>
  <div class="iu-grid-span-6 demo-box">6</div>
</div>`,
    `
<div class="iu-grid-cols">
  <div class="demo-box">Auto</div>
  <div class="iu-grid-span-2 demo-box">2 cols</div>
  <div class="demo-box">Auto</div>
  <div class="iu-grid-span-2 demo-box">2 cols</div>
  <div class="demo-box">Auto</div>
  <div class="demo-box">Auto</div>
</div>`,
  ]);

export const Rows = () =>
  markupWithCodeExample(`
<div class="iu-grid-rows">
  <div class="demo-box">Automatic</div>
  <div class="demo-box">Grid</div>
  <div class="demo-box">Rows</div>
</div>
`);

export const Responsive = () =>
  markupWithCodeExample([
    `
<div class="iu-grid-cols-3">
  <div class="iu-grid-span-3 iu-grid-span-md-2 iu-grid-span-lg-1 demo-box">Column</div>
  <div class="iu-grid-span-3 iu-grid-span-md-1 iu-grid-span-lg-1 demo-box">Column</div>
  <div class="iu-grid-span-3 iu-grid-span-md-3 iu-grid-span-lg-1 demo-box">Column</div>
</div>
`,
  ]);
