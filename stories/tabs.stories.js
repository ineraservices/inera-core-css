import { storiesOf } from "@storybook/html";
import { useEffect } from "@storybook/client-api";
import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

function initTabs() {
  const tabbed = document.querySelector(".ic-tabbed");
  const tablist = tabbed.querySelector(".ic-tabbed__tabs");
  const tabs = tablist.querySelectorAll(".ic-tabbed__tab");
  const panels = tabbed.querySelectorAll(".ic-tabbed__section");
  const dropdown = tabbed.querySelector(".ic-tabbed__dropdown > select");

  // The tab switching function
  const switchTab = (oldTab, newTab) => {
    newTab.focus();
    // Make the active tab focusable by the user (Tab key)
    newTab.removeAttribute("tabindex");
    // Set the selected state
    newTab.setAttribute("aria-selected", "true");
    oldTab.removeAttribute("aria-selected");
    oldTab.setAttribute("tabindex", "-1");
    // Get the indices of the new and old tabs to find the correct
    // tab panels to show and hide
    let index = Array.prototype.indexOf.call(tabs, newTab);
    let oldIndex = Array.prototype.indexOf.call(tabs, oldTab);
    panels[oldIndex].hidden = true;
    panels[index].hidden = false;
  };

  // Add the tablist role to the first <ul> in the .tabbed container
  tablist.setAttribute("role", "tablist");

  // Add semantics are remove user focusability for each tab
  Array.prototype.forEach.call(tabs, (tab, i) => {
    tab.setAttribute("role", "tab");
    tab.setAttribute("id", "tab" + (i + 1));
    tab.setAttribute("tabindex", "-1");
    tab.parentNode.setAttribute("role", "presentation");

    // Handle clicking of tabs for mouse users
    tab.addEventListener("click", (e) => {
      e.preventDefault();
      let currentTab = tablist.querySelector("[aria-selected]");
      if (e.currentTarget !== currentTab) {
        switchTab(currentTab, e.currentTarget);
      }
    });

    // Handle keydown events for keyboard users
    tab.addEventListener("keydown", (e) => {
      // Get the index of the current tab in the tabs node list
      let index = Array.prototype.indexOf.call(tabs, e.currentTarget);
      // Work out which key the user is pressing and
      // Calculate the new tab's index where appropriate
      let dir =
        e.which === 37
          ? index - 1
          : e.which === 39
          ? index + 1
          : e.which === 40
          ? "down"
          : null;
      if (dir !== null) {
        e.preventDefault();
        // If the down key is pressed, move focus to the open panel,
        // otherwise switch to the adjacent tab
        dir === "down"
          ? panels[i].focus()
          : tabs[dir]
          ? switchTab(e.currentTarget, tabs[dir])
          : void 0;
      }
    });
  });

  // Add tab panel semantics and hide them all
  Array.prototype.forEach.call(panels, (panel, i) => {
    panel.setAttribute("role", "tabpanel");
    panel.setAttribute("tabindex", "-1");
    let id = panel.getAttribute("id");
    panel.setAttribute("aria-labelledby", tabs[i].id);
    panel.hidden = true;
  });

  // Initially activate the first tab and reveal the first tab panel
  tabs[0].removeAttribute("tabindex");
  tabs[0].setAttribute("aria-selected", "true");
  panels[0].hidden = false;
}

const Script = () =>
  markupWithCodeExample(`
<script>
function initTabs() {
  const tabbed = document.querySelector('.ic-tabbed');
  const tablist = tabbed.querySelector('.ic-tabbed__tabs');
  const tabs = tablist.querySelectorAll('.ic-tabbed__tab');
  const panels = tabbed.querySelectorAll('.ic-tabbed__section');
  const dropdown = tabbed.querySelector('.ic-tabbed__dropdown > select');

  // The tab switching function
  const switchTab = (oldTab, newTab) => {
    newTab.focus();
    // Make the active tab focusable by the user (Tab key)
    newTab.removeAttribute('tabindex');
    // Set the selected state
    newTab.setAttribute('aria-selected', 'true');
    oldTab.removeAttribute('aria-selected');
    oldTab.setAttribute('tabindex', '-1');
    // Get the indices of the new and old tabs to find the correct
    // tab panels to show and hide
    let index = Array.prototype.indexOf.call(tabs, newTab);
    let oldIndex = Array.prototype.indexOf.call(tabs, oldTab);
    panels[oldIndex].hidden = true;
    panels[index].hidden = false;
  }

  // Add the tablist role to the first <ul> in the .tabbed container
  tablist.setAttribute('role', 'tablist');

  // Add semantics are remove user focusability for each tab
  Array.prototype.forEach.call(tabs, (tab, i) => {
    tab.setAttribute('role', 'tab');
    tab.setAttribute('id', 'tab' + (i + 1));
    tab.setAttribute('tabindex', '-1');
    tab.parentNode.setAttribute('role', 'presentation');

    // Handle clicking of tabs for mouse users
    tab.addEventListener('click', e => {
      e.preventDefault();
      let currentTab = tablist.querySelector('[aria-selected]');
      if (e.currentTarget !== currentTab) {
        switchTab(currentTab, e.currentTarget);
      }
    });

    // Handle keydown events for keyboard users
    tab.addEventListener('keydown', e => {
      // Get the index of the current tab in the tabs node list
      let index = Array.prototype.indexOf.call(tabs, e.currentTarget);
      // Work out which key the user is pressing and
      // Calculate the new tab's index where appropriate
      let dir = e.which === 37 ? index - 1 : e.which === 39 ? index + 1 : e.which === 40 ? 'down' : null;
      if (dir !== null) {
        e.preventDefault();
        // If the down key is pressed, move focus to the open panel,
        // otherwise switch to the adjacent tab
        dir === 'down' ? panels[i].focus() : tabs[dir] ? switchTab(e.currentTarget, tabs[dir]) : void 0;
      }
    });
  });

  // Add tab panel semantics and hide them all
  Array.prototype.forEach.call(panels, (panel, i) => {
    panel.setAttribute('role', 'tabpanel');
    panel.setAttribute('tabindex', '-1');
    let id = panel.getAttribute('id');
    panel.setAttribute('aria-labelledby', tabs[i].id);
    panel.hidden = true;
  });

  // Initially activate the first tab and reveal the first tab panel
  tabs[0].removeAttribute('tabindex');
  tabs[0].setAttribute('aria-selected', 'true');
  panels[0].hidden = false;
}
</script>
`);

const Default = () =>
  markupWithCodeExample(`

<div class="ic-tabbed tabbed">

  <!-- Device version (no javascript) -->
  <div class="ic-tabbed__dropdown ic-forms__select iu-hide-from-md">
    <select name="select_default">
      <option value="section1">Section 1</option>
      <option value="section2">Section 2</option>
      <option value="section3">Section 3</option>
      <option value="section4">Section 4</option>
    </select>
  </div>

  <!-- Desktop version -->
  <ul class="ic-tabbed__tabs iu-hide-sm">
    <li>
      <a class="ic-tabbed__tab" href="#section1" role="tab" id="tab1" aria-selected="true">Section 1</a>
    </li>
    <li>
      <a class="ic-tabbed__tab" href="#section2">Section 2</a>
    </li>
    <li>
       <a class="ic-tabbed__tab" href="#section3">Section 3</a>
    </li>
     <li>
       <a class="ic-tabbed__tab" href="#section4">Section 4</a>
    </li>
  </ul>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section1">
    <h2>Section 1</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod, tortor nec pharetra ultricies, ante erat imperdiet velit, nec laoreet enim lacus a velit. <a href="javascript:void(0)">Nam luctus</a>, enim in interdum condimentum, nisl diam iaculis lorem, vel volutpat mi leo sit amet lectus. Praesent non odio bibendum magna bibendum accumsan.</p>
  </section>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section2">
    <h2>Section 2</h2>
    <p>Nullam at diam nec arcu suscipit auctor non a erat. Sed et magna semper, eleifend magna non, facilisis nisl. Proin et est et lorem dictum finibus ut nec turpis. Aenean nisi tortor, euismod a mauris a, mattis scelerisque tortor. Sed dolor risus, varius a nibh id, condimentum lacinia est. In lacinia cursus odio a aliquam. Curabitur tortor magna, laoreet ut rhoncus at, sodales consequat tellus.</p>
  </section>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section3">
    <h2>Section 3</h2>
    <p>Phasellus ac tristique orci. Nulla maximus <a href="">justo nec dignissim consequat</a>. Sed vehicula diam sit amet mi efficitur vehicula in in nisl. Aliquam erat volutpat. Suspendisse lorem turpis, accumsan consequat consectetur gravida, <a href="javascript:void(0)">pellentesque ac ante</a>. Aliquam in commodo ligula, sit amet mollis neque. Vestibulum at facilisis massa.</p>
  </section>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section4">
    <h2>Section 4</h2>
    <p>Nam luctus, enim in interdum condimentum, nisl diam iaculis lorem, vel volutpat mi leo sit amet lectus. Praesent non odio bibendum magna bibendum accumsan. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod, tortor nec pharetra ultricies, ante erat imperdiet velit, nec laoreet enim lacus a velit. </p>
  </section>
</div>
`);

const tabsWithIcons = () =>
  markupWithCodeExample(`

<div class="ic-tabbed tabbed">

  <!-- Device version (no javascript) -->
  <div class="ic-tabbed__dropdown ic-forms__select iu-hide-from-md">
    <select name="select_default">
      <option value="section1">Section 1</option>
      <option value="section2">Section 2</option>
      <option value="section3">Section 3</option>
      <option value="section4">Section 4</option>
    </select>
  </div>

  <!-- Desktop version -->
  <ul class="ic-tabbed__tabs iu-hide-sm">
    <li> 
      <!-- Include ic-tab-icon in class -->      
      <a class="ic-tabbed__tab ic-tab-icon" href="#section1" role="tab" id="tab1" aria-selected="true">       
        <svg width="20" height="20" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
          <path fill-rule="evenodd" clip-rule="evenodd" d="m2.224 16.75 15.85-15.538a2.75 2.75 0 0 1 3.851 0L37.776 16.75a1.75 1.75 0 0 1-1.225 3H34.75v17.94c0 1.108-.928 1.981-2.067 2.055l-.157.005h-8.855V28.138c0-.272-.273-.533-.638-.557l-.086-.003h-5.894c-.418 0-.724.271-.724.56V39.75H7.474c-1.21 0-2.224-.901-2.224-2.06V19.75H3.449a1.75 1.75 0 0 1-1.745-1.607L1.7 18c0-.47.189-.92.525-1.25Zm34.502 1.072L20.875 2.283a1.25 1.25 0 0 0-1.75 0L3.274 17.821a.25.25 0 0 0 .175.429H6.75v19.44c0 .288.306.56.724.56h7.354l.001-10.112c0-1.109.928-1.982 2.067-2.055l.157-.005h5.919l.134.005c1.144.075 2.065.953 2.065 2.055V38.25h7.355c.38 0 .667-.225.717-.482l.007-.078V18.25h3.301a.25.25 0 0 0 .175-.428Z" fill="var(--icon-color)"/>
          <path d="M18.908 5.636c.575-.48 1.442-.512 2.084-.072l.133.102 10.39 9.788a.75.75 0 0 1-.943 1.162l-.086-.07L20.13 6.787c-.047-.04-.148-.047-.199-.039l-.028.01-10.389 9.788a.75.75 0 0 1-1.103-1.01l.075-.082 10.422-9.818Z" fill="var(--icon-color2)"/>
        </svg>
      Section 1</a>       
    </li>
    <li>
      <!-- Include ic-tab-icon in class -->
      <a class="ic-tabbed__tab ic-tab-icon" href="#section2">
        <svg width="20" height="20" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
          <path d="m38.432 5.93.96 1.153L26.227 18.03l12.73 13.924-1.107 1.012L25.072 18.99l-3.358 2.794a2.75 2.75 0 0 1-3.36.12l-.156-.12-3.304-2.748L2.16 32.965l-1.107-1.012 12.686-13.878L.52 7.083l.96-1.153 17.677 14.7a1.25 1.25 0 0 0 1.488.081l.11-.082 17.677-14.7Z" fill="var(--icon-color2)"/>
          <path fill-rule="evenodd" clip-rule="evenodd" d="M3.049 5.25h33.912A2.75 2.75 0 0 1 39.711 8v23.022a2.75 2.75 0 0 1-2.75 2.75H3.049a2.75 2.75 0 0 1-2.75-2.75V8a2.75 2.75 0 0 1 2.75-2.75Zm33.912 1.5H3.049c-.69 0-1.25.56-1.25 1.25v23.022c0 .69.56 1.25 1.25 1.25h33.912c.69 0 1.25-.56 1.25-1.25V8c0-.69-.56-1.25-1.25-1.25Z" fill="var(--icon-color)"/>
        </svg>
      Section 2</a>
    </li>
    <li>
      <!-- Include ic-tab-icon in class -->
      <a class="ic-tabbed__tab ic-tab-icon" href="#section3">
        <svg width="20" height="20" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M9.562 10.769c0-5.298 4.528-9.824 9.783-9.824 6.058 0 10.816 4.304 10.816 9.824 0 6.136-5.056 11.898-10.816 11.898-4.995 0-9.783-6.032-9.783-11.898Zm19.099 0c0-4.656-4.055-8.324-9.316-8.324-4.427 0-8.283 3.855-8.283 8.324 0 5.104 4.202 10.398 8.283 10.398 4.885 0 9.316-5.05 9.316-10.398Z" fill="var(--icon-color)"/>
          <path d="M19.397 8.81a.75.75 0 0 1 .743.647l.007.102-.001 4.867h1.474a.75.75 0 0 1 .743.649l.007.102a.75.75 0 0 1-.648.743l-.102.007h-2.223a.75.75 0 0 1-.743-.649l-.007-.101-.001-4.868h-3.698a.75.75 0 0 1-.743-.648l-.007-.102a.75.75 0 0 1 .648-.743l.102-.007h4.449Z" fill="var(--icon-color2)"/>
          <path fill-rule="evenodd" clip-rule="evenodd" d="M8.692 28.037c2.488-2.4 6.232-3.5 11.731-3.5 5.011 0 8.45 1.082 10.76 3.44 1.068 1.09 1.776 2.18 2.483 4.004l.202.542.204.586.208.633.213.685.221.74.44 1.514.204.68.191.605.188.557a.75.75 0 0 1-.709.995H4.695a.75.75 0 0 1-.708-.996l.133-.394.138-.426.312-1.02.384-1.306.223-.74.216-.685.21-.633.206-.586.204-.542c.749-1.91 1.5-3.014 2.68-4.153Zm21.42.989c-1.986-2.027-5.045-2.988-9.689-2.988-5.147 0-8.527.991-10.689 3.079-.771.744-1.332 1.47-1.851 2.531l-.193.415c-.096.215-.191.444-.287.69l-.192.51-.098.272-.197.582c-.034.1-.067.204-.101.31l-.208.66-.216.72-.382 1.3-.278.911h28.26l-.117-.381-.16-.535-.327-1.126-.326-1.104-.206-.662-.199-.608-.194-.558-.19-.512c-.634-1.635-1.247-2.574-2.16-3.506Z" fill="var(--icon-color)"/>
          <path d="M27.735 33.576a.75.75 0 0 1 .871.462l.03.098.883 3.79a.75.75 0 0 1-1.431.438l-.03-.098-.883-3.79a.75.75 0 0 1 .56-.9Zm-16.648.56a.75.75 0 0 1 1.477.24l-.017.1-.883 3.79a.75.75 0 0 1-1.477-.24l.016-.1.884-3.79Z" fill="var(--icon-color)"/>
        </svg>
      Section 3</a>
    </li>
     <li>
      <!-- Include ic-tab-icon in class -->
      <a class="ic-tabbed__tab ic-tab-icon" href="#section4">
        <svg width="20" height="20" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
          <path d="M1 .25a.75.75 0 0 1 .743.648L1.75 1v37.25H39a.75.75 0 0 1 .743.648l.007.102a.75.75 0 0 1-.648.743L39 39.75H.25V1A.75.75 0 0 1 1 .25Z" fill="var(--icon-color)"/>
          <path d="M32.94 11.246a1.25 1.25 0 0 1 2.07 1.395l-.075.113-8.886 11.75a1.25 1.25 0 0 1-1.703.277l-.105-.08-7.204-6.148-9.11 11.834a1.25 1.25 0 0 1-1.64.306l-.112-.078a1.25 1.25 0 0 1-.305-1.64l.077-.113 9.912-12.875a1.25 1.25 0 0 1 1.69-.274l.112.085 7.194 6.14 8.085-10.692Z" fill="var(--icon-color2)"/>
        </svg>
      Section 4</a>
    </li>
  </ul>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section1">
    <h2>Section 1</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod, tortor nec pharetra ultricies, ante erat imperdiet velit, nec laoreet enim lacus a velit. <a href="javascript:void(0)">Nam luctus</a>, enim in interdum condimentum, nisl diam iaculis lorem, vel volutpat mi leo sit amet lectus. Praesent non odio bibendum magna bibendum accumsan.</p>
  </section>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section2">
    <h2>Section 2</h2>
    <p>Nullam at diam nec arcu suscipit auctor non a erat. Sed et magna semper, eleifend magna non, facilisis nisl. Proin et est et lorem dictum finibus ut nec turpis. Aenean nisi tortor, euismod a mauris a, mattis scelerisque tortor. Sed dolor risus, varius a nibh id, condimentum lacinia est. In lacinia cursus odio a aliquam. Curabitur tortor magna, laoreet ut rhoncus at, sodales consequat tellus.</p>
  </section>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section3">
    <h2>Section 3</h2>
    <p>Phasellus ac tristique orci. Nulla maximus <a href="">justo nec dignissim consequat</a>. Sed vehicula diam sit amet mi efficitur vehicula in in nisl. Aliquam erat volutpat. Suspendisse lorem turpis, accumsan consequat consectetur gravida, <a href="javascript:void(0)">pellentesque ac ante</a>. Aliquam in commodo ligula, sit amet mollis neque. Vestibulum at facilisis massa.</p>
  </section>

  <section class="ic-tabbed__section ic-text iu-py-900" id="section4">
    <h2>Section 4</h2>
    <p>Nam luctus, enim in interdum condimentum, nisl diam iaculis lorem, vel volutpat mi leo sit amet lectus. Praesent non odio bibendum magna bibendum accumsan. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod, tortor nec pharetra ultricies, ante erat imperdiet velit, nec laoreet enim lacus a velit. </p>
  </section>
</div>
`);

storiesOf("components/Tabs", module)
  .addDecorator(wrapStoryInContainer)
  .addParameters({
    docs: {
      description: {
        component: [
          "* Inspired from: https://inclusive-components.design/tabbed-interfaces/",
          "* Build with progressive enhancement in mind. So if you implement your own script make sure you set all the attributes needed.",
          "* Section padding is for demo. Use appropriate for your usecase.",
        ].join("\r\n"),
      },
    },
    status: "",
  })

  .add("Default", () => {
    useEffect(() => {
      initTabs();
    }, []);
    return Default();
  })

  .add("Tabs With Icons", () => {
    useEffect(() => {
      initTabs();
    }, []);
    return tabsWithIcons();
  })

  .add("Script", () => {
    return Script();
  });
