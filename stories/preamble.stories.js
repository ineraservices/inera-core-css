import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Preamble",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
  },
};

export const Default = () =>
  markupWithCodeExample([
    `
<p class="ic-preamble">
  Lorem ipsum dolor sit amet, consec tetur adipisc ing elit,
  sed do eiusmod tempor incidi dunt ut labore et aliqua. Lorem
  ipsum dolor sit amet, consec tetur adipisc ing elit,
  sed do eiusmod tempor incidi dunt ut labore et aliqua.
</p>`,
  ]);

export const InsideText = () =>
  markupWithCodeExample([
    `
<div class="ic-text">
  <p class="ic-preamble">
    Lorem ipsum dolor sit amet, consec tetur adipisc ing elit,
    sed do eiusmod tempor incidi dunt ut labore et aliqua. Lorem
    ipsum dolor sit amet, consec tetur adipisc ing elit,
    sed do eiusmod tempor incidi dunt ut labore et aliqua.
  </p>
</div>
`,
  ]);
