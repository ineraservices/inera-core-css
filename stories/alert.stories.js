import { storiesOf } from "@storybook/html";
import { useEffect } from "@storybook/client-api";

import {
  markupWithCodeExample,
  wrapStoryInContainer,
  bleedContainer,
} from "./helpers";

// Script start

function getExpandButton(item) {
  return item.querySelector(".ic-global-alert__expand");
}

function expandOrContract(item, expand) {
  let btn = getExpandButton(item);
  let content = item.querySelector(".ic-global-alert__body");

  if (btn.dataset.showLabel && btn.dataset.hideLabel) {
    const text = expand ? btn.dataset.hideLabel : btn.dataset.showLabel;
    btn.setAttribute("aria-label", text);
    btn.querySelector("span").innerText = text;
  }
  btn.setAttribute("aria-expanded", expand);
  item.classList.toggle("ic-global-alert--expanded", expand);
  content.classList.toggle("ic-global-alert__body--hidden", !expand);
}

function toggleExpanded(item) {
  return function inner() {
    const isExpanded = item.classList.contains("ic-global-alert--expanded");
    expandOrContract(item, !isExpanded);
  };
}

function expandableSection() {
  let items = document.querySelectorAll(".ic-global-alert--expandable");
  for (let item of items) {
    let btn = getExpandButton(item);
    btn.onclick = toggleExpanded(item);
  }
}

// Script end

const Default = () =>
  markupWithCodeExample([
    `
<div class="ic-alert ic-alert--status ic-alert--info">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-info-icon"></i>
    Information
  </h3>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
</div>
`,
    `
<div class="ic-alert ic-alert--status ic-alert--error">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-error-icon"></i>
    Error
  </h3>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
</div>
`,
    `
<div class="ic-alert ic-alert--status ic-alert--success">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-success-icon"></i>
    Success
  </h3>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
</div>
`,
  ]);

const ContentBody = () =>
  markupWithCodeExample([
    `
<div class="ic-alert ic-alert--status ic-alert--info">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-info-icon"></i>
    Information
  </h3>
  <div class="ic-alert__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
    <ul role="list">
      <li>List information</li>
      <li>List information</li>
      <li>List information</li>
    </ul>
    <p><a href="javascript:void(0)">And a link</a></p>
  </div>
</div>
`,
    `
<div class="ic-alert ic-alert--status ic-alert--error">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-error-icon"></i>
    Error
  </h3>
  <div class="ic-alert__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
    <ul role="list">
      <li>List information</li>
      <li>List information</li>
      <li>List information</li>
    </ul>
    <p><a href="javascript:void(0)">And a link</a></p>
  </div>
</div>
`,
    `
<div class="ic-alert ic-alert--status ic-alert--success">
  <h3 class="ic-alert__headline">
    <i class="ic-alert__icon ic-success-icon"></i>
    Success
  </h3>
  <div class="ic-alert__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit. </p>
    <ul role="list">
      <li>List information</li>
      <li>List information</li>
      <li>List information</li>
    </ul>
    <p><a href="javascript:void(0)">And a link</a></p>
  </div>
</div>
`,
  ]);

const LocalMessage = () =>
  markupWithCodeExample([
    `
<div class="ic-alert ic-alert--status ic-alert--local">
  <i class="ic-alert__icon ic-observe-icon"></i>
  <div class="ic-alert__body">
    <p>Short message!</p>
  </div>
</div>
`,
    `
<div class="ic-alert ic-alert--status ic-alert--local">
  <i class="ic-alert__icon ic-observe-icon"></i>
  <div class="ic-alert__body">
    <p>Warning! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt. Praesent eu posuere elit.</p>
  </div>
</div>
`,
    `
<div class="ic-alert ic-alert--local">
  <i class="ic-alert__icon ic-observe-icon"></i>
  <div class="ic-alert__body">
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse euismod diam nec ligula tincidunt tincidunt.
      Praesent eu posuere elit. Maecenas vel dapibus elit, sit amet vehicula turpis. Nullam vel neque eu ligula elementum.
    </p>
    <ul role="list">
      <li>This is a list</li>
      <li>This is a list</li>
      <li>This is a list</li>
    </ul>
    <p>Sodales eget quis lorem. Sed tempus porttitor dui nec tempus. Cras nec volutpat lorem, et faucibus felis. </p>
  </div>
</div>
`,
  ]);

const GlobalAlert = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
</header>
<div class="ic-global-alert">
  <div class="ic-global-alert__inner">
    <i class="ic-global-alert__icon ic-observe-icon"></i>
    <div>
      Maecenas sed diam eget risus varius blandit sit amet non magna.
      Fusce dapibus, tellus ac cursus commodo, tortor mauris. Lorem ipsum dolor sit amet.<br />
      /Author, 2019-05-16
    </div>
  </div>
</div>
`,
  ]);

const GlobalAlertExpandable = () =>
  markupWithCodeExample([
    `
<header class="ic-page-header">
  <div class="ic-page-header__inner">
    <div class="ic-page-header__item ic-page-header__logo-container">
      <a class="ic-page-header__logo-link" href="#/">
        <span class="ic-page-header__logo-link-text"> [1177/Inera] </span>
      </a>
    </div>
  </div>
</header>
<div class="ic-global-alert ic-global-alert--expandable ic-global-alert--expanded">
  <div class="ic-global-alert__inner">
    <i class="ic-global-alert__icon ic-observe-icon"></i>
    <h2 class="ic-global-alert__headline">Headline</h2>
    <button data-show-label="Visa meddelande" data-hide-label="Dölj meddelande" aria-label="Dölj meddelande" class="ic-global-alert__expand" type="button" aria-expanded="true">
      <span>Dölj meddelande</span>
    </button>
    <div class="ic-global-alert__body">
      Maecenas sed diam eget risus varius blandit sit amet non magna.
      Fusce dapibus, tellus ac cursus commodo, tortor mauris. Lorem ipsum dolor sit amet.<br />
      /Author, 2019-05-16
    </div>
  </div>
</div>
<div class="ic-global-alert ic-global-alert--expandable">
  <div class="ic-global-alert__inner">
    <i class="ic-global-alert__icon ic-observe-icon"></i>
    <h2 class="ic-global-alert__headline">Headline</h2>
    <button data-show-label="Visa meddelande" data-hide-label="Dölj meddelande" aria-label="Visa meddelande" class="ic-global-alert__expand" type="button" aria-expanded="false">
      <span>Visa meddelande</span>
    </button>
    <div class="ic-global-alert__body ic-global-alert__body--hidden">
      Maecenas sed diam eget risus varius blandit sit amet non magna.
      Fusce dapibus, tellus ac cursus commodo, tortor mauris. Lorem ipsum dolor sit amet.<br />
      /Author, 2019-05-16
    </div>
  </div>
</div>
`,
  ]);

storiesOf("components/Status Message", module)
  .addParameters({
    docs: {
      description: {
        component: [""].join("\r\n"),
      },
    },
    status: "",
  })
  .add(
    "Default",
    () => {
      return Default();
    },
    { decorators: [wrapStoryInContainer] }
  )
  .add("ContentBody", () => {
    return ContentBody();
  })
  .add(
    "LocalMessage",
    () => {
      return LocalMessage();
    },
    { decorators: [wrapStoryInContainer] }
  )
  .add(
    "GlobalAlert",
    () => {
      return GlobalAlert();
    },
    { decorators: [bleedContainer] }
  )
  .add(
    "GlobalAlertExpandable",
    () => {
      useEffect(() => {
        expandableSection();
      }, []);
      return GlobalAlertExpandable();
    },
    { decorators: [bleedContainer] }
  );
