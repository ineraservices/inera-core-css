export default `<div style="max-width: 88ch" class="ic-text">
<br />
  <h1>Gemensam CSS, 1177 / Inera</h1>

  <p class="ic-preamble">Innan Inera Design System, IDS (<a href="https://inera.design/?path=/story/introduction--page">inera.design.se</a>) fanns, som är vårt huvudsakliga sätt att dela kod mellan olika tjänster och projekt så skapades också Gemensam CSS. I all nyutveckling framöver används IDS och vår CSS kommer ha end of life 2025. </p>
  <p class="ic-preamble"> För de projekt som inte kan använda IDS-komponenterna i sin helhet finns möjlighet att använda CSS genom ids-design. <u>Observera</u> att ett arbete fortfarande behöver göras för att uppdatera till detta sätt att använda CSS, då namngivning ändrats.
</p>


  <h2>Om Gemensam CSS</h2>

  <p>De flesta komponenter finns för både varumärket 1177 och Inera och du väljer vilket som visas genom att välja i verktygsfältet längst upp eller via URL:en.</p>
  <p>Nyare komponenter släpps bara till <a href="https://inera.design/?path=/story/introduction--page">IDS</a>.</p>

  <p>
  Källkoden för inera-core-css finns på
    <a href="https://bitbucket.org/ineraservices/inera-core-css/"
      >bitbucket</a
    >.
  </p>
  <h2>Avveckling av Gemensam CSS</h2>
  <p>
  Varför avvecklas Gemensam CSS?
  </p>
  <ul>
  <li>Bristande återanvändbarhet: Gemensam CSS saknar effektiv kodåteranvändning och versionshantering, vilket leder till ökad tidsåtgång och kostnader.</li>
  <li>Tillgänglighetsbrister: CSS:en bidrar inte till att säkerställa efterlevnad till tillgänglighetslagstiftningen, vilket kan påverka användarupplevelsen negativt.</li>
  <li>Teknisk och ekonomisk belastning: Förvaltning av två separata bibliotek (CSS och IDS) är resurskrävande och ineffektivt.</li>
</ul>
<p>Vi rättar buggar och problem löpande under året men avvecklar gradvis den aktiva förvaltningen och utveckling av funktionalitet.</p>
  <h2>Releaseplan 2024</h2>
  <p> Inga mer planerade releaser för 2024. <p>
</ul>
  <h2>Buggar och problem</h2>
  <p>Rapportera gärna buggar och problem i Slackkanalen IneraUX / Komponenter_kod eller via <a href="https://inera1177.typeform.com/to/Uk1F94">formuläret. Återkoppling på UX-ramverk.</a></p>
</div>
`;
