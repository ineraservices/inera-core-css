import { wrapStoryInContainer } from "./helpers";

import home from "./html/about-home";
import install from "./html/about-install";
import develop from "./html/about-develop";

export default {
  title: "About",
  decorators: [wrapStoryInContainer],
};

export const Home = () => home;

export const Installation = () => install;

export const Develop = () => develop;
