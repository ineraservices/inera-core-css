import { markupWithCodeExample } from "./helpers";

export default {
  title: "utilities/responsive",
  parameters: {
    status: "",
  },
};

const visibilityMarkup = ["sm", "md", "lg", "xl", "xxl"]
  .map((breakpoint) =>
    markupWithCodeExample([
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-hide-${breakpoint}">display: none on ${breakpoint} screens</p></div>`,
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-hide-from-${breakpoint}">display: none on ${breakpoint} screens and larger</p></div>`,
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-show-${breakpoint}">display: block on ${breakpoint} screens</p></p></div>`,
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-show-${breakpoint}-inline">display: inline on ${breakpoint} screens</p></p></div>`,
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-show-${breakpoint}-inline-block">display: inline-block on ${breakpoint} screens</p></p></div>`,
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-show-${breakpoint}-grid">display: grid on ${breakpoint} screens</p></p></div>`,
      `<div class="iu-bg-muted"><p class="iu-bg-main iu-show-${breakpoint}-flex">display: flex on ${breakpoint} screens</p></p></div>`,
    ])
  )
  .join("");

export const Visibility = () => visibilityMarkup;
