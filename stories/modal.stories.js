import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "components/Modal",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* Observe that this demo is css only. You would need javascript to toggle visibility and add classes/styles to body element.",
          '* Remember to use `role="alertdialog"` for the Error modal and `role="dialog"` for the others.',
          "* Remember to add a close button with screen readable text.",
          "* Make sure the the rest of the page is [inert](https://github.com/WICG/inert).",
          "* Make sure tabfocus is trapped.",
          "* Keep the content brief. You should not need inline scrolling!",
          "* Pressing `esc` button should close the modal.",
          "* When modal is opened focus should be set on the modal. When the modal is closed - focus should be moved back.",
          "* Primary button is placed last and secondary first. CSS is reversing order for small devices.",
        ].join("\r\n"),
      },
    },
  },
};

export const Dialog = () =>
  markupWithCodeExample(`
<div role="dialog" class="ic-modal" aria-labelledby="demo-modal-content">
  <button type="button" aria-label="Close modal" class="ic-modal__close iu-svg-icon">
    <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path fill="currentColor" d="M12 10.733l5.07-5.07c.35-.35.917-.35 1.267 0 .35.35.35.917 0 1.267L13.267 12l5.07 5.07c.35.35.35.917 0 1.267-.35.35-.917.35-1.267 0L12 13.267l-5.07 5.07c-.35.35-.917.35-1.267 0-.35-.35-.35-.917 0-1.267l5.07-5.07-5.07-5.07c-.35-.35-.35-.917 0-1.267.35-.35.917-.35 1.267 0l5.07 5.07z" transform="translate(-994 -650) translate(410 637) translate(584 13)"/>
    </svg>
  </button>
  <div class="ic-modal__head" id="demo-modal-content">
    <h3>Headline</h3>
  </div>
  <div class="ic-modal__body ic-text">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu smod tempor incididunt ut labore et dolore magna aliqua.</p>
    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>
  </div>
</div>`);

export const Info = () =>
  markupWithCodeExample(`
<div role="dialog" class="ic-modal" aria-labelledby="demo-modal-content">
  <div class="ic-modal__head" id="demo-modal-content">
    <h3>Headline</h3>
  </div>
  <div class="ic-modal__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu smod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>
  </div>
  <div class="ic-button-group">
    <button class="ic-button ic-button--secondary" type="button">Back</button>
  </div>
</div>
`);

export const Confirm = () =>
  markupWithCodeExample(`
<div role="dialog" class="ic-modal" aria-labelledby="demo-modal-content">
  <div class="ic-modal__head" id="demo-modal-content">
    <h3>Headline</h3>
  </div>
  <div class="ic-modal__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu smod tempor incididunt ut labore et dolore magna aliqua. </p>
  </div>
  <div class="ic-button-group ic-button-group--right">
  <button class="ic-button" type="button">Submit</button>
  <button class="ic-button ic-button--secondary" type="button">Cancel</button>
  </div>
</div>
`);

export const Error = () =>
  markupWithCodeExample(`
<div role="alertdialog" class="ic-modal ic-modal--error" aria-labelledby="demo-modal-content">
  <div class="ic-modal__head" id="demo-modal-content">
    <h3 class="ic-modal__headline">Headline</h3>
  </div>
  <div class="ic-modal__body">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu smod tempor incididunt ut labore et dolore magna aliqua.</p>
  </div>
  <div class="ic-button-group ic-button-group--center">
    <button class="ic-button ic-button--secondary" type="button">Cancel</button>
    <button class="ic-button" type="button">Submit</button>
  </div>
</div>
`);

export const Backdrop = () =>
  markupWithCodeExample(`
<div class="ic-backdrop">
  <div role="dialog" aria-modal="true" class="ic-modal">
    Example with backdrop.
  </div>
</div>`);
