import { markupWithCodeExample } from "../helpers";

export default {
  title: "base/Colors/1177-Profession",
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: "1177-profession",
  },
};

const colors = [
  "main",
  "secondary-dark",
  "secondary-light",
  "black",
  "white",
  "text",
  "background",
  "cta",
  "cta-dark",
  "cta-light",
  "cta-text",
  "nav",
  "nav-dark",
  "nav-light",
  "nav-text",
  "muted",
  "grey-100",
  "grey-200",
  "grey-300",
  "grey-400",
  "grey-500",
  "grey-600",
  "grey-700",
  "grey-800",
  "grey-900",
  "highlight",
  "error",
  "error-light",
  "success",
  "success-light",
  "information",
  "information-light",
  "observe",
  "observe-light",
  "sky-base",
  "sky-dark",
  "sky-clear",
  "sky-line",
  "sky-background",
  "grass-base",
  "grass-dark",
  "grass-clear",
  "grass-line",
  "grass-background",
  "plum-base",
  "plum-dark",
  "plum-clear",
  "plum-line",
  "plum-background",
  "sun-base",
  "sun-dark",
  "sun-clear",
  "sun-line",
  "sun-background",
  "stone-base",
  "stone-dark",
  "stone-clear",
  "stone-line",
  "stone-background",
];

export const Samples = () =>
  colors.reduce(function colorSample(html, color) {
    html += `
<div class="iu-flex iu-align-center">
  <div
    style="
      font-size: 10px;
      box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);
      text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.25);
      margin-bottom: 10px;
      margin-right: 10px;
      padding: 5px;
      width: 100px;
      height: 100px;
    "
    class="iu-bg-${color} iu-radius-round iu-flex iu-align-center iu-text-center iu-color-white">
  </div>
  <p>${color}</p>
</div>`;
    return html;
  }, "");

export const WithMarkup = () =>
  markupWithCodeExample(
    colors.map(
      (color) =>
        `<p class="iu-color-${color}">Text color: ${color}</p>
<p class="iu-bg-${color} iu-color-white" style="text-shadow: 1px 1px 0 black">Background color: ${color}</p>`
    )
  );
