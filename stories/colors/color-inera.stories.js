import { markupWithCodeExample } from "../helpers";

export default {
  title: "base/Colors/Inera",
  parameters: {
    status: "",
    docs: {
      description: {
        component:
          "More UX information on [Confluence](https://inera.atlassian.net/wiki/spaces/USI/pages/212674241/Ramverk+och+navigation+-+1177).",
      },
    },
    theme: "inera",
  },
};

const ineraColors = [
  "primary30",
  "primary40",
  "primary50",
  "secondary40",
  "secondary90",
  "secondary95",
  "accent30",
  "accent40",
  "accent90",
  "graphic-key",
  "graphic-key-alt",
  "neutral20",
  "neutral40",
  "neutral90",
  "neutral99",
  "background",
  "success40",
  "success99",
  "attention40",
  "attention95",
  "error40",
  "error99",
];

export const Samples = () =>
  ineraColors.reduce(function colorSample(html, color) {
    html += `
<div class="iu-flex iu-align-center">
  <div
    style="
      font-size: 10px;
      box-shadow: 0 3px 5px rgba(0, 0, 0, 0.3);
      text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.25);
      margin-bottom: 10px;
      margin-right: 10px;
      padding: 5px;
      width: 100px;
      height: 100px;
    "
    class="iu-bg-${color} iu-radius-round iu-flex iu-align-center iu-text-center iu-color-white">
  </div>
  <p>${color}</p>
</div>`;
    return html;
  }, "");

export const WithMarkup = () =>
  markupWithCodeExample(
    ineraColors.map(
      (color) =>
        `<p class="iu-color-${color}">Text color: ${color}</p>
<p class="iu-bg-${color} iu-color-white" style="text-shadow: 1px 1px 0 black">Background color: ${color}</p>`
    )
  );
