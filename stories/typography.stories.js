import { markupWithCodeExample, wrapStoryInContainer } from "./helpers";

export default {
  title: "base/Typography",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* definition list: responsive helpers (--md, --lg) extends `.ic-definition-list`",
        ].join("\r\n"),
      },
    },
  },
};

const markup = `
<h1>Heading level 1</h1>
<h1 class="h1--small">Heading small level 1</h1>
<h2>Heading level 2</h2>
<h2 class="iu-color-heading">Heading level 2 (default heading color)</h2>
<h2><a class="iu-color-anchor" href="#">Heading level 2 (anchor)</a></h2>
<h3>Heading level 3</h3>
<h3><a class="iu-color-anchor" href="#">Heading level 3 (anchor)</a></h3>
<h4>Heading level 4</h4>
<h4><a class="iu-color-anchor" href="#">Heading level 4 (anchor)</a></h4>
<h5 class="ic-subtitle">Heading 5</h5>
<h6 class="ic-subtitle-small">Heading 6</h6>
<p class="ic-preamble">A preamble paragraph</p>
<p>Paragraph</p>
<p><strong>Strong/Bold</strong></p>
<p><em>Emphasis/Italic</em></p>
<p><a href="javascript:void(0)">Link</a></p>
<p><a href="https://www.1177.se/">En länk till 1177.se</a></p>
<blockquote>Blockquote</blockquote>
<h3>Unordered list</h3>
<ul role="list">
  <li>Unordered list item
    <ul role="list">
      <li>Sublist</li>
      <li>Sublist</li>
    </ul>
  </li>
  <li>Unordered list item</li>
</ul>
<h4>This is a more discrete list used in for example alert boxes</h4>
<ul class="ic-discrete-list">
  <li>Unordered list item
    <ul role="list">
      <li>Sublist</li>
      <li>Sublist</li>
    </ul>
  </li>
  <li>Unordered list item</li>
</ul>
<h3>Ordered list</h3>
<ol role="list">
  <li>Ordered list item</li>
  <li>Ordered list item</li>
  <li>Ordered list item</li>
  <li>Ordered list item</li>
</ol>
<h3>Definition list</h3>
<dl>
  <dt>Definition term</dt>
  <dd>Definition description</dd>
  <dt>Definition term</dt>
  <dd>Definition description</dd>
</dl>
<dl class="ic-definition-list">
  <dt>Definition term</dt>
  <dd>Definition description</dd>
  <dt>Definition term</dt>
  <dd>Definition description</dd>
</dl>
<h3>Tables</h3>
<table>
  <thead>
    <tr>
      <th>Table heading</th>
      <th>Table heading</th>
      <th>Table heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Table cell</td>
      <td>Table cell</td>
      <td>Table cell</td>
    </tr>
  </tbody>
</table>
<h3>Text versions</h3>
<p><small>Small text with &lt;small&gt; element</small></p>
<p class="ic-small-text">Small text with class on &lt;p&gt; element</p>
<p class="ic-light-text">Text with lighter color</p>
<p class="page-listing">Sidlistning</p>
<p><a class="local-nav" href="">Lokal navigering</a></p>
<p><a class="local-nav--selected" href="">Lokal navigering, vald</a></p>
<p><a class="local-nav--hover" href="">Lokal navigering, hover (&:hover)</a></p>
<address>Address</address>
`;

export const README = () => `
<div class="ic-text">
  <h2>Om typsnitten</h2>
  <p>Du behöver länka in typsnitten som används på sidan manuellt.</p>
  <p>För <strong>1177</strong> används Inter UI och Open Sans.</p>
  <P>För <strong>Inera</strong> används Poppins och Open Sans.</p>
  <p>Typsnitten finns under
    <a href="https://bitbucket.org/ineraservices/inera-core-css/src/develop/static/fonts/">./static/fonts</a>
  </p>
  
  <h3>Exempel 1177</h3>
<code style="white-space:pre">&lt;link rel="stylesheet" href="/fonts/inter/inter.css" /&gt;
&lt;link rel="stylesheet" href="/fonts/open-sans/open-sans.css" /&gt;
</code>

  <h3>Exempel inera</h3>
  <code style="white-space:pre">&lt;link rel="stylesheet" href="/fonts/poppins/poppins.css" /&gt;
&lt;link rel="stylesheet" href="/fonts/open-sans/open-sans.css" /&gt;
</code>
</div>
`;

export const Default = () => markupWithCodeExample(markup);

export const Text = () =>
  markupWithCodeExample(`
<div class="ic-text">
  ${markup}
</div>
`);

export const Images = () =>
  markupWithCodeExample([
    `
<div class="ic-text">
  <div class="ic-alert iu-mb-900">
    <p>Images are not floated on small devices.<br/>If needed use utilities <tt>.iu-fl</tt> or <tt>.iu-fr</tt></p>
  </div>
  <h2>Image to the right</h2>
  <figure class="ic-image ic-image-right">
    <img class="ic-image__img" src="https://via.placeholder.com/300x200" alt="placeholder image" />
    <figcaption class="ic-image__text">This is a text for the image</figcaption>
  </figure>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam erat, semper a lacus sit amet, ullamcorper mattis lacus. Aliquam tristique ut lacus a mollis. Aliquam erat volutpat. Suspendisse vehicula a nisi sit amet blandit. Pellentesque ac ultricies justo. Duis et dignissim libero. Nulla gravida dui at viverra rhoncus. Sed semper felis lorem. Mauris mollis ullamcorper arcu, non tristique mauris pulvinar vitae. Ut eu aliquet leo. Nam id velit sed nunc dignissim tempor in ac augue. Nullam tincidunt non risus molestie aliquet. Sed bibendum tellus ac purus malesuada semper. Donec accumsan mi eu dolor pellentesque pulvinar. Nullam eget lacus quis ex congue pellentesque vel et quam. Aliquam diam eros, consectetur id enim id, pharetra accumsan magna.</p>
  <p>Etiam neque tortor, lobortis dignissim metus at, interdum euismod ante. Nunc ultricies sollicitudin porta. Praesent eleifend turpis magna, vel dictum quam porttitor nec. Maecenas eget pellentesque nulla, nec eleifend tortor. Nam sit amet tincidunt tellus. Aenean venenatis tincidunt enim non tempor. Sed quis ante scelerisque, fermentum nulla sit amet, ullamcorper felis. Sed non purus eleifend turpis porta elementum. Nullam feugiat ex et tellus facilisis maximus. Proin sollicitudin facilisis neque, ut dignissim magna. Donec magna velit, suscipit nec odio eget, bibendum porttitor sem. Donec augue mauris, malesuada at rhoncus a, venenatis quis odio. Donec tempor ex ac arcu auctor semper. Nunc cursus, diam eget hendrerit mattis, arcu purus luctus ex, vitae eleifend ante mi a massa.</p>
</div>
`,
    `
<div class="ic-text">
  <h2>Image to the left</h2>
  <figure class="ic-image ic-image-left">
    <img class="ic-image__img" src="https://via.placeholder.com/300x200" alt="placeholder image" />
    <figcaption class="ic-image__text">This is a text for the image</figcaption>
  </figure>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam erat, semper a lacus sit amet, ullamcorper mattis lacus. Aliquam tristique ut lacus a mollis. Aliquam erat volutpat. Suspendisse vehicula a nisi sit amet blandit. Pellentesque ac ultricies justo. Duis et dignissim libero. Nulla gravida dui at viverra rhoncus. Sed semper felis lorem. Mauris mollis ullamcorper arcu, non tristique mauris pulvinar vitae. Ut eu aliquet leo. Nam id velit sed nunc dignissim tempor in ac augue. Nullam tincidunt non risus molestie aliquet. Sed bibendum tellus ac purus malesuada semper. Donec accumsan mi eu dolor pellentesque pulvinar. Nullam eget lacus quis ex congue pellentesque vel et quam. Aliquam diam eros, consectetur id enim id, pharetra accumsan magna.</p>
  <p>Etiam neque tortor, lobortis dignissim metus at, interdum euismod ante. Nunc ultricies sollicitudin porta. Praesent eleifend turpis magna, vel dictum quam porttitor nec. Maecenas eget pellentesque nulla, nec eleifend tortor. Nam sit amet tincidunt tellus. Aenean venenatis tincidunt enim non tempor. Sed quis ante scelerisque, fermentum nulla sit amet, ullamcorper felis. Sed non purus eleifend turpis porta elementum. Nullam feugiat ex et tellus facilisis maximus. Proin sollicitudin facilisis neque, ut dignissim magna. Donec magna velit, suscipit nec odio eget, bibendum porttitor sem. Donec augue mauris, malesuada at rhoncus a, venenatis quis odio. Donec tempor ex ac arcu auctor semper. Nunc cursus, diam eget hendrerit mattis, arcu purus luctus ex, vitae eleifend ante mi a massa.</p>
</div>
`,
    `
<div dir="rtl" class="ic-text">
  <h2>Image to the right with text direction RTL (= image to the left).</h2>
  <figure class="ic-image ic-image-right">
    <img class="ic-image__img" src="https://via.placeholder.com/300x200" alt="placeholder image" />
    <figcaption class="ic-image__text">This is a text for the image</figcaption>
  </figure>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam erat, semper a lacus sit amet, ullamcorper mattis lacus. Aliquam tristique ut lacus a mollis. Aliquam erat volutpat. Suspendisse vehicula a nisi sit amet blandit. Pellentesque ac ultricies justo. Duis et dignissim libero. Nulla gravida dui at viverra rhoncus. Sed semper felis lorem. Mauris mollis ullamcorper arcu, non tristique mauris pulvinar vitae. Ut eu aliquet leo. Nam id velit sed nunc dignissim tempor in ac augue. Nullam tincidunt non risus molestie aliquet. Sed bibendum tellus ac purus malesuada semper. Donec accumsan mi eu dolor pellentesque pulvinar. Nullam eget lacus quis ex congue pellentesque vel et quam. Aliquam diam eros, consectetur id enim id, pharetra accumsan magna.</p>
  <p>Etiam neque tortor, lobortis dignissim metus at, interdum euismod ante. Nunc ultricies sollicitudin porta. Praesent eleifend turpis magna, vel dictum quam porttitor nec. Maecenas eget pellentesque nulla, nec eleifend tortor. Nam sit amet tincidunt tellus. Aenean venenatis tincidunt enim non tempor. Sed quis ante scelerisque, fermentum nulla sit amet, ullamcorper felis. Sed non purus eleifend turpis porta elementum. Nullam feugiat ex et tellus facilisis maximus. Proin sollicitudin facilisis neque, ut dignissim magna. Donec magna velit, suscipit nec odio eget, bibendum porttitor sem. Donec augue mauris, malesuada at rhoncus a, venenatis quis odio. Donec tempor ex ac arcu auctor semper. Nunc cursus, diam eget hendrerit mattis, arcu purus luctus ex, vitae eleifend ante mi a massa.</p>
</div>
`,
    `
<div dir="rtl" class="ic-text">
  <h2>Image to the left with text direction RTL (= image to the right).</h2>
  <figure class="ic-image ic-image-left">
    <img class="ic-image__img" src="https://via.placeholder.com/300x200" alt="placeholder image" />
    <figcaption class="ic-image__text">This is a text for the image</figcaption>
  </figure>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam erat, semper a lacus sit amet, ullamcorper mattis lacus. Aliquam tristique ut lacus a mollis. Aliquam erat volutpat. Suspendisse vehicula a nisi sit amet blandit. Pellentesque ac ultricies justo. Duis et dignissim libero. Nulla gravida dui at viverra rhoncus. Sed semper felis lorem. Mauris mollis ullamcorper arcu, non tristique mauris pulvinar vitae. Ut eu aliquet leo. Nam id velit sed nunc dignissim tempor in ac augue. Nullam tincidunt non risus molestie aliquet. Sed bibendum tellus ac purus malesuada semper. Donec accumsan mi eu dolor pellentesque pulvinar. Nullam eget lacus quis ex congue pellentesque vel et quam. Aliquam diam eros, consectetur id enim id, pharetra accumsan magna.</p>
  <p>Etiam neque tortor, lobortis dignissim metus at, interdum euismod ante. Nunc ultricies sollicitudin porta. Praesent eleifend turpis magna, vel dictum quam porttitor nec. Maecenas eget pellentesque nulla, nec eleifend tortor. Nam sit amet tincidunt tellus. Aenean venenatis tincidunt enim non tempor. Sed quis ante scelerisque, fermentum nulla sit amet, ullamcorper felis. Sed non purus eleifend turpis porta elementum. Nullam feugiat ex et tellus facilisis maximus. Proin sollicitudin facilisis neque, ut dignissim magna. Donec magna velit, suscipit nec odio eget, bibendum porttitor sem. Donec augue mauris, malesuada at rhoncus a, venenatis quis odio. Donec tempor ex ac arcu auctor semper. Nunc cursus, diam eget hendrerit mattis, arcu purus luctus ex, vitae eleifend ante mi a massa.</p>
</div>
`,
  ]);

export const DefinitionList = () =>
  markupWithCodeExample([
    `
<dl class="ic-definition-list">
  <dt>Definition term</dt>
  <dd>Definition description</dd>
  <dt>Definition of a long term</dt>
  <dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam erat, semper a lacus sit amet, ullamcorper mattis lacus. Aliquam tristique ut lacus a mollis. Aliquam erat volutpat. Suspendisse vehicula a nisi sit amet blandit. Pellentesque ac ultricies justo. Duis et dignissim libero. Nulla gravida dui at viverra rhoncus. Sed semper felis lorem. Mauris mollis ullamcorper arcu, non tristique mauris pulvinar vitae. Ut eu aliquet leo. Nam id velit sed nunc dignissim tempor in ac augue. Nullam tincidunt non risus molestie aliquet. Sed bibendum tellus ac purus malesuada semper. Donec accumsan mi eu dolor pellentesque pulvinar. Nullam eget lacus quis ex congue pellentesque vel et quam.</dd>
  <dt>Short term</dt>
  <dd>Aliquam diam eros, consectetur id enim id, pharetra accumsan magna.</dd>
</dl>
`,
    `
<dl class="ic-definition-list--md">
  <dt>Small screen</dt>
  <dd>Has 1 column</dd>
  <dt>Medium screen</dt>
  <dd>Has grid with 2 columns</dd>
  <dt>Large screen</dt>
  <dd>Has grid with 2 columns</dd>
</dl>
`,
    `
<dl class="ic-definition-list--lg">
  <dt>Small screen</dt>
  <dd>Has 1 column</dd>
  <dt>Medium screen</dt>
  <dd>Has 1 column</dd>
  <dt>Large screen</dt>
  <dd>Has grid with 2 columns</dd>
</dl>
`,
    `
<dl dir="rtl" class="ic-definition-list">
  <dt>Text direction</dt>
  <dd>RTL</dd>
  <dt>Should</dt>
  <dd>work</dd>
</dl>
`,
  ]);
