import { markupWithCodeExample, bleedContainer } from "./helpers";

export default {
  title: "components/Navigation",
  decorators: [bleedContainer],
  parameters: {
    status: "",
  },
};

export const MobileMenu = () =>
  markupWithCodeExample([
    `
<nav aria-label="Användarmeny" class="ic-nav-list" id="mobile-nav">
      <ul class="ic-nav-list__list">
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 1</a>
          <button
            aria-expanded="false"
            aria-controls="submenu-0"
            class="ic-nav-list__expand"
          >
            <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
          </button>
          <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
        </li>
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 2</a>
        </li>
        <li
          class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
        >
          <a href="javascript:void(0)">Menu item 3</a>
          <button
            aria-expanded="true"
            aria-controls="submenu-1"
            class="ic-nav-list__expand"
          >
            <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
          </button>
          <ul class="ic-nav-list__list" id="submenu-1">
            <li class="ic-nav-list__item">
              <a href="javascript:void(0)">Submenu item 1</a>
            </li>
            <li
              class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
            >
              <a href="javascript:void(0)">Submenu item 2</a>
              <button
                aria-expanded="true"
                aria-controls="submenu-2"
                class="ic-nav-list__expand"
              >
                <span class="iu-sr-only">Visa innehåll för Submenu item 2</span>
              </button>
              <ul class="ic-nav-list__list" id="submenu-2">
                <li class="ic-nav-list__item">
                  <a href="javascript:void(0)">Sub Submenu item 1</a>
                </li>
                <li
                  class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
                >
                  <a href="javascript:void(0)">Sub Submenu item 2</a>
                  <button
                    aria-expanded="true"
                    aria-controls="submenu-3"
                    class="ic-nav-list__expand"
                  >
                    <span class="iu-sr-only"
                      >Visa innehåll för Sub Submenu item 2</span
                    >
                  </button>
                  <ul class="ic-nav-list__list" id="submenu-3">
                    <li class="ic-nav-list__item">
                      <a href="javascript:void(0)">Sub sub submenu item 1</a>
                    </li>
                    <li
                      class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
                    >
                      <a href="javascript:void(0)">Sub sub submenu item 2</a>
                      <button
                        aria-expanded="true"
                        aria-controls="submenu-4"
                        class="ic-nav-list__expand"
                      >
                        <span class="iu-sr-only"
                          >Visa innehåll för Sub Submenu item 2</span
                        >
                      </button>
                      <ul class="ic-nav-list__list" id="submenu-4">
                        <li class="ic-nav-list__item">
                          <a href="javascript:void(0)"
                            >Sub sub sub submenu item 1</a
                          >
                        </li>
                        <li class="ic-nav-list__item">
                          <a aria-current="page" href="javascript:void(0)"
                            >Sub sub sub submenu item 2</a
                          >
                        </li>
                        <li class="ic-nav-list__item">
                          <a href="javascript:void(0)"
                            >Sub sub sub submenu item 3</a
                          >
                        </li>
                        <li class="ic-nav-list__item">
                          <a href="javascript:void(0)"
                            >Sub sub sub submenu item 4</a
                          >
                        </li>
                      </ul>
                    </li>
                    <li class="ic-nav-list__item">
                      <a href="javascript:void(0)">Sub sub submenu item 3</a>
                    </li>
                    <li class="ic-nav-list__item">
                      <a href="javascript:void(0)">Sub sub submenu item 4</a>
                    </li>
                  </ul>
                </li>
                <li class="ic-nav-list__item">
                  <a href="javascript:void(0)">Sub submenu item 3</a>
                </li>
              </ul>
            </li>
            <li class="ic-nav-list__item">
              <a href="javascript:void(0)">Submenu item 3</a>
            </li>
          </ul>
        </li>
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 4</a>
        </li>
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 5</a>
        </li>
      </ul>
    </nav>
`,
  ]);

export const MobileMenuRTL = () =>
  markupWithCodeExample([
    `
<div dir="rtl">
  <nav aria-label="Användarmeny" class="ic-nav-list" id="mobile-nav">
      <ul class="ic-nav-list__list">
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 1</a>
          <button
            aria-expanded="false"
            aria-controls="submenu-0"
            class="ic-nav-list__expand"
          >
            <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
          </button>
          <ul hidden class="ic-nav-list__list" id="submenu-0"></ul>
        </li>
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 2</a>
        </li>
        <li
          class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
        >
          <a href="javascript:void(0)">Menu item 3</a>
          <button
            aria-expanded="true"
            aria-controls="submenu-1"
            class="ic-nav-list__expand"
          >
            <span class="iu-sr-only">Visa innehåll för Menu item 1</span>
          </button>
          <ul class="ic-nav-list__list" id="submenu-1">
            <li class="ic-nav-list__item">
              <a href="javascript:void(0)">Submenu item 1</a>
            </li>
            <li
              class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
            >
              <a href="javascript:void(0)">Submenu item 2</a>
              <button
                aria-expanded="true"
                aria-controls="submenu-2"
                class="ic-nav-list__expand"
              >
                <span class="iu-sr-only">Visa innehåll för Submenu item 2</span>
              </button>
              <ul class="ic-nav-list__list" id="submenu-2">
                <li class="ic-nav-list__item">
                  <a href="javascript:void(0)">Sub Submenu item 1</a>
                </li>
                <li
                  class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
                >
                  <a href="javascript:void(0)">Sub Submenu item 2</a>
                  <button
                    aria-expanded="true"
                    aria-controls="submenu-3"
                    class="ic-nav-list__expand"
                  >
                    <span class="iu-sr-only"
                      >Visa innehåll för Sub Submenu item 2</span
                    >
                  </button>
                  <ul class="ic-nav-list__list" id="submenu-3">
                    <li class="ic-nav-list__item">
                      <a href="javascript:void(0)">Sub sub submenu item 1</a>
                    </li>
                    <li
                      class="ic-nav-list__item ic-nav-list__item--has-children ic-nav-list__item--expanded"
                    >
                      <a href="javascript:void(0)">Sub sub submenu item 2</a>
                      <button
                        aria-expanded="true"
                        aria-controls="submenu-4"
                        class="ic-nav-list__expand"
                      >
                        <span class="iu-sr-only"
                          >Visa innehåll för Sub Submenu item 2</span
                        >
                      </button>
                      <ul class="ic-nav-list__list" id="submenu-4">
                        <li class="ic-nav-list__item">
                          <a href="javascript:void(0)"
                            >Sub sub sub submenu item 1</a
                          >
                        </li>
                        <li class="ic-nav-list__item">
                          <a aria-current="page" href="javascript:void(0)"
                            >Sub sub sub submenu item 2</a
                          >
                        </li>
                        <li class="ic-nav-list__item">
                          <a href="javascript:void(0)"
                            >Sub sub sub submenu item 3</a
                          >
                        </li>
                        <li class="ic-nav-list__item">
                          <a href="javascript:void(0)"
                            >Sub sub sub submenu item 4</a
                          >
                        </li>
                      </ul>
                    </li>
                    <li class="ic-nav-list__item">
                      <a href="javascript:void(0)">Sub sub submenu item 3</a>
                    </li>
                    <li class="ic-nav-list__item">
                      <a href="javascript:void(0)">Sub sub submenu item 4</a>
                    </li>
                  </ul>
                </li>
                <li class="ic-nav-list__item">
                  <a href="javascript:void(0)">Sub submenu item 3</a>
                </li>
              </ul>
            </li>
            <li class="ic-nav-list__item">
              <a href="javascript:void(0)">Submenu item 3</a>
            </li>
          </ul>
        </li>
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 4</a>
        </li>
        <li class="ic-nav-list__item">
          <a href="javascript:void(0)">Menu item 5</a>
        </li>
      </ul>
    </nav>
</div>`,
  ]);
