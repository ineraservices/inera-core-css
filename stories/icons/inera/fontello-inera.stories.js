/*
import { markupWithCodeExample, wrapStoryInContainer } from "../../helpers";

export default {
  title: "components/Icons/Inera",
  decorators: [wrapStoryInContainer],
  parameters: {
    status: "",
    docs: {
      description: {
        component: [
          "* Fontello files are not included in the master css for each theme. They are included under: `/dist/min/icons/[theme]/fontello/style.css`",
          "* The Fontello font is deprecated. Please use the SVG Icons instead!",
        ].join("\r\n"),
      },
    },
  },
};

export const Fontello = () =>
  markupWithCodeExample([
    `<link href="/min/icons/inera/fontello/style.css" rel="stylesheet" />`,
    `<span aria-hidden="true" class="icon-installningar"></span>`,
    `<span aria-hidden="true" class="icon-arrow"></span>`,
    `<span aria-hidden="true" class="icon-plus"></span>`,
    `<span aria-hidden="true" class="icon-minus"></span>`,
    `<span aria-hidden="true" class="icon-arrow-rigt-ext-circle"></span>`,
    `<span aria-hidden="true" class="icon-print"></span>`,
    `<span aria-hidden="true" class="icon-enlarge-arrows"></span>`,
    `<span aria-hidden="true" class="icon-cancel"></span>`,
    `<span aria-hidden="true" class="icon-arrow"></span>`,
    `<span aria-hidden="true" class="icon-pdf "></span>`,
    `<span aria-hidden="true" class="icon-ppt "></span>`,
    `<span aria-hidden="true" class="icon-xls "></span>`,
    `<span aria-hidden="true" class="icon-doc "></span>`,
    `<span aria-hidden="true" class="icon-menu-x"></span>`,
    `<span aria-hidden="true" class="icon-map-pin-plus"></span>`,
    `<span aria-hidden="true" class="icon-info-full"></span>`,
    `<span aria-hidden="true" class="icon-phone"></span>`,
    `<span aria-hidden="true" class="icon-search"></span>`,
    `<span aria-hidden="true" class="icon-info"></span>`,
    `<span aria-hidden="true" class="icon-check"></span>`,
    `<span aria-hidden="true" class="icon-pointer"></span>`,
    `<span aria-hidden="true" class="icon-listview"></span>`,
    `<span aria-hidden="true" class="icon-blockview"></span>`,
    `<span aria-hidden="true" class="icon-file"></span>`,
    `<span aria-hidden="true" class="icon-link-ext"></span>`,
    `<span aria-hidden="true" class="icon-twitter"></span>`,
    `<span aria-hidden="true" class="icon-facebook"></span>`,
    `<span aria-hidden="true" class="icon-email-2"></span>`,
    `<span aria-hidden="true" class="icon-angle-right"></span>`,
    `<span aria-hidden="true" class="icon-angle-down"></span>`,
  ]);
 */
